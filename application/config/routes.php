<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';

//Rutas del home
$route['acerca'] = "home/acerca";
$route['konscio'] = "home/konscio";
$route['videos/(:num)'] = "home/videos/$1";
$route['videos'] = "home/videos";
$route['libros'] = "home/libros";
$route['cursos_talleres'] = "home/cursos_talleres";
$route['eventos'] = "home/eventos";
$route['testimonios'] = "home/testimonios";
$route['preguntas_frecuentes'] = "home/preguntas_frecuentes";
$route['blogs'] = "home/blogs";

$route['articulo/(:num)'] = 'home/blogs/$1'; //Paginacion index articulo de post
$route['admin/posts(:num)'] = 'admin/posts/$1'; //Paginacion posts en admin
$route['admin/categorias(:num)'] = 'admin/categorias/$1'; //Paginacion categorias en admin
$route['articulo/categoria/(:any)/(:num)'] = 'articulo/categoria/$1/$2'; //Filtrar por categoría en articulo
$route['articulo/(:any)'] = 'home/articulo/$1'; //Detalle de un post public
//$route['articulo/(:any)/(:any)'] = 'articulo/post_view/$1/$2'; //Detalle de un post
$route['translate_uri_dashes'] = FALSE;
$route[LOGIN_PAGE] = 'app/login';
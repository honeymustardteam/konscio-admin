<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
//require './vendor/autoload.php';  

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Admin extends MY_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->database();
		//Importamos librerias
		$this->load->library("parser");
		$this->load->library("Form_validation");
		//Importamos helpers
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('Post_helper');
		$this->load->helper('Date_helper');
		$this->load->helper('text');
		//Importamos los modelos
		$this->load->model('Post');
		$this->load->model('Eventos');
		$this->load->model('Testimonios');
		$this->load->model('Cursos');
		$this->load->model('Clientes');

		$this->load->model('Videos');
		//Acemos que la autorizacion de este controlador sea solo por el admin
		$this->init_seccion_auto(9);
	}

	/*****************************************************
	**********************Posts**********************
	*****************************************************/

	public function index()
	{
		redirect(base_url().'admin/eventos');
		//$view["body"] = $this->load->view('admin/index',null,true);
		//$view["titulo_english"] = "Inicio";
		//$this->parser->parse("admin/template/body",$view);
	}

	public function posts($num_page = 1){

		$num_page--;
        $num_post = $this->Post->count_post_admin();
        $last_page = ceil($num_post / PAGE_SIZE);

        if ($num_page < 0) {
            $num_page = 0;
        } elseif ($num_page > $last_page) {
            // TODO
            $num_page = 0;
        }

        $offset = $num_page * PAGE_SIZE;
		
		$data['last_page'] = $last_page;
		$data['current_page'] = $num_page;
		$data['last_page'] = $last_page;
		$data['pagination'] = true;
		$data['token_url'] = 'admin/posts/';
		$data["posts"] = $this->Post->get_pagination_admin($offset,'admin');
		//$data['main_posts'] = $this->Main_Post->get_main_posts();
		$view["body"] = $this->load->view('admin/post/index',$data,true);
		$view["title"] = "Posts";
		$this->parser->parse("admin/template/body",$view);
	}

	public function post($post_id=null){
		if($post_id==null){
			$main_posts = [];
			//Crear post
			$data['title'] = $data['titlee'] = $data['content'] = $data['content1'] = $data['description'] 
			= $data['posted'] = $data['url_clean'] = $data['cover_image'] = '';
		} else {
			//Editar post
			$post = $this->Post->find($post_id);

			if(is_array($post)){

				if(count($post)==0){
					show_404();
					return ;
				}

			}

			$data['title'] = $post->title;
			$data['titlee'] = $post->titlee;
			$data['content'] = $post->content;
			$data['content1'] = $post->content1;
			$data['posted'] = $post->posted;
			$data['url_clean'] = $post->url_clean;
			$data['cover_image'] = $post->cover_image;
		}

		if($this->input->server('REQUEST_METHOD')=="POST"){
			$this->form_validation->set_rules('title','Titulo','required|min_length[5]|max_length[255]');
			$this->form_validation->set_rules('titlee','Title','required|min_length[5]|max_length[255]');
			$this->form_validation->set_rules('content','Contenido','required|min_length[10]');
			$this->form_validation->set_rules('content1','Content','required|min_length[10]');
			$this->form_validation->set_rules('posted','Publicado','required');

			//Llenar campos con la información anterior
			$data['title'] = $this->input->post('title');
			$data['titlee'] = $this->input->post('titlee');
			$data['subtitle'] = $this->input->post('subtitle');
			$data['content'] = $this->input->post('content');
			$data['content1'] = $this->input->post('content1');
			$data['description'] = $this->input->post('description');
			$data['posted'] = $this->input->post('posted');
			$data['url_clean'] = $this->input->post('url_clean');

			if($this->form_validation->run()){

				$url_clean = clean_name($this->input->post('title'));

				$save = array(
					'content' => $this->input->post('content'),
					'content1' => $this->input->post('content1'),
					'title' => $this->input->post('title'),
					'titlee' => $this->input->post('titlee'),
					'posted' => $this->input->post('posted'),
					'url_clean' => $url_clean,
				);
				if($post_id==null){
					$post_id = $this->Post->insert($save);
				} else {
					$this->Post->update($post_id, $save);
				}
				if (isset($_FILES['cover_upload']) && $_FILES['cover_upload']['name'] != ''){
					$numRamdom = rand ( 10000 , 99999 );
					$this->upload('posts',$post_id,$this->input->post('title').$numRamdom,'cover_upload','cover_image');
				}
				//Una vez subidas las imagenes se reedirecciona a la url
				redirect(base_url()."admin/posts");
			}
		}
		//Si no hay una peticion POST entonces mostrarmos la vista de crear
		$data["data_posted"] = posted();
		$view["body"] = $this->load->view('admin/post/crearPost',$data,true);
		$this->parser->parse("admin/template/body",$view);
	}

	public function post_delete($post_id=null){
		if($post_id==null){
			echo "0";
		} else {
			$this->Post->delete($post_id);
			echo "1";
		}
	}

	function images_server(){
		$data['images'] = all_images();
		//var_dump($data);
		$this->load->view("admin/post/image",$data);
	}


	public function upload($tabla='posts', $post_id = null,$title = null,$image="upload",$name_image='image'){
		//Configuramos los datos de la imagen
		//echo "<script>console.log('SUBIENDO IMAGEN..')</script>";
		$type_img = ($image=="upload") ? '' : 'cover_';
		$image = $image;
		if($title!=null)
			$title = clean_name($title);

		$config['upload_path'] = "uploads/".$type_img."post";

		if($title!=null)
			$config['file_name'] = $title; //$type_img.clean_name($title);

		$config['allowed_types'] = "gif|jpg|png|jpeg";
		$config['max_size'] = 50000;
		$config['overwrite'] = true;
		//Cargamos libreria de subir archivos
		$this->load->library('upload',$config);
		$this->upload->initialize($config);
		if ($this->upload->do_upload($image)) {
			//echo "Se sube la imagen ".$title."<br>";
			//echo "Imagen ".$image;
			//Se carga la imagen
			$data = $this->upload->data();
			if($title!=null && $post_id!=null){
				$save = array($name_image => $title.$data["file_ext"]);

				if($tabla == 'posts'){
					$this->Post->update($post_id,$save);
				}
				if($tabla == 'eventos'){
					$this->Eventos->update($post_id,$save);
				}
				if($tabla == 'testimonios'){
					$this->Testimonios->update($post_id,$save);
				}
				if($tabla == 'videos'){
					$this->Videos->update($post_id,$save);
				}
				if($tabla == 'cursos'){
					$this->Cursos->update($post_id,$save);
				}
				
			} else{
				$title = $data['file_name'];
				echo json_encode(array("fileName" => $title, "uploaded" => 1,"url" => "/".PROJECT_FOLDER."/uploads/post/".$title));
			}
			$this->resize_image($data['full_path'],$title.$data["file_ext"]);
		} else {
			//var_dump( $this->upload->display_errors());
			//echo "No se sube la imagen";
		}
	}

	public function resize_image($path_image,$image_name){
		$config['image_library'] = 'gd2';
		$config['source_image'] = $path_image;
		//$config['new_image'] = 'uploads/';
		$config['maintain_ratio'] = TRUE;
		//$config['create_thumb'] = TRUE;
		//$config['width'] = 500;
		//$config['height'] = 500;

		$this->load->library('image_lib',$config);
		$this->image_lib->resize();

	}









	public function eventos($num_page = 1){

		$num_page--;
        $num_post = $this->Eventos->count_post_admin();
        $last_page = ceil($num_post / PAGE_SIZE);

        if ($num_page < 0) {
            $num_page = 0;
        } elseif ($num_page > $last_page) {
            // TODO
            $num_page = 0;
        }

        $offset = $num_page * PAGE_SIZE;
		
		$data['last_page'] = $last_page;
		$data['current_page'] = $num_page;
		$data['last_page'] = $last_page;
		$data['pagination'] = true;
		$data['token_url'] = 'admin/eventos/';
		$data["posts"] = $this->Eventos->get_pagination_admin($offset,'admin');
		//$data['main_eventos'] = $this->Main_Post->get_main_eventos();
		$view["body"] = $this->load->view('admin/eventos/index',$data,true);
		$view["title"] = "Eventos";
		$this->parser->parse("admin/template/body",$view);
	}

	public function evento($id=null){
		if($id==null){
			$main_posts = [];
			//Crear post
			$data['fecha_spanish'] = $data['fecha_english'] = $data['titulo_english'] = $data['titulo_spanish'] = $data['contenido_spanish'] = $data['contenido_english'] = $data['description'] 
			= $data['posted'] = $data['url_clean'] = $data['cover_image'] = '';
		} else {
			//Editar post
			$post = $this->Eventos->find($id);

			if(is_array($post)){

				if(count($post)==0){
					show_404();
					return ;
				}

			}

			$data['fecha_spanish'] = $post->fecha_spanish;
			$data['fecha_english'] = $post->fecha_english;

			$data['titulo_english'] = $post->titulo_english;
			$data['titulo_spanish'] = $post->titulo_spanish;
			$data['contenido_spanish'] = $post->contenido_spanish;
			$data['contenido_english'] = $post->contenido_english;
			$data['posted'] = $post->posted;
			$data['url_clean'] = $post->url_clean;
			$data['cover_image'] = $post->cover_image;
		}

		if($this->input->server('REQUEST_METHOD')=="POST"){
			$this->form_validation->set_rules('titulo_english','Titulo','required|min_length[5]|max_length[255]');
			$this->form_validation->set_rules('titulo_spanish','Title','required|min_length[5]|max_length[255]');
			$this->form_validation->set_rules('contenido_spanish','Contenido','required|min_length[10]');
			$this->form_validation->set_rules('contenido_english','Content','required|min_length[10]');
			$this->form_validation->set_rules('posted','Publicado','required');

			//Llenar campos con la información anterior
			$data['titulo_english'] = $this->input->post('titulo_english');
			$data['titulo_spanish'] = $this->input->post('titulo_spanish');
			$data['fecha_english'] = $this->input->post('titulo_english');
			$data['fecha_spanish'] = $this->input->post('titulo_spanish');
			$data['subtitle'] = $this->input->post('subtitle');
			$data['contenido_spanish'] = $this->input->post('contenido_spanish');
			$data['contenido_english'] = $this->input->post('contenido_english');
			$data['description'] = $this->input->post('description');
			$data['posted'] = $this->input->post('posted');
			$data['url_clean'] = $this->input->post('url_clean');

			if($this->form_validation->run()){

				$url_clean = clean_name($this->input->post('titulo_english'));

				$save = array(
					'contenido_spanish' => $this->input->post('contenido_spanish'),
					'contenido_english' => $this->input->post('contenido_english'),
					'titulo_english' => $this->input->post('titulo_english'),
					'titulo_spanish' => $this->input->post('titulo_spanish'),
					'fecha_english' => $this->input->post('fecha_english'),
					'fecha_spanish' => $this->input->post('fecha_spanish'),
					'posted' => $this->input->post('posted'),
					'url_clean' => $url_clean,
				);
				if($id==null){
					$id = $this->Eventos->insert($save);
				} else {
					$this->Eventos->update($id, $save);
				}
				if (isset($_FILES['cover_upload']) && $_FILES['cover_upload']['name'] != ''){
					$this->upload('eventos',$id,$this->input->post('titulo_spanish'),'cover_upload','cover_image');
				}
				//Una vez subidas las imagenes se reedirecciona a la url
				redirect(base_url()."admin/eventos");
			}
		}
		//Si no hay una peticion POST entonces mostrarmos la vista de crear
		$data["data_posted"] = posted();
		$view["body"] = $this->load->view('admin/eventos/crearPost',$data,true);
		$this->parser->parse("admin/template/body",$view);
	}

	public function evento_delete($id=null){
		if($id==null){
			echo "0";
		} else {
			$this->Eventos->delete($id);
			echo "1";
		}
	}









	public function testimonios($num_page = 1){

		$num_page--;
        $num_post = $this->Testimonios->count_post_admin();
        $last_page = ceil($num_post / PAGE_SIZE);

        if ($num_page < 0) {
            $num_page = 0;
        } elseif ($num_page > $last_page) {
            // TODO
            $num_page = 0;
        }

        $offset = $num_page * PAGE_SIZE;
		
		$data['last_page'] = $last_page;
		$data['current_page'] = $num_page;
		$data['last_page'] = $last_page;
		$data['pagination'] = true;
		$data['token_url'] = 'admin/testimonios/';
		$data["posts"] = $this->Testimonios->get_pagination_admin($offset,'admin');
		//$data['main_eventos'] = $this->Main_Post->get_main_eventos();
		$view["body"] = $this->load->view('admin/testimonios/index',$data,true);
		$view["title"] = "Testimonios";
		$this->parser->parse("admin/template/body",$view);
	}

	public function testimonio($id=null){
		if($id==null){
			$main_posts = [];
			//Crear post
			$data['titulo_english'] = $data['titulo_spanish'] = $data['contenido_spanish'] = $data['contenido_english'] = $data['description'] 
			= $data['posted'] = $data['url_clean'] = $data['cover_image'] = '';
		} else {
			//Editar post
			$post = $this->Testimonios->find($id);

			if(is_array($post)){

				if(count($post)==0){
					show_404();
					return ;
				}

			}

			$data['titulo_english'] = $post->titulo_english;
			$data['titulo_spanish'] = $post->titulo_spanish;
			$data['contenido_spanish'] = $post->contenido_spanish;
			$data['contenido_english'] = $post->contenido_english;
			$data['posted'] = $post->posted;
			$data['url_clean'] = $post->url_clean;
			$data['cover_image'] = $post->cover_image;
		}

		if($this->input->server('REQUEST_METHOD')=="POST"){
			$this->form_validation->set_rules('titulo_english','Titulo','required|min_length[5]|max_length[255]');
			$this->form_validation->set_rules('titulo_spanish','Title','required|min_length[5]|max_length[255]');
			$this->form_validation->set_rules('contenido_spanish','Contenido','required|min_length[10]');
			$this->form_validation->set_rules('contenido_english','Content','required|min_length[10]');
			$this->form_validation->set_rules('posted','Publicado','required');

			//Llenar campos con la información anterior
			$data['titulo_english'] = $this->input->post('titulo_english');
			$data['titulo_spanish'] = $this->input->post('titulo_spanish');
			$data['subtitle'] = $this->input->post('subtitle');
			$data['contenido_spanish'] = $this->input->post('contenido_spanish');
			$data['contenido_english'] = $this->input->post('contenido_english');
			$data['description'] = $this->input->post('description');
			$data['posted'] = $this->input->post('posted');
			$data['url_clean'] = $this->input->post('url_clean');

			if($this->form_validation->run()){

				$url_clean = clean_name($this->input->post('titulo_english'));

				$save = array(
					'contenido_spanish' => $this->input->post('contenido_spanish'),
					'contenido_english' => $this->input->post('contenido_english'),
					'titulo_english' => $this->input->post('titulo_english'),
					'titulo_spanish' => $this->input->post('titulo_spanish'),
					'posted' => $this->input->post('posted'),
					'url_clean' => $url_clean,
				);
				if($id==null){
					$id = $this->Testimonios->insert($save);
				} else {
					$this->Testimonios->update($id, $save);
				}
				if (isset($_FILES['cover_upload']) && $_FILES['cover_upload']['name'] != ''){
					$this->upload('testimonios',$id,$this->input->post('titulo_english'),'cover_upload','cover_image');
				}
				//Una vez subidas las imagenes se reedirecciona a la url
				redirect(base_url()."admin/testimonios");
			}
		}
		//Si no hay una peticion POST entonces mostrarmos la vista de crear
		$data["data_posted"] = posted();
		$view["body"] = $this->load->view('admin/testimonios/crearPost',$data,true);
		$this->parser->parse("admin/template/body",$view);
	}

	public function testimonio_delete($id=null){
		if($id==null){
			echo "0";
		} else {
			$this->Testimonios->delete($id);
			echo "1";
		}
	}


















	public function videos($num_page = 1){

		$num_page--;
        $num_post = $this->Videos->count_post_admin();
        $last_page = ceil($num_post / PAGE_SIZE);

        if ($num_page < 0) {
            $num_page = 0;
        } elseif ($num_page > $last_page) {
            // TODO
            $num_page = 0;
        }

        $offset = $num_page * PAGE_SIZE;
		
		$data['last_video'] = $num_post;

		$data['last_page'] = $last_page;
		$data['current_page'] = $num_page;
		$data['last_page'] = $last_page;
		$data['pagination'] = true;
		$data['token_url'] = 'admin/videos/';
		$data["posts"] = $this->Videos->get_pagination_admin($offset,'admin');
		//$data['main_eventos'] = $this->Main_Post->get_main_eventos();
		$view["body"] = $this->load->view('admin/videos/index',$data,true);
		$view["title"] = "Videos";
		$this->parser->parse("admin/template/body",$view);
	}

	public function videos_mover($post_id = '', $accion=''){

		
		
		$post = $this->Videos->find($post_id);
		$ordenActual = $post->orden;
		
		if($accion == 'bajar'){
			$old = $this->Videos->find_by_orden($post->orden+1);
			$post->orden = (int)$post->orden+1;
			
		}else{
			//subir
			$old = $this->Videos->find_by_orden($post->orden-1);
			$post->orden = (int)$post->orden-1;
		}

		//echo $old->post_id;
		//var_dump($old);
		$old->orden = $ordenActual;
		
		$this->Videos->update($post_id, $post);
		$this->Videos->update($old->post_id, $old);

		//$this->Eventos->update($id, $save);

		redirect(base_url()."admin/videos");
	}

	public function video($id=null){
		if($id==null){
			$main_posts = [];
			//Crear post
			$data['titulo_english'] = $data['titulo_spanish'] = $data['url_spanish'] = $data['url_english'] = $data['description'] 
			= $data['posted'] = $data['show_in'] = $data['url_clean'] = $data['cover_image_es'] = $data['cover_image_en'] = '';
		} else {
			//Editar post
			$post = $this->Videos->find($id);

			if(is_array($post)){

				if(count($post)==0){
					show_404();
					return ;
				}

			}

			$data['titulo_english'] = $post->titulo_english;
			$data['titulo_spanish'] = $post->titulo_spanish;
			$data['url_spanish'] = $post->url_spanish;
			$data['url_english'] = $post->url_english;
			$data['posted'] = $post->posted;
			$data['show_in'] = $post->show_in;
			$data['url_clean'] = $post->url_clean;
			$data['cover_image_es'] = $post->cover_image_es;
			$data['cover_image_en'] = $post->cover_image_en;
		}

		if($this->input->server('REQUEST_METHOD')=="POST"){
			// $this->form_validation->set_rules('titulo_english','Titulo','required|min_length[5]|max_length[255]');
			// $this->form_validation->set_rules('titulo_spanish','Title','required|min_length[5]|max_length[255]');
			// $this->form_validation->set_rules('url_spanish','Contenido','required|min_length[10]');
			// $this->form_validation->set_rules('url_english','Content','required|min_length[10]');
			$this->form_validation->set_rules('posted','Publicado','required');
			$this->form_validation->set_rules('show_in','Idioma','required');

			//Llenar campos con la información anterior
			$data['titulo_english'] = $this->input->post('titulo_english');
			$data['titulo_spanish'] = $this->input->post('titulo_spanish');
			$data['subtitle'] = $this->input->post('subtitle');
			$data['url_spanish'] = $this->input->post('url_spanish');
			$data['url_english'] = $this->input->post('url_english');
			$data['description'] = $this->input->post('description');
			$data['posted'] = $this->input->post('posted');
			$data['show_in'] = $this->input->post('show_in');
			$data['url_clean'] = $this->input->post('url_clean');

			if($this->form_validation->run()){

				$url_clean = clean_name($this->input->post('titulo_english'));

				$save = array(
					'url_spanish' => $this->input->post('url_spanish'),
					'url_english' => $this->input->post('url_english'),
					'titulo_english' => $this->input->post('titulo_english'),
					'titulo_spanish' => $this->input->post('titulo_spanish'),
					'posted' => $this->input->post('posted'),
					'show_in' => $this->input->post('show_in'),
					'url_clean' => $url_clean,
				);
				if($id==null){
					$id = $this->Videos->insert($save);
				} else {
					$this->Videos->update($id, $save);
				}
				if (isset($_FILES['cover_upload_es']) && $_FILES['cover_upload_es']['name'] != ''){
					$this->upload('videos',$id,$this->input->post('titulo_spanish').'_img1','cover_upload_es','cover_image_es');
				}

				if (isset($_FILES['cover_upload_en']) && $_FILES['cover_upload_en']['name'] != ''){
					$this->upload('videos',$id,$this->input->post('titulo_english').'_img2','cover_upload_en','cover_image_en');
				}

				//Una vez subidas las imagenes se reedirecciona a la url
				redirect(base_url()."admin/videos");
			}
		}
		//Si no hay una peticion POST entonces mostrarmos la vista de crear
		$data["data_posted"] = posted();
		$data["data_mostrar"] = array( "es" => "Español", "en" => "Ingles", "ambos" => "Español e Ingles");
		$view["body"] = $this->load->view('admin/videos/crearPost',$data,true);
		$this->parser->parse("admin/template/body",$view);
	}

	public function video_delete($id=null){
		if($id==null){
			echo "0";
		} else {
			$this->Videos->delete($id);
			echo "1";
		}
	}






























	public function cursos($num_page = 1){

		$num_page--;
        $num_post = $this->Cursos->count_post_admin();
        $last_page = ceil($num_post / PAGE_SIZE);

        if ($num_page < 0) {
            $num_page = 0;
        } elseif ($num_page > $last_page) {
            // TODO
            $num_page = 0;
        }

        $offset = $num_page * PAGE_SIZE;
		
		$data['last_page'] = $last_page;
		$data['current_page'] = $num_page;
		$data['last_page'] = $last_page;
		$data['pagination'] = true;
		$data['token_url'] = 'admin/cursos/';
		$data["posts"] = $this->Cursos->get_pagination_admin($offset,'admin');
		//$data['main_eventos'] = $this->Main_Post->get_main_eventos();
		$view["body"] = $this->load->view('admin/cursos/index',$data,true);
		$view["title"] = "Cursos";
		$this->parser->parse("admin/template/body",$view);
	}

	public function curso($id=null){
		if($id==null){
			$main_posts = [];
			//Crear post
			$data['fecha_spanish'] = $data['duracion'] = $data['precio'] = $data['fecha_english'] = $data['titulo_english'] = $data['titulo_spanish'] = $data['contenido_spanish'] = $data['contenido_english'] = $data['description'] 
			= $data['posted'] = $data['url_clean'] = $data['cover_image'] = '';
		} else {
			//Editar post
			$post = $this->Cursos->find($id);

			if(is_array($post)){

				if(count($post)==0){
					show_404();
					return ;
				}

			}

			$data['fecha_spanish'] = $post->fecha_spanish;
			$data['fecha_english'] = $post->fecha_english;

			$data['duracion'] = $post->duracion;
			$data['precio'] = $post->precio;

			$data['titulo_english'] = $post->titulo_english;
			$data['titulo_spanish'] = $post->titulo_spanish;
			$data['contenido_spanish'] = $post->contenido_spanish;
			$data['contenido_english'] = $post->contenido_english;
			$data['posted'] = $post->posted;
			$data['url_clean'] = $post->url_clean;
			$data['cover_image'] = $post->cover_image;
		}

		if($this->input->server('REQUEST_METHOD')=="POST"){
			$this->form_validation->set_rules('titulo_english','Titulo','required|min_length[5]|max_length[255]');
			$this->form_validation->set_rules('titulo_spanish','Title','required|min_length[5]|max_length[255]');
			$this->form_validation->set_rules('contenido_spanish','Contenido','required|min_length[10]');
			$this->form_validation->set_rules('contenido_english','Content','required|min_length[10]');
			$this->form_validation->set_rules('posted','Publicado','required');

			//Llenar campos con la información anterior
			$data['duracion'] = $this->input->post('duracion');
			$data['precio'] = $this->input->post('precio');

			$data['titulo_english'] = $this->input->post('titulo_english');
			$data['titulo_spanish'] = $this->input->post('titulo_spanish');
			$data['fecha_english'] = $this->input->post('titulo_english');
			$data['fecha_spanish'] = $this->input->post('titulo_spanish');
			$data['subtitle'] = $this->input->post('subtitle');
			$data['contenido_spanish'] = $this->input->post('contenido_spanish');
			$data['contenido_english'] = $this->input->post('contenido_english');
			$data['description'] = $this->input->post('description');
			$data['posted'] = $this->input->post('posted');
			$data['url_clean'] = $this->input->post('url_clean');

			if($this->form_validation->run()){

				$url_clean = clean_name($this->input->post('titulo_english'));

				$save = array(
					'duracion' => $this->input->post('duracion'),
					'precio' => $this->input->post('precio'),
					'contenido_spanish' => $this->input->post('contenido_spanish'),
					'contenido_english' => $this->input->post('contenido_english'),
					'titulo_english' => $this->input->post('titulo_english'),
					'titulo_spanish' => $this->input->post('titulo_spanish'),
					'fecha_english' => $this->input->post('fecha_english'),
					'fecha_spanish' => $this->input->post('fecha_spanish'),
					'posted' => $this->input->post('posted'),
					'url_clean' => $url_clean,
				);
				if($id==null){
					$id = $this->Cursos->insert($save);
				} else {
					$this->Cursos->update($id, $save);
				}
				if (isset($_FILES['cover_upload']) && $_FILES['cover_upload']['name'] != ''){
					$this->upload('cursos',$id,$this->input->post('titulo_spanish'),'cover_upload','cover_image');
				}
				//Una vez subidas las imagenes se reedirecciona a la url
				redirect(base_url()."admin/cursos");
			}
		}
		//Si no hay una peticion POST entonces mostrarmos la vista de crear
		$data["data_posted"] = posted();
		$view["body"] = $this->load->view('admin/cursos/crearPost',$data,true);
		$this->parser->parse("admin/template/body",$view);
	}

	public function curso_delete($id=null){
		if($id==null){
			echo "0";
		} else {
			$this->Cursos->delete($id);
			echo "1";
		}
	}










	public function clientes($num_page = 1){

		$num_page--;
        $num_post = $this->Clientes->count_post_admin();
        $last_page = ceil($num_post / PAGE_SIZE);

        if ($num_page < 0) {
            $num_page = 0;
        } elseif ($num_page > $last_page) {
            // TODO
            $num_page = 0;
        }

        $offset = $num_page * PAGE_SIZE;
		
		$data['last_page'] = $last_page;
		$data['current_page'] = $num_page;
		$data['last_page'] = $last_page;
		$data['pagination'] = true;
		$data['token_url'] = 'admin/clientes/';
		$data["posts"] = $this->Clientes->get_pagination_admin($offset,'admin');
		//$data['main_eventos'] = $this->Main_Post->get_main_eventos();
		$view["body"] = $this->load->view('admin/clientes/index',$data,true);
		$view["title"] = "Clientes";
		$this->parser->parse("admin/template/body",$view);
	}



	public function createExcel(){
		$fileName = 'clientes.xlsx';  
		$employeeData = $this->Clientes->todos();
		$spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
       	$sheet->setCellValue('A1', 'Id');
        $sheet->setCellValue('B1', 'Nombre');
        $sheet->setCellValue('C1', 'Telefono');
        $sheet->setCellValue('D1', 'Correo');   
        $rows = 2;
        foreach ($employeeData as $val){
            $sheet->setCellValue('A' . $rows, $val->id);
            $sheet->setCellValue('B' . $rows, $val->nombre);
            $sheet->setCellValue('C' . $rows, $val->telefono);
            $sheet->setCellValue('D' . $rows, $val->correo);
            $rows++;
        } 

        $writer = new Xlsx($spreadsheet);
		$writer->save("upload/".$fileName);
		header("Content-Type: application/vnd.ms-excel");
        redirect(base_url()."/upload/".$fileName);              
    } 













}
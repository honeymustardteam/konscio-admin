<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');


class Api extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Post');
        $this->load->helper('Post_helper');
        $this->load->helper('Date_helper');
        $this->load->helper('text');
        $this->load->model('Category');
        $this->load->library("parser");
        $this->load->model('Category_Post');
        $this->load->model('Main_Post');
        $this->optional_session_auto(1);
    }

    public function index($num_page = 1) {

        $num_page--;
        $num_post = $this->Post->count_posted();
        $last_page = ceil($num_post / PAGE_SIZE);

        if ($num_page < 0) {
            $num_page = 0;
        } elseif ($num_page > $last_page) {
            // TODO
            $num_page = 0;
        }

        $offset = $num_page * PAGE_SIZE;

        $data = $this->build_header('', '', '', base_url() . 'blog');

        $data['last_page'] = $last_page;
        $data['current_page'] = $num_page;
        $data['token_url'] = 'blog/';
        $data['posts'] = $this->Post->get_pagination($offset);
        $data['last_page'] = $last_page;
        $data['pagination'] = true;
        //TODO: Crear metodos a parte para obtener estos datos
        //$data['categories'] = $this->Category->findAll();
        //$data['main_posts'] = $this->Main_Post->get_main_posts();
        //$data['posts_most_seen'] = $this->Post->get_most_seen();
        
        //$view['main_posts'] = $this->load->view('blog/layouts/main_posts',$main_posts,TRUE);
        //$view['categories'] = $this->load->view("blog/categories", $data, TRUE);
        //$view['title_tag'] = "LO MÁS NUEVO";
        
        //$view['most_seen'] = $this->load->view('blog/layouts/most_seen',$most_seen,TRUE);
        //$view['body'] = $this->load->view("blog/posts", $data, TRUE);

        //$this->parser->parse("blog/template/body_blog_es", $view);
        echo json_encode($data);
    }
    //Devolver array de todas las categorias creadas
    public function categories(){
        echo json_encode($this->Category->findAll());
    }
    //Devolver array de los 3 posts principales
    public function main_posts(){
        echo json_encode($this->Main_Post->get_main_posts());
    }
    //Devolver array de los 3 posts mas visto
    public function posts_most_seen(){
        echo json_encode($this->Post->get_most_seen());
    }
    //Numero de paginacion de posts
    public function posts_count() {

        $num_post = $this->Post->count();
        $last_page = ceil($num_post / PAGE_SIZE);

        echo json_encode($last_page);
        //$view['body'] = $this->load->view("spa/utils/post_list", $data, TRUE);
        //$this->parser->parse("spa/template/body", $view);
    }
    //Info de un post en especifico
    public function post_view($clean_url) {

        if (ENVIRONMENT === 'production')
            $this->output->cache(PAGE_CACHE);

        if (strpos($this->uri->uri_string(), 'blog/post_view') !== false)
            show_404();

        if (!isset($clean_url)) {
            show_404();
        }

        $post = $this->Post->GetByUrlClean($clean_url);

        if (!isset($post)) {
            show_404();
        }

        $data = $this->build_header(APP_NAME . ' - ' . $post->title, $post->description, image_post($post->post_id), base_url() . $post->url_clean);
        //Aumentar numero de visitas
        $this->add_count($post->url_clean);
        $c_post = $this->Category_Post->get_categories_of_post($post->post_id);
        $data['post'] = $post;
        //$view['title'] = $post->title;
        //$view['subtitle'] = $post->subtitle;
        //$view['cover_image'] = $post->cover_image !=""? base_url()."/uploads/cover_post/".$post->cover_image :'';
        //$view['created_at'] = format_date($post->created_at);
        //$related_posts['related_posts'] = $this->Post->get_related_posts($clean_url,$post->c_url_clean);
        //Posts que tengan la misma categoría
        //TODO: Verificar que el algoritmo funcione con todas las categorias
        $data['related_posts'] = $this->get_related_posts($c_post,$post->post_id);
       
        //echo json_encode($c_post);

        //Categorias que tiene el post
        $array_categorias = [];
        foreach($c_post as $key => $c_p){
            $cat_temp = $this->Category->find($c_p->category_id);
            if(count($cat_temp)>0){
                array_push($array_categorias,$cat_temp);
            }
        }
        //$share['url_post'] = $post->url_clean;
        //$share['title'] = $post->title;
        //$view['share_post'] = $this->load->view('blog/layouts/share_post',$share,TRUE);
        $data['categories'] =  $array_categorias; //$this->Category_Post->get_categories_of_post($post->post_id);
        echo json_encode($data);
        //$view['categories'] = $this->load->view("blog/categories", $data, TRUE);
        //$view['related_posts'] = $this->load->view('blog/layouts/related_posts',$related_posts,TRUE);
        //$view['body'] = $this->load->view("blog/post_detail", $data, TRUE);
        //$this->parser->parse("blog/template/body_detail_blog_es", $view);
    }

    /* funciones privada */

    private function get_related_posts($c_post,$post_id){
        $related_posts;
        $array_related_posts = [];
        if(count($c_post>0)){
            //echo json_encode($c_post);
            //TODO:Hacer que esto no sea con foreach
            shuffle($c_post); 
            foreach($c_post as $key => $c){
                $category_id = $c->category_id;
            }
            $posts = (isset($category_id)) ? $this->Category_Post->get_posts_of_category($category_id,$post_id) : [];
            if(count($posts)>0){
                foreach($posts as $key=> $p){
                    //echo json_encode($posts);
                    $p_temp = $this->Post->find((int)$p->post_id);
                    if($p_temp->posted=="si"){
                        array_push($array_related_posts,$p_temp);
                    }
                    
                }
                $related_posts = $array_related_posts; //$this->Post->get_related_posts($category_id);
            } else {
                $related_posts = [];
            }
        } else {
            $related_posts = [];
        }
        return $related_posts;
    }

    // This is the counter function.. 
    private function add_count($slug) {
        // load cookie helper
        //$this->load->helper('cookie');
        if(isset($_COOKIE['read_articles'])) {
            //grab the JSON encoded data from the cookie and decode into PHP Array
            $read_articles = json_decode($_COOKIE['read_articles'], true);
             if(isset($read_articles[$slug]) AND $read_articles[$slug] == 1) {
               //this post has already been read
             } else {
          
               //increment the post view count by 1 using update queries
          
               $read_articles[$slug] = 1;
               //set the cookie again with the new article ID set to 1
               setcookie("read_articles",json_encode($read_articles),time()+60*60*24);  
               $this->Post->update_counter(urldecode($slug));
             }
          
          } else {
          
            //hasn't read an article in 24 hours?
            //increment the post view count
            $read_articles = Array(); 
            $read_articles[$slug] = 1;
            setcookie("read_articles",json_encode($read_articles),time()+60*60*24);      
            $this->Post->update_counter(urldecode($slug));
          }
        //$this->Post->update_counter(urldecode($slug));
    }

    private function build_header($title = '', $desc = '', $imgurl = '', $url = '') {
        // meta SEO
        $data['title'] = $title;
        $data['desc'] = $desc;
        $data['imgurl'] = $imgurl;
        $data['url'] = $url;

        return $data;
    }

}

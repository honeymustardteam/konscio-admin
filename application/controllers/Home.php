<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Home extends CI_Controller {



	public function __construct(){

		parent::__construct();

		$this->load->database();

        $this->load->helper('url');

		$this->load->library("parser");
		
		$this->load->library('session');

		//Helpers

		$this->load->helper('Post_helper');

		$this->load->helper('text');

		$this->load->helper('Date_helper');

		//Importamos los modelos
		$this->load->model('Cursos');
		$this->load->model('Post');
		$this->load->model('Eventos');
		$this->load->model('Testimonios');
		$this->load->model('Videos');
		//Lenguajes
		$this->lang->load("home","spanish");
		$this->lang->load("home","english");

		if(!$this->session->userdata('site_lang')){
        	$this->session->set_userdata('site_lang', 'spanish');
		}
		

	}

	public function index() {

		$offset = 1;

		$data['cursos'] = $this->Cursos->get_tresHome();

		$data['eventos'] = $this->Eventos->get_tresHome();

		$data['ses'] = $this->session->userdata('site_lang');
		$data["nombre_css"] = 'eventos.css';
		
		$view["head"] = $this->load->view('public/template/head',$data,true);
		$view["header"] = $this->load->view('public/template/header',$data,true);
		
		$view["content"] = $this->load->view('public/home',$data,true);
			
        $this->parser->parse('public/template/base',$view);
		//$this->session->set_userdata('site_lang');
		
	}






	public function acerca() {
		$this->session->userdata('site_lang');
		

		$data["nombre_css"] = 'autora.css';
		$data["nombre_css_2"] = 'libros.css';
		$data["nombre_css_3"] = 'testimonios.css';
		
		$data['posts_testimonios'] = $this->Testimonios->get_pagination(1);
		$data['ses'] = $this->session->userdata('site_lang');

		$view["head"] = $this->load->view('public/template/head',$data,true);
		$view["header"] = $this->load->view('public/template/header',$data,true);
		$view["content"] = $this->load->view('public/acerca',null,true);
		
		
        $this->parser->parse('public/template/base',$view);
	}


	public function konscio() {
		$this->session->userdata('site_lang');
		
		$data["nombre_css"] = 'acercadeKonscio.css';
		$view["head"] = $this->load->view('public/template/head',$data,true);
		$view["header"] = $this->load->view('public/template/header',$data,true);
		$view["content"] = $this->load->view('public/konscio',null,true);
		
        $this->parser->parse('public/template/base',$view);
	}


	
	// public function videos() {
	// 	$this->session->userdata('site_lang');
		
	// 	$data["nombre_css"] = 'acercadeKonscio.css';
	// 	$view["head"] = $this->load->view('public/template/head',$data,true);
	// 	$view["header"] = $this->load->view('public/template/header',$data,true);
	// 	$view["content"] = $this->load->view('public/videos',null,true);
		
    //     $this->parser->parse('public/template/base',$view);
	// }

	public function videos($num_page = 1) {
		$this->session->userdata('site_lang');

		$num_page--;

        $num_post = $this->Videos->count_posted();

        $last_page = ceil($num_post / PAGE_SIZE);

        if ($num_page < 0) {

            $num_page = 0;

        } elseif ($num_page > $last_page) {
            // TODO
            $num_page = 0;
        }
		$offset = $num_page * PAGE_SIZE;

		$data['posts_videos'] = $this->Videos->get_pagination($offset);
		$data['last_page'] = $last_page;
        $data['current_page'] = $num_page;
		$data['token_url'] = 'videos/';
		$data['pagination'] = true;
		//Traduccion cargadas desde la carpeta Language y su respectivo lenguaje(carpeta)
		$data['ses'] = $this->session->userdata('site_lang');
		//headerbar traducido

		$data["nombre_css"] = 'acercadeKonscio.css';
		$view["head"] = $this->load->view('public/template/head',$data,true);
		$view["header"] = $this->load->view('public/template/header',$data,true);
		$view["content"] = $this->load->view('public/videos',null,true);
		
        $this->parser->parse('public/template/base',$view);
	}







	public function libros() {
		$this->session->userdata('site_lang');
		
		
		$data["nombre_css"] = 'libros.css';
		$view["head"] = $this->load->view('public/template/head',$data,true);
		$view["header"] = $this->load->view('public/template/header',$data,true);
		$view["content"] = $this->load->view('public/libros',null,true);
		
        $this->parser->parse('public/template/base',$view);
	}

	public function cursos_talleres($num_page=1) {
		$this->session->userdata('site_lang');

		$num_page--;

        $num_post = $this->Cursos->count_posted();

        $last_page = ceil($num_post / PAGE_SIZE);

        if ($num_page < 0) {

            $num_page = 0;

        } elseif ($num_page > $last_page) {
            // TODO
            $num_page = 0;
        }
		$offset = $num_page * PAGE_SIZE;

		$data['posts'] = $this->Cursos->get_pagination($offset);
		$data['last_page'] = $last_page;
        $data['current_page'] = $num_page;
		$data['token_url'] = 'cursos/';
		$data['pagination'] = true;
		//Traduccion cargadas desde la carpeta Language y su respectivo lenguaje(carpeta)
		$data['ses'] = $this->session->userdata('site_lang');
		//headerbar traducido


		
		
		$data["nombre_css"] = 'cursos_y_talleres.css';
		$view["head"] = $this->load->view('public/template/head',$data,true);
		$view["header"] = $this->load->view('public/template/header',$data,true);
		$view["content"] = $this->load->view('public/cursos_talleres',null,true);
		
        $this->parser->parse('public/template/base',$view);
	}

	public function eventos($num_page = 1) {
		$this->session->userdata('site_lang');

		$num_page--;

        $num_post = $this->Eventos->count_posted();

        $last_page = ceil($num_post / PAGE_SIZE);

        if ($num_page < 0) {

            $num_page = 0;

        } elseif ($num_page > $last_page) {
            // TODO
            $num_page = 0;
        }
		$offset = $num_page * PAGE_SIZE;

		$data['posts'] = $this->Eventos->get_pagination($offset);
		$data['posts_videos'] = $this->Videos->get_pagination(0);
		$data['last_page'] = $last_page;
        $data['current_page'] = $num_page;
		$data['token_url'] = 'eventos/';
		$data['pagination'] = true;
		//Traduccion cargadas desde la carpeta Language y su respectivo lenguaje(carpeta)
		$data['ses'] = $this->session->userdata('site_lang');
		//headerbar traducido






		
		
		$data["nombre_css"] = 'eventos.css';
		$view["head"] = $this->load->view('public/template/head',$data,true);
		$view["header"] = $this->load->view('public/template/header',$data,true);
		$view["content"] = $this->load->view('public/eventos',null,true);
		$view["content2"] = $this->load->view('public/videos',null,true);
		
		
        $this->parser->parse('public/template/base',$view);
	}

	public function testimonios($num_page=1) {
		$this->session->userdata('site_lang');

		$num_page--;

        $num_post = $this->Testimonios->count_posted();

        $last_page = ceil($num_post / PAGE_SIZE);

        if ($num_page < 0) {

            $num_page = 0;

        } elseif ($num_page > $last_page) {
            // TODO
            $num_page = 0;
        }
		$offset = $num_page * PAGE_SIZE;

		$data['posts'] = $this->Testimonios->get_pagination($offset);
		$data['last_page'] = $last_page;
        $data['current_page'] = $num_page;
		$data['token_url'] = 'testimonios/';
		$data['pagination'] = true;
		//Traduccion cargadas desde la carpeta Language y su respectivo lenguaje(carpeta)
		$data['ses'] = $this->session->userdata('site_lang');
		//headerbar traducido
		
		
		$data["nombre_css"] = 'testimonios.css';
		$view["head"] = $this->load->view('public/template/head',$data,true);
		$view["header"] = $this->load->view('public/template/header',$data,true);
		$view["content"] = $this->load->view('public/testimonios',null,true);
		
        $this->parser->parse('public/template/base',$view);
	}

	public function preguntas_frecuentes() {
		$this->session->userdata('site_lang');
		
		
		$data["nombre_css"] = 'preguntas_frecuentes.css';
		$view["head"] = $this->load->view('public/template/head',$data,true);
		$view["header"] = $this->load->view('public/template/header',$data,true);
		$view["content"] = $this->load->view('public/preguntas_frecuentes',null,true);
		
        $this->parser->parse('public/template/base',$view);
	}




	
	public function blogs($num_page = 1) {
		$this->session->userdata('site_lang');
		$num_page--;

        $num_post = $this->Post->count_posted();

        $last_page = ceil($num_post / PAGE_SIZE);



        if ($num_page < 0) {

            $num_page = 0;

        } elseif ($num_page > $last_page) {

            // TODO

            $num_page = 0;

        }



		$offset = $num_page * PAGE_SIZE;



		$data['posts'] = $this->Post->get_pagination($offset);

		$data['last_page'] = $last_page;

        $data['current_page'] = $num_page;

		$data['token_url'] = 'blogs/';

		$data['pagination'] = true;

		//Traduccion cargadas desde la carpeta Language y su respectivo lenguaje(carpeta)
		$data['ses'] = $this->session->userdata('site_lang');
		//headerbar traducido


		$data["nombre_css"] = 'blog.css';
		$view["head"] = $this->load->view('public/template/head',$data,true);
		$view["header"] = $this->load->view('public/template/header',$data,true);
		$view["content"] = $this->load->view('public/blogs',null,true);
		
        $this->parser->parse('public/template/base',$view);
	}

	
	
	
	
	
	
	
	public function articulo($clean_url){
		if (!isset($clean_url)) {

            show_404();

        }

		$post = $this->Post->GetByUrlClean($clean_url);

        if (!isset($post)) {

            show_404();

		}
		$data['ses'] = $this->session->userdata('site_lang');
		$data['post'] = $post;
		$data["nombre_css"] = 'articulo.css';
		$view["head"] = $this->load->view('public/template/head',$data,true);
		$view["header"] = $this->load->view('public/template/header',$data,true);
		$view['content'] = $this->load->view("public/articulo", $data, TRUE);

        $this->parser->parse('public/template/base',$view);
	}







}
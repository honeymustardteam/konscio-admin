<?php
class LangSwitch extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
    }

    function switchLanguage($language = "") {
        $language = ($language != "") ? $language : "spanish";
        $this->session->set_userdata('site_lang', $language);
        //redirect('https://proyectoshm.com/conciencia-evolucion');
        //redirect(base_url());  //este es local  
        redirect($_SERVER['HTTP_REFERER']);
    }
}
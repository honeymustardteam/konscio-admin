<?php

defined('BASEPATH') or exit('No direct script access allowed');


class VideosInicio extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->database();

        //Importamos helpers
        $this->load->helper('url');

        //Importamos los modelos
        $this->load->model('Videos');

    }

    /*****************************************************
     **********************Posts**********************
     *****************************************************/

    public function index()
    {
        $response = $this->Videos->get_all();
        echo json_encode($response);
    }



}

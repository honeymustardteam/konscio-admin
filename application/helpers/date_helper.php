<?php 

function format_date($date){
	setlocale(LC_TIME,"en_US");
	$date = new DateTime($date);
	//return $date->format("d/m/yy");

	return $date->format('jS \of F Y ');
}

function format_date_espa($date){
	date_default_timezone_set('UTC');
    date_default_timezone_set("America/Mexico_City");
    //$hora = strftime("%I:%M:%S %p", strtotime($timestamp));Descomentar en caso de requerir hora
    setlocale(LC_TIME, 'es_ES');
    $fecha = utf8_encode(strftime("%d de %B de %Y", strtotime($date)));
    return $fecha;
}
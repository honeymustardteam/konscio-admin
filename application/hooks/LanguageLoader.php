<?php
class LanguageLoader
{
    
    function initialize() {
        

        $CI =& get_instance();
        $CI->load->helper('language');

        $site_lang = $CI->session->userdata('site_lang');
        if ($site_lang) {
            $CI->lang->load('home',$site_lang);
        } else {
            $CI->lang->load('home','spanish');
        }
    }
}
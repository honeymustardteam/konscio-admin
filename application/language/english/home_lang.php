<?php
$lang["menu"]["inicio"] = "Home";
$lang["menu"]["acerca_de"] = "About the Author";
$lang["menu"]["terapias"] = "Therapies";
$lang["menu"]["testimonios"] = "Testimonials";
$lang["menu"]["cv"] = "Accreditations";
$lang["menu"]["entrevistas"] = "Interviews";
$lang["menu"]["congresos"] = "Conferences";
$lang["menu"]["que_es"] = "What is the Konscio Method?";
$lang["menu"]["videos"] = "Videos";
$lang["menu"]["libros"] = "Books";
$lang["menu"]["cursos_talleres"] = "Courses & Workshops";
$lang["menu"]["eventos"] = "Events";
$lang["menu"]["blog"] = "Blog";
$lang["menu"]["preguntas_freq"] = "FAQ's";
$lang["menu"]["contacto"] = "Contact";


$lang["home"]["video1"] = "https://player.vimeo.com/video/659158117?h=790d299dea";
$lang["home"]["txt_1"] = "The next step in the evolution of the human being is one of consciousness.";
$lang["home"]["txt_2"] = "SEE THERAPIES";
$lang["home"]["txt_3"] = "What is the Konscio Method?";
$lang["home"]["txt_4"] = "The word";
$lang["home"]["txt_5"] = "Konscio";
$lang["home"]["txt_6"] = "is the Greek term for consciousness. The Greeks were the first to speak of consciousness";
$lang["home"]["txt_7"] = " per se. ";
$lang["home"]["txt_8"] = "Konscio here is used to mean the awareness of ourselves for what we are: spiritual beings living a human experience, part of Source Konscio. To be aware of this, we must expand our consciousness to all the extent of our being, to the vast, magnificent, immortal being of light that we truly are.  ";
$lang["home"]["txt_9"] = "The Konscio Method’s";
$lang["home"]["txt_10"] = "is to help bring the consciousness of humanity to its intrinsic truth. To remember who we are, in essence, we need to clean the debris that has clouded our perception of our magnificence. ";
$lang["home"]["txt_11"] = "We need to heal our negative beliefs and emotions and the situations that created them. As we do, our consciousness naturally expands, we become increasingly aware of our true essence of light and love.";
$lang["home"]["txt_12"] = "THE FOUR LEVELS OF KONSCIO";
$lang["home"]["txt_13"] = "Egoic";
$lang["home"]["txt_14"] = "Empathic";
$lang["home"]["txt_15"] = "One";
$lang["home"]["txt_16"] = "Source";
$lang["home"]["txt_17"] = "KNOW MORE";
$lang["home"]["txt_18"] = "COURSES AND WORKSHOPS";
$lang["home"]["txt_19"] = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean <br>commodo ligula eget dolor. Aenean massa. ";
$lang["home"]["txt_20"] = "Sign up";
$lang["home"]["txt_21"] = "EVENTS";
$lang["home"]["txt_22"] = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean";


$lang["acerca"]["txt_1"] = "About Milly Diericx Ph.D";
$lang["acerca"]["txt_3"] = "She is an energy therapist and a hypnotherapist. She arrived at these techniques after going through all conventional options for healing her Lupus Erythematosus Systemicus (an autoimmune disease). Traditional medicine only offered ways to diminish the symptoms, causing all kinds of side effects, but no real solutions. She refused to give up on life and searched for alternative options, the ones that worked she studied and now she offers them to anyone who is interested, having tested their effectiveness on herself.";
$lang["acerca"]["txt_4"] = "After alternative healing techniques, she became interested in the workings of the deep mind. She thus studied a Masters in Ericksonian Psychotherapy, and then specialized in Past Life Regressions, Life Between Lives® and Quantum Consciousness. This profoundly spiritual work gives great clarity about the self, life’s purpose and mission, and the agreements that brought us to our present life story. She later immersed in the grief process through a Ph.D in Thanatology.";
$lang["acerca"]["txt_4b"] = "Si quieres hacer una cita, por favor contáctanos";
$lang["acerca"]["txt_5"] = "She now continues her trajectory with  ";
$lang["acerca"]["txt_6"] = "The Konscio Method™  ";
$lang["acerca"]["txt_7"] = "in which she has compiled all her knowledge and experience, using different methods to heal the levels of consciousness, helping people expand their consciousness and reach ever higher levels of coherence, inner peace and wellbeing.  ";


$lang["terapias"]["txt_1"] = "Therapies";
$lang["terapias"]["txt_2"] = "The Konscio Method™";
$lang["terapias"]["txt_3"] = "The Konscio Method is designed to expand consciousness by healing the obstacles the person has to becoming the magnificent being of light they really are. In the first consultation, we do a holistic diagnosis using state of the art technology to assess the physical, energetic and emotional state of the person. After, an intention is set together with the consultant about their journey and a program of sessions to achieve the set goals. The treatment includes techniques like: ";
$lang["terapias"]["txt_4"] = "Resonance Repatterning ®";
$lang["terapias"]["txt_5"] = "Is a method developed by Chloe Wordsworth based in the new science that discovered that everything in the Universe is energy manifesting in frequency patterns, including our physical, emotional and mental problems. By using specific energetic healing modalities, the negative resonance with our problems can be changed to positive resonance with the things we want in life. This revolutionary method achieves incredible change in the negative patterns that cause us suffering, replacing them with the behavior and attitudes that bring us satisfaction, well-being and joy. ";
$lang["terapias"]["txt_6"] = "Ericksonian Psychotherapy";
$lang["terapias"]["txt_7"] = "It is a technique developed by Milton H. Erickson, considered the father of modern hypnotherapy. It is an easy way to access the subconscious and unconscious mind, resolving the existential knots that make us fall into depression, suffering and hopelessness. Having resolved these knots we return easily to the natural way we are, which is curious, lively and tending towards general wellbeing.";
$lang["terapias"]["txt_8"] = "Life Between Lives Hypnotherapy ®";
$lang["terapias"]["txt_9"] = "This method of deep hypnosis developed by Dr. Michael Newton, PhD., founder of the Michael Newton Institute, gives us the possibility of opening the deepest memories, beyond the unconscious, into the super-conscious mind, where the soul memories are recorded. Accessing these memories brings us clarity about our real, immortal and eternal spiritual being we call our soul.";
$lang["terapias"]["txt_10"] = "Allergy Antidotes®";
$lang["terapias"]["txt_11"] = "A therapy focused on alleviating food and substance allergies and intolerances. It uses Traditional Chinese Medicine meridians to reduce immune system reactions to substances and foods that cause allergic reactions.";
$lang["terapias"]["txt_12"] = "Quantum Consciousness and Universal Consciousness";
$lang["terapias"]["txt_13"] = "Method created by Peter Smith. Consciousness expansion that leads a person to a profound self-knowledge through journey into  the different planes and dimensions of the soul. With this technique you can resolve problems from the past, as well as knowing the mission and purpose of the soul in the present incarnation.";
$lang["terapias"]["txt_14"] = "Jin Shin Jyutsu";
$lang["terapias"]["txt_15"] = "Energetic therapy brought to the west by Mary Burmeister, based on the idea that the blueprint for the human body is perfect, and throughout life there are blockages to the energy flows that obstruct the proper function of the body. Through touching certain points on the body, the flow is reestablished recalibrating optimal bodily functions.";
$lang["terapias"]["txt_16"] = "Reiki";
$lang["terapias"]["txt_17"] = "Energy transmission method using the placement of the hands on the body. When it receives the extra healing energy, the body can perform its natural healing processes faster and more efficiently.";
$lang["terapias"]["txt_18"] = "Past Life Regressions®";
$lang["terapias"]["txt_19"] = "Based on Brian Weiss’ method, it’s aeep hypnosis that accesses the memories of the soul and the different bodies and times it has incarnated in. Knowing about our past lives helps us heal situations in this life that have no rational explanation with the experiences lived here and now.";
$lang["terapias"]["txt_20"] = "Medical Biomagnetism®";
$lang["terapias"]["txt_21"] = "Dr. Issac Goiz discovered that microorganisms tend to live in complementary pairs, which he calls the Biomagnetic Pair®. By placing magnets in certain points on the body, neutral Ph is reestablished which is conducive to rapid healing of both microorganisms, propitiating optimal health.";
$lang["terapias"]["txt_22"] = "Astrology";
$lang["terapias"]["txt_23"] = "Astrology is a millenary art that has been compiled through the observation of human behavior and the movement of celestial bodies and their interrelations. Through these observations, the astrological method was created, bringing useful information about the person, their personality, their behavioral tendencies and their health.";
$lang["terapias"]["txt_24"] = "Quantum Bio-reprogramming™";
$lang["terapias"]["txt_25"] = "A person’s aura is cleansed of energetic debris through kinesiology. By cleaning the auric field, circumstances of life are considerably improved.";
$lang["terapias"]["txt_26"] = "Spiritual Alchemy";
$lang["terapias"]["txt_27"] = "Based on Carl Jung’s interpretation of the ancient art of Alchemy, it is the transformation of the human spirit from its base matter, associated with lead, through various steps in a process of transmutation reaching the spirit’s sublimation, associated to gold.";


$lang["publicaciones"]["txt_1"] = "Publications";
$lang["publicaciones"]["txt_2"] ="U.S. News Article: What It's Like to Be Diagnosed With a Chronic Illness 'Your body has become a sort of prison from which you can never escape, except through death.' By Milly Diericx | Contributor Nov. 2, 2016, at 6:00 a.m.";
$lang["publicaciones"]["txt_3"] ="U.S. News Article: 4 Keys to Coping With the Emotions of a Chronic Illness Diagnosis First on the list: acceptance and getting to know your new self. By Milly Diericx | Contributor Nov. 4, 2016, at 10:27 a.m.";
$lang["publicaciones"]["txt_4"] ="U.S. News Article: 5 Daily Steps to Combat the Depression That Comes With Chronic Illness Hope, positive thinking and gratitude make the list. By Milly Diericx | Contributor Dec. 14, 2016, at 6:00 a.m.";
$lang["publicaciones"]["txt_5"] ="Mind Body Green, The Only 3 Questions You Need To Ask Yourself The Next Time You're Stressed Out,by Milly Diericx October 31, 2016.";
$lang["publicaciones"]["txt_6"] ="Willowbay.com, Article about Befriending the Wolf: How One Therapist Found Health By Befriending Her Incurable Disease Quick Adsense WordPressPlugin: http://quicksense.net/ 2a ";
$lang["publicaciones"]["txt_7"] ="Lupus Foundation of America, Lupus Awareness Month, Article on Befiending the Wolf: The Guide to Living and Thriving with Lupus. By Migdalia Fernandez,  27 de Enero, 2017.";
$lang["publicaciones"]["txt_8"] ="Mariah Nichols, Voices from the Disability Community, Milly Diericx, 7 March, 2017.";



$lang["videos"]["video1"] = "https://player.vimeo.com/video/675569222?h=5016b6620a";



$lang["konscio"]["txt_1"] = "What is the Konscio Method?";
$lang["konscio"]["txt_2"] = "What is the Konscio Method?";
$lang["konscio"]["txt_3"] = "The four levels of Konscio ";
$lang["konscio"]["txt_4"] = "How to reach them";
$lang["konscio"]["txt_5"] = "Why reach them?";
$lang["konscio"]["txt_6"] = "Results";
$lang["konscio"]["txt_7"] = "What is the Konscio Method?";
$lang["konscio"]["txt_8"] = "The word Konscio";
$lang["konscio"]["txt_9"] = "";
$lang["konscio"]["txt_10"] = "is the Greek term for consciousness. The Greeks were the first to speak of consciousness";
$lang["konscio"]["txt_11"] = " per se. ";
$lang["konscio"]["txt_12"] = "Konscio here is used to mean the awareness of ourselves for what we are: spiritual beings living a human experience, part of Source Konscio. To be aware of this, we must expand our consciousness to all the extent of our being, to the vast, magnificent, immortal being of light that we truly are. ";
$lang["konscio"]["txt_13"] = "The Konscio Method’s ";
$lang["konscio"]["txt_14"] = " goal is";
$lang["konscio"]["txt_15"] = "is to help bring the consciousness of humanity to its intrinsic truth. To remember who we are, in essence, we need to clean the debris that has clouded our perception of our magnificence. We need to heal our negative beliefs and emotions and the situations that created them. As we do, our consciousness naturally expands, we become increasingly aware of our true essence of light and love.";
$lang["konscio"]["txt_16"] = "The four levels of Konscio ";
$lang["konscio"]["txt_17"] = "I have divided consciousness, Konscio, into four levels to make their attainment easier. These levels are chosen from my own experience of them, for Konscio is a continuum and not divided in stages; however, taking it in steps makes it more accessible. The Konscio Method is to have a healthy first level to reach the second, a healthy second to reach the third, and if we are lucky, level four will manifest itself.";
$lang["konscio"]["txt_18"] = "Egoic";
$lang["konscio"]["txt_19"] = "Level One,";
$lang["konscio"]["txt_20"] = "Egoic Konscio.";
$lang["konscio"]["txt_21"] = "This is the waking consciousness we are familiar with. Here we have a body, an identity, a gender, a name, in short, an Ego. This level is the one that lets us navigate this life with all that is required of it, to belong to a society, to work, to know who we are. This level is healed when we can accept ourselves exactly as we are, accept our life story exactly as it is, that is, love ourselves unconditionally.";
$lang["konscio"]["txt_22"] = "Level Two,";
$lang["konscio"]["txt_23"] = "Empathic Konscio.";
$lang["konscio"]["txt_24"] = "It is my belief and hope that humanity is in the process of shifting to this level, and that's what we have to do, I think, to survive the present crises we are experiencing (environmental, sanitary, economic and political). This level lets us empathize with one another and with all the species with whom we share our home.";
$lang["konscio"]["txt_25"] = "Empathy is not putting the interests of others before our own. It means connecting to the wisdom of the heart and making every choice asking, 'What impact will this have on other people, species, the planet?' and choosing to do the least harm possible through our actions. It needs a healthy Ego, a resolved first level, so that we are not acting out of greed, guilt, a feeling of being less than or more than anyone/anything else, or wanting to be the savior, but just from being us, an essential part of the Whole. At this level, we feel connected to everyone and everything around us, we expand the unconditional love we attained for ourselves in level one to every being that we share the earth with.";
$lang["konscio"]["txt_26"] = "Empathic";
$lang["konscio"]["txt_27"] = "One ";
$lang["konscio"]["txt_28"] = "Level three,";
$lang["konscio"]["txt_29"] = "One Konscio.";
$lang["konscio"]["txt_30"] = "At this level, we expand beyond being connected to everyone and everything to being One with everything there is. Religions and spiritual paths have created methods to attempt to get to this level. Here we experience oneness, enlightenment, peace. We expand the unconditional love we now feel for every species and make it deeper, so that we are now part of everything. Here love ceases to be a verb, something we do for ourselves or others, here it becomes a state of being, we start being love energy, connected to everything there is.";
$lang["konscio"]["txt_31"] = "Level Four,";
$lang["konscio"]["txt_32"] = "Source Konscio.";
$lang["konscio"]["txt_33"] = "At this level, we become aware that we are the Source Itself; we experience being Divine Consciousness. There are no proven methods to reach this level, the people who have touched it have done so spontaneously, in a moment without time. Having attained the three other levels prepares you to connect on this very sacred consciousness level, although nothing can guarantee that you will. You have to be ready and open to the experience. Here you experience absolute peace, absolute love, stillness, and nothingness. Here there is no more identity, but you are the Source of everything there is, pure Source Konscio, absolute, complete unconditional love, the actual Source of this loving energy that underlies everything.";
$lang["konscio"]["txt_34"] = "Source";
$lang["konscio"]["txt_35"] = "How to reach them";
$lang["konscio"]["txt_36"] = "Level One, Egoic Konscio. ";
$lang["konscio"]["txt_37"] = "The one that needs the most work is Level One, for here are all our traumas, hurts, suffering, negative beliefs, and emotions. The first step is to heal this level, and the second level comes into being quite naturally. If you're still trying to be empathic and it's not working, there are still issues to resolve in level one. The methods to heal this level are: physical, having healthy behaviors that are healthy for our bodies; emotional, learning to deal with, resolve and channel our emotional states; and mental, healing the limiting beliefs we have about ourselves. There are many different types of therapies designed to target these aspects, I recommend and use with my clients, those that I have experimented with and have worked for me.";
$lang["konscio"]["txt_38"] = "Level Two, Empathic Konscio.";
$lang["konscio"]["txt_39"] = "Once empathy comes naturally to you, and you pass every decision through the heart and cause the least damage to that which surrounds you, and on the contrary, you feel happy and fulfilled when you are helping others, then you have attained level two. There is usually a lot of going back and forth from level one to level two. There's no blame in this, consciousness starts expanding as you heal, and it contracts again when there is another issue or layer that comes up for healing. It is a process, and we must be compassionate with ourselves through it. Here there is a lot of heart healing necessary. Our heart is the energy center -chakra- of level two, and if it's been broken or hurt through abuse or emotional trauma, we have to heal that before we can attain this level. We need to forgive every one that hurt us, to be able to experience unconditional love towards us, and towards them, see them with compassion and understanding.";
$lang["konscio"]["txt_40"] = "Level three, One Konscio.";
$lang["konscio"]["txt_41"] = "Once empathy is second nature to us, and there is no effort involved in passing choices through the heart, our meditation or spiritual practices will effortlessly bring us into One Konscio. We will want to spend more and more time at this level, for we are at peace here. You have attained this level when you can live your regular life without losing the One perspective, when you don't contract to lower levels anymore if things happen that used to trigger you. As long as you continue to react, there are more things to heal in levels one and two. Again, it's O.K. This is a journey, not a destination. When you are at this level for most of the time effortlessly, you have attained One Konscio; you are enlightened and ready for level four to manifest. Being enlightened means being aware of the fact that we are all One, that there is no separation.";
$lang["konscio"]["txt_42"] = "Level four, Source Konscio. ";
$lang["konscio"]["txt_43"] = "It is elusive and also the easiest of them if you're ready. The people who have reported touching it have done so spontaneously. It has come to people with no spiritual training, and it has eluded saints. The thing is, if you're not ready for it, it can be overwhelming, so I think it's better to do it step by step and let the experience come to you when you're ready for it. Source Consciousness is the easiest because it is our true essence, we are all Source Konscio, so it’s like coming home. Source Konscio is in every particle of creation, in the space between atoms, keeping galaxies together. Becoming it is just being aware of this, that all there is, is Source Konscio in infinite forms.";
$lang["konscio"]["txt_44"] = "Why reach them?";
$lang["konscio"]["txt_45"] = "This process is clearly not for everyone. The step from level one to level two is, for me, inevitable in the long run as the only way to survive as a species, as the next step in human evolution. However, the ones that will willingly take the journey are special souls, those who want to be part of the solution. Those brave empathic souls are waking up to Empathic Konscio. Together, we will create a critical mass, enough individuals acting, feeling, and living differently to create a new reality so that others can join us. There will be less and less division, aggression, injustice, war, us versus them, much more respect for each other and nature, for the whole world, in this new reality. ";
$lang["konscio"]["txt_46"] = "Levels three and four are for those who feel they want something more, who have felt the calling of the spiritual world, old souls, ready to get back to Source Konscio. This is why I have divided my work into two books. The first is dedicated to the shift from level one to level two. This is enough for most of us; just this shift will bring the new humanity into being. The second book is much more profound, intended for spiritual seekers who are serious about going into their being's depth and breadth. These levels are a personal journey at this stage of our development. We will reach them as a whole later in our story. First, we have to pass the test of shifting into level two as a society!";
$lang["konscio"]["txt_47"] = "Results";
$lang["konscio"]["txt_48"] = "The results of doing this work are very pleasurable. Healing Egoic Konscio brings about peace of mind, openness of the heart, unconditional love for ourselves, less suffering, a clear goal, a personal value system, and the acquisition of the things our hearts desire in life, and thus joy. Here we feel truly grateful for who we are, for our story, for being us.";
$lang["konscio"]["txt_49"] = "Healing Empathic Konscio brings unconditional love for all beings, forgiveness, peace of heart, and joy in ourselves and our relationships. Here we are grateful for the people in our lives, and we become active in bringing about the type of society we all yearn for, a more just, loving, compassionate way of life.";
$lang["konscio"]["txt_50"] = "Attaining level three, One Konscio, gives us a feeling of belonging that we haven't dreamed of yet. We are part of the Whole, and here we become entirely aware of it at all times. This awareness brings complete inner peace, bliss, unconditional love for all things, for we know we are all One.";
$lang["konscio"]["txt_51"] = "Level four, Source Konscio, is absolute bliss. The bliss that comes from being nothing and everything at the same time, from perfect stillness, from being the Source of love, life, creation.";
$lang["konscio"]["txt_52"] = "It is a journey filled with setbacks, for we have accumulated so many hurts and issues that resolving all of them takes time and commitment. However, there is the highest prize imaginable at the end of the journey, coming back to Source consciously, willingly returning to our true essence. For Buddhists, it is called attaining Nirvana: when we become one with everything there is, dissolving back into Source Konscio and ceasing the karmic wheel";
$lang["konscio"]["txt_53"] = "Konscio";





$lang["libros"]["txt_1"] = "BOOKS";
$lang["libros"]["txt_2"] = "Throughout my experience, I have written the following books";

$lang["libros"]["txt_3"] = "The Konscio Method";
$lang["libros"]["txt_4"] = "I have divided my work into two books: This first one deals with healing of the first and second Konscio levels. This shift in consciousness is what humanity needs to survive the troubled times in which we are living.";
$lang["libros"]["txt_5"] = "This book is for those who want to be part of the solution, spearheading the shift of consciousness. We need to build a critical mass of individuals who think and act differently to bring a more evolved state of consciousness into mainstream culture.
The second book deals with the third and fourth Konscio levels and is meant for people with transcendental aspirations.  
Konscio is the Greek term for consciousness. The Greeks were the first to speak of consciousness";
$lang["libros"]["txt_6"] = " per se, ";
$lang["libros"]["txt_7"] = "although in ancient times, they used the word ";
$lang["libros"]["txt_8"] = " psyche, ";
$lang["libros"]["txt_9"] = "which included the spiritual and mental aspects. Konscio is used here to refer to the awareness of ourselves for what we really are: spiritual beings living a human experience. To be aware of this, we have to be able to expand our consciousness to the full extent of our being—to the huge, magnificent, immortal beings of light that we truly are. The Konscio project is to help bring the consciousness of humanity to its truth.
To make this dive into consciousness easier to explain, I have chosen to divide it into four major levels. Each one could be subdivided in myriad ways, but for me, these four broad categories are clear. So, let's think of ourselves as a Matryoshka doll—a series of four nested dolls, one inside the other. We expand from the first and smallest doll, Egoic Konscio, which describes our physical bodies and the lives we are living; to Empathic Konscio, the next larger doll that houses our emotional and mental bodies and connects us with other human beings, their emotions, and minds; to One Konscio, the next size up, which houses our spiritual self, or spiritual body, and connects us through the Universal Field to all things, enabling us to know we are One; to the largest doll, the Source Konscio, which is no longer housed in the body and does not constitute a body,";
$lang["libros"]["txt_10"] = " per se. ";
$lang["libros"]["txt_11"] = "It is the knowledge that we are already a part of Source. I have led myself and my clients through these consciousness levels, finding treasures of ever-growing meaning in each. I have only touched Source Konscio once, and it was a profoundly life-altering experience.";
$lang["libros"]["txt_21"] = "Diericx, Milly, The Konscio Method: Heal. Expand. Evolve. KDP, Amazon, 2022.";
$lang["libros"]["txt_23"] = "Buy now";

$lang["libros"]["txt_12"] = "Befriending The Wolf";
$lang["libros"]["txt_13"] = "My body had become my enemy. It was slowly but surely killing me relentlessly. All I could feel was emptiness in my gut, a sense of hoplesness so profound that it was a black hole, swallowing everything else… I had no feelings whatsoever, I could not be angry or worried or afraid. I could not love, and I could not care for anyone, not even myself…";
$lang["libros"]["txt_14"] = "My life had absolutley no purpose: many times all I wanted was for my treacherous body to get on with it and finish the job. The pain was horrendous. My skin chaffed between the sheets. I could not move at all. I was helpless as a newborn mouse, exposed, vulnerable, weak, in the dark. 
…my attention was completely taken over by a single thought: I would rather die than live like this…";
$lang["libros"]["txt_22"] = "Diericx, Milly, Befriending the Wolf: The Guide to Living and Thriving with Lupus, Morgan James, N.Y., 2016.";
$lang["libros"]["txt_24"] = "Buy now";

$lang["libros"]["txt_15"] = "Be Love: A Conscious Shift to Birthing the Future";
$lang["libros"]["txt_16"] = "I found that balance is key in order to be healthy, because if one of the aspects of your life is out of balance, then everything starts to go wrong. That’s the Traditional Chinese Medicine vision of health—everything has to be in balance. That’s what I did with all these techniques. With patience and dedication, I was able to balance my emotions, thought patterns and, of course, my body—the body and everything that it needs to be well—and also, the complete being, which includes the spiritual aspect…";
$lang["libros"]["txt_17"] = "When you can say a person has healed, that healing has to do with the whole, meaning the body, the mind, the emotions, and the soul. That’s when a person can access the whole of their own power, their own healing, and their own meaning and purpose in life, and actually go for it and manifest what they came to do in this life, which is amazing to watch…
This idea is that every single one of us is a powerful being and that we can do it is fundamental. Whatever we put our minds to, we can do at a personal level, at a social level, and at a planetary level. All the decisions we have come to, we have agreed on in a way, and we all do it…
This is the legacy I want to leave: We are all incredibly powerful beings; everything we put our minds to can be done. Just that idea in all our minds is enough to start the chain reaction of change. ";

$lang["libros"]["txt_18"] = "When buying the Konscio Method Book get a free workbook. Click here.";
$lang["libros"]["txt_19"] = "Your information";
$lang["libros"]["txt_20"] = "Attach a screenshot of your proof of purchase at Amazon of The Konscio Method and you will receive on your email a Workbook absolutely free. ";



$lang["cursos_talleres"]["txt_1"] = "Courses & Workshops";
$lang["cursos_talleres"]["txt_2"] = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean";
$lang["cursos_talleres"]["txt_3"] = "commodo ligula eget dolor. Aenean massa.";
$lang["cursos_talleres"]["txt_4"] = "Register";
$lang["cursos_talleres"]["txt_5"] = "I want to register";
$lang["cursos_talleres"]["txt_6"] = "Name";
$lang["cursos_talleres"]["txt_7"] = "Telephone";
$lang["cursos_talleres"]["txt_8"] = "Email";
$lang["cursos_talleres"]["txt_9"] = "Message";
$lang["cursos_talleres"]["txt_10"] = "SEND";



$lang["eventos"]["txt_1"] = "EVENTS";
$lang["eventos"]["txt_2"] = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean";
$lang["eventos"]["txt_3"] = "commodo ligula eget dolor. Aenean massa.";


$lang["testimonios"]["txt_1"] = "TESTIMONIALS";

$lang["cv"]["txt_1"] = "Accreditations";
$lang["cv"]["txt_2"] = "Education";
$lang["cv"]["txt_3"] = "Universidad Iberoamericana: BA in Sociology, October 13, 1993. Honors. ";
$lang["cv"]["txt_4"] = "Centro Ericksoniano de Mexico, Masters in Ericksonian Psycotherapy, July 15, 2014.";
$lang["cv"]["txt_5"] = "Instituto Mexicano de Psicooncología, Ph.D in Thanatology, December 10, 2019.";
$lang["cv"]["txt_6"] = "Diplomas";
$lang["cv"]["txt_7"] = "Desarrollo-E: Diploma in the Eneagram system, November 9, 2005.";
$lang["cv"]["txt_8"] = "Resonance Repatterning  Association, Healing Family Systems, February 10, 2006.";
$lang["cv"]["txt_9"] = "Gestalt Institute, Energetic Reprogramming for Health (Allergy Antidotes), February 18, 2007.";
$lang["cv"]["txt_10"] = "Mary Burmeister, Certificate of Attendance in the art of Jin Shin Jyutsu, August, 2007.";
$lang["cv"]["txt_11"] = "Rami Rion, Reiki Master, October 28, 2008. ";
$lang["cv"]["txt_12"] = "Resonance Repatterning Institute, Quantum Change Made Easy Instructor, July 10, 2010.";
$lang["cv"]["txt_13"] = "Omega NYC: Reflect, Reconnect, Renew. Touching the Unseen World: Discovering your Spiritual Self, April 20, 2012. ";
$lang["cv"]["txt_14"] = "Weiss Institute, Past Life Therapy Training, October 8-12, 2012. ";
$lang["cv"]["txt_15"] = "The Newton Institute, Certified Life Between Lives Therapist, January 2015. ";
$lang["cv"]["txt_16"] = "Access Consiousness, Certificate of Attendance, April 28, 2016.";
$lang["cv"]["txt_17"] = "Stanford Business School, Executive Program for Social Entrepeneurs, February 8, 2019.";
$lang["cv"]["txt_18"] = "Quantum Bio Reprogramming. January 16, 2020. ";
$lang["cv"]["txt_19"] = "Institute for Quantum Consciousness, Facilitator Training “Journey Through other Realms”, April 7, 2019. Universal Consciousness Facilitator, January 20, 2022. ";
$lang["cv"]["txt_20"] = "Centre of Excellence, Alchemy Diploma, November 2, 2022";
$lang["cv"]["txt_21"] = "Resonance Reppatterning Method by Chloe Wordsworth:";
$lang["cv"]["txt_22"] = "La RR Educación Educativa AC, Certified Practitioner of Resonance Repatterning, February 3, 2004.";
$lang["cv"]["txt_23"] = "The Holographic Repatterning Association, Certified Practitioner Level 2, September 2004";
$lang["cv"]["txt_24"] = "RR Asociación Educativa AC, Certified Practitioner, February 1, 2014";
$lang["cv"]["txt_25"] = "Resonance Repatterning Institute, Certified Advanced Practitioner diploma, April 2022. ";
$lang["cv"]["txt_26"] = "Other studies:";
$lang["cv"]["txt_27"] = "Medical Biomagnetism with Isaac Goiz Durán. ";
$lang["cv"]["txt_28"] = "Astrology with Olvido Ramos, independent teacher. ";


$lang["entrevistas"]["txt_1"] = "Interviews";
$lang["entrevistas"]["txt_2"] = "PODCAST";
$lang["entrevistas"]["txt_3"] = "MyNDTALK, Women's Wednesday - Befriending The Wolf - Milly Diericx, December 7, 2016.";
$lang["entrevistas"]["txt_4"] = "The Dona Seebo Show, Interview about Befriending the Wolf, March 30, 2017.";
$lang["entrevistas"]["txt_5"] = "It’s Your Health with Lisa Davies, Interview with Milly Diericx, December 16, 2016.";
$lang["entrevistas"]["txt_6"] = "Grupo Fórmula, Interview with Leonardo Cursio about The Konscio Method. February 16, 2022.";
$lang["entrevistas"]["txt_7"] = "Avance Humano, interview with Beatriz Godinez about The Konscio Method, February 2022.";
$lang["entrevistas"]["txt_8"] = "TV INTERVIEWS";
$lang["entrevistas"]["txt_9"] = "NY1 News, The indefatigable femenine battle against Lupus, January 11, 2017.";
$lang["entrevistas"]["txt_10"] = "Frente al País with Ana Paula Ordorica, News story about Clínica Bienestar 360, July 16, 2018.";
$lang["entrevistas"]["txt_11"] = "Expert TV in the program La Era de la Nueva Mujer with Elisa Eraña, about Alternative Health, Tuesday, May 7, 2019:";

$lang["congresos"]["txt_1"] = "Conferences";
$lang["congresos"]["txt_2"] = "XIII International Thanatology Congress of the XXIst Century,  “Thanatological attention for patients with chronic illnesses”,October 22, 2021.";

$lang["blog"]["txt_1"] = "BLOG";
$lang["blog"]["txt_2"] = "Read more";


$lang["preguntas"]["txt_1"] = "FREQUENTLY ASKED QUESTIONS";
$lang["preguntas"]["txt_2"] = "What problems does the Konscio Method solve?";
$lang["preguntas"]["txt_3"] = "The Konscio Method is a healing journey. We need to heal the trauma and suffering that has caused the problems in our lives. In this method, we are already perfect and whole; we have just forgotten it. The healing is like sweeping away the debris that has clouded our understanding of our true essence. The problems we have are misinformation caused by negative beliefs and thoughts that bring about negative behaviors and their consequences.";
$lang["preguntas"]["txt_4"] = "As we heal the beliefs and thoughts causing the behaviors, the problems cease to plague us.";
$lang["preguntas"]["txt_5"] = "What does the Konscio Method entail?";
$lang["preguntas"]["txt_6"] = "The Konscio method is all about healing. Each person has to do the work of healing from the inside. The method provides the tools needed for this, but it is up to each individual to do the actual work. The time needed to heal each person’s story depends on that person, their degree of readiness, their defense mechanisms, and the number of issues involved. It’s a personal journey, not a competition. ";
$lang["preguntas"]["txt_7"] = "How long does the Konscio Method take?";
$lang["preguntas"]["txt_8"] = "It depends on the level of commitment of each individual and the number of issues experienced by them. Unfortunately, there is no easy answer; some people have experienced a sort of revelation that solves everything in a clean sweep. Most of us have to work at it.";
$lang["preguntas"]["txt_9"] = "Problems tend to be like the layers of an onion, you solve one, and the next one comes up, and the next and so forth. For many of us, like me, this a life journey.";
$lang["preguntas"]["txt_10"] = "What are the results of the Konscio Method?";
$lang["preguntas"]["txt_11"] = "The increasing well-being, better health, abundance, and inner peace are a gift in themselves, and they will constantly come, in increments, every time you heal an aspect of your life. This method is rewarding at every turn. The amount of healing anyone is willing to do will bring its own reward. You can take it at any rhythm that's good for you. That is what all forms of therapy and self-help are attempting, to heal different aspects of life. ";
$lang["preguntas"]["txt_12"] = "Every time you heal, you feel better, you become more in touch with who you really are.";
$lang["preguntas"]["txt_13"] = "Are the changes obtained in the Konscio Method lasting?";
$lang["preguntas"]["txt_14"] = "Yes! When we learn something, we cannot unlearn it. When we see a new aspect of ourselves, we cannot unsee it. When we remove an obstacle, we walk more freely. We could try and go back to our old selves, but it would take much more effort than accepting the new path. No one who doesn't want change will undergo any kind of healing journey, so when something is healed, rest assured, it's healed. ";
$lang["preguntas"]["txt_15"] = "If it comes up again, then it wasn't completely healed, and it will never come back exactly the same; the work done will have modified it, we will have learned and grown with the experience and the effort.";
$lang["preguntas"]["txt_16"] = "Is the Konscio Method safe?";
$lang["preguntas"]["txt_17"] = "It is. It will bring up issues and traumas, but to be healed. If any of the techniques sound dangerous to you, don't do them. Take this at your pace. Remember, all kinds of professional therapists can help you through the rough patches, in any school that is comfortable for you. Healing is always good, even if the process can be painful at times. ";
$lang["preguntas"]["txt_18"] = "Ask for any kind of help you need; none of us is alone, we are in this together, and we will heal and evolve together.";


$lang["contacto"]["txt_1"] = "Send us a message";
$lang["contacto"]["txt_2"] = "We will gladly answer";
$lang["contacto"]["txt_3"] = "all your questions";
$lang["contacto"]["txt_4"] = "Name";
$lang["contacto"]["txt_5"] = "Telephone";
$lang["contacto"]["txt_6"] = "Email";
$lang["contacto"]["txt_7"] = "Message";
$lang["contacto"]["txt_8"] = "SEND";
$lang["contacto"]["txt_9"] = "Privacy Notice";
$lang["contacto"]["txt_17"] = "./assets/pdf/PRIVACY_NOTICE.pdf";
$lang["contacto"]["txt_10"] = "Web design: Caja Creativa";


$lang["contacto"]["txt_11"] = "Message sent";
$lang["contacto"]["txt_12"] = "Message sent successfully";
$lang["contacto"]["txt_13"] = "Sent";
$lang["contacto"]["txt_14"] = "Thank you for contacting us, we will get in touch with you.";

$lang["contacto"]["txt_15"] = "Error";
$lang["contacto"]["txt_16"] = "Try to refresh the page.";
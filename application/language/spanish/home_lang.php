<?php
$lang["menu"]["inicio"] = "Inicio";
$lang["menu"]["acerca_de"] = "Acerca de la Autora";
$lang["menu"]["terapias"] = "Terapias";
$lang["menu"]["testimonios"] = "Testimonios";
$lang["menu"]["cv"] = "Acreditaciones";
$lang["menu"]["entrevistas"] = "Entrevistas";
$lang["menu"]["congresos"] = "Congresos";
$lang["menu"]["que_es"] = "¿Qué es el Método Konscio?";
$lang["menu"]["videos"] = "Vídeos";
$lang["menu"]["libros"] = "Libros";
$lang["menu"]["cursos_talleres"] = "Cursos & Talleres";
$lang["menu"]["eventos"] = "Eventos";
$lang["menu"]["blog"] = "Blog";
$lang["menu"]["preguntas_freq"] = "Preguntas Frecuentes";
$lang["menu"]["contacto"] = "Contacto";


$lang["home"]["video1"] = "https://player.vimeo.com/video/645324220?h=06666afcdc";
$lang["home"]["txt_1"] = "El siguiente paso en la evolución del ser humano, es uno de consciencia.";
$lang["home"]["txt_2"] = "VER TERAPIAS";
$lang["home"]["txt_3"] = "¿QUÉ ES EL MÉTODO KONSCIO?";
$lang["home"]["txt_4"] = "La palabra";
$lang["home"]["txt_5"] = "Konscio";
$lang["home"]["txt_6"] = "viene del griego y significa consciencia. Los griegos fueron los primeros en hablar de la consciencia";
$lang["home"]["txt_7"] = " per se. ";
$lang["home"]["txt_8"] = "Aquí Konscio significa el estar conscientes de nosotros mismos por lo que realmente somos: seres espirituales viviendo una experiencia humana, parte del Konscio Fuente. Para tomar consciencia de esto debemos de expandir nuestra consciencia a toda la extensión del ser magnificente, inmortal y luminoso que somos. La meta del ";
$lang["home"]["txt_9"] = "Método Konscio";
$lang["home"]["txt_10"] = "es ayudar a traer la consciencia de la humanidad a esta verdad intrínseca.";
$lang["home"]["txt_11"] = "Para recordar quiénes somos, debemos de limpiar la basura que ha nublado nuestra percepción de nuestra magnificencia. Necesitamos sanar nuestros pensamientos y emociones negativas y las situaciones que los crearon. Cuando hacemos esto, nuestra consciencia naturalmente se expande, nos damos cada vez más cuenta de nuestra naturaleza de amor y luz.";
$lang["home"]["txt_12"] = "LOS 4 NIVELES DE KONSCIO";
$lang["home"]["txt_13"] = "Egoica";
$lang["home"]["txt_14"] = "Empática";
$lang["home"]["txt_15"] = "Unificada";
$lang["home"]["txt_16"] = "Fuente";
$lang["home"]["txt_17"] = "CONOCER MÁS";
$lang["home"]["txt_18"] = "CURSOS Y TALLERES";
$lang["home"]["txt_19"] = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean <br>commodo ligula eget dolor. Aenean massa. ";
$lang["home"]["txt_20"] = "Inscribirme";
$lang["home"]["txt_21"] = "EVENTOS";
$lang["home"]["txt_22"] = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean";



$lang["acerca"]["txt_1"] = "Acerca de Milly Diericx";
$lang["acerca"]["txt_3"] = "Es una terapeuta de sistemas de energía e hipnosis. Llegó a estas terapias después de haber agotado todas las vías convencionales de sanar su Lupus Eritematoso Sistémico (enfermedad autoinmune). La medicina tradicional sólo le ofrecía paliativos, maneras de disminuir los síntomas más no le ofrecía soluciones reales. Al negarse a darse por vencida buscó terapias alternativas, después las estudió y ahora las lleva a toda persona que le interese, habiendo probado en carne propia su efectividad.";
$lang["acerca"]["txt_4"] = "Después se interesó por los funcionamientos de la mente profunda, y así estudió una maestría en Piscoterapia Ericksoniana, seguida de una especialidad en regresiones a Vidas Pasadas y a Vida Entre Vidas®, así como en Consciencia Cuántica. Este trabajo profundamente espiritual otorga una gran claridad acerca de el ser mismo, su propósito de vida y los acuerdos que hizo antes de llegar aquí. Así mismo se adentró en los procesos del duelo con un doctorado en tanatología. ";
$lang["acerca"]["txt_4b"] = "Si quieres hacer una cita, por favor contáctanos";
$lang["acerca"]["txt_5"] = "Ahora continúa su trayectoria con la expansión de la consciencia con ";
$lang["acerca"]["txt_6"] = "El Método Konscio™ ";
$lang["acerca"]["txt_7"] = "en el que ha compilado todo su conocimiento y experiencia, utilizando diferentes métodos para sanar los diferentes niveles de consciencia, ayudando a las personas a expandir su consciencia y llegar a niveles cada vez más altos de coherencia, paz interior y bienestar. ";


$lang["terapias"]["txt_1"] = "Terapias";
$lang["terapias"]["txt_2"] = "El Método Konscio™";
$lang["terapias"]["txt_3"] = "El Método Konscio está diseñado para expandir el nivel de consciencia sanando los obstáculos que tiene la persona para desarrollarse como el ser magnificente de luz que realmente es. En la primera consulta se lleva a cabo un diagnóstico holístico del estado de salud física, energética y emocional, utilizando tecnología de punta. En colaboración con el consultante se lleva a cabo una intención para el tratamiento y un programa de sesiones para lograr las metas establecidas. El tratamiento incluye técnicas como las siguientes:  ";
$lang["terapias"]["txt_4"] = "Resonance Repatterning ®";
$lang["terapias"]["txt_5"] = "Es un método desarrollado por Chloe Wordsworth basado en el concepto de la nueva ciencia de que todo en el universo es energía manifiesta en patrones de frecuencia, incluyendo nuestros problemas emocionales, mentales y físicos. A través de modalidades especificas, se cambia la resonancia de la frecuencia de los problemas a la frecuencia de las cosas positivas que deseamos en la vida. Este método revolucionario logra cambios increíbles en los patrones que nos causan malestar, logrando en vez de ellos conductas y actitudes que nos llevan a la alegría, satisfacción y bienestar.";
$lang["terapias"]["txt_6"] = "Psicoterapia Ericksoniana";
$lang["terapias"]["txt_7"] = "Este técnica desarrollada por Milton H. Erickson, considerado el padre de la hipnosis moderna, es una manera fácil de acceder a la mente subconsciente e inconciente y desde ahí resolver los nudos mentales que nos hacen caer en actitudes de depresión, desesperanza y sufrimiento, regresando fácilmente a nuestra manera natural de ser que es curiosa, vivaz y tendiente al bienestar.";
$lang["terapias"]["txt_8"] = "Regresiones a Vida entre Vidas™";
$lang["terapias"]["txt_9"] = "Este método de hipnosis profunda desarrollado por el Dr. Michael Newton fundador del Michael Newton Institute abre la posibilidad de acceder a las memorias más profundas, más allá de la mente inconsciente, a lo que él llama el super-consciente, que guarda las memorias del alma y del mundo espiritual y nos brinda la claridad acerca de nuestro verdadero ser inmortal y eterno que llamamos alma.";
$lang["terapias"]["txt_10"] = "Reprogramación Energética de la Salud™(Allergy Antidotes®)";
$lang["terapias"]["txt_11"] = "Terapia enfocada a aliviar alergias e intolerancias alimenticias y a sustancias. Utiliza los meridianos energéticos de la Medicina Tradicional China para reducir las reacciones del sistema inmunológico ante sustancias o alimentos que causan reacciones alérgicas.";
$lang["terapias"]["txt_12"] = "Consciencia Cuántica y Consciencia Universal";
$lang["terapias"]["txt_13"] = "Método creado por Peter Smith. Es una expansión de la conciencia que lleva a la persona a un conocimiento profundo de su ser a través de los diferentes planos y dimensiones del alma. Con esta técnica pueden resolverse problemas del pasado, así como llegar a conocer la misión y propósito del alma en esta encarnación.";
$lang["terapias"]["txt_14"] = "Jin Shin Jyutsu";
$lang["terapias"]["txt_15"] = "Terapia energética traída al occidente por Mary Burmeister, parte de la idea de que el plano original del cuerpo es perfecto, y a lo largo de la vida se van creando bloqueos a los flujos energéticos que mantienen el buen funcionamiento del cuerpo. A través de tocar ciertos puntos en el cuerpo, estos flujos se restablecen, recalibrando el correcto funcionamiento del cuerpo.";
$lang["terapias"]["txt_16"] = "Reiki";
$lang["terapias"]["txt_17"] = "Método de transmisión de energía a través de las manos. Cuando el cuerpo recibe energía curativa extra puede llevar a cabo sus procesos naturales de sanación mucho más rápida y eficientemente.";
$lang["terapias"]["txt_18"] = "Regresión a vidas pasadas";
$lang["terapias"]["txt_19"] = "Basada en el método de Brian Weiss, es una hipnosis profunda que puede accesar las memorias del alma y los cuerpos y tiempos diferentes en los que dicha alma ha encarnado. Conocer las vidas pasadas ayuda a sanar situaciones de esta vida que no parecen tener explicación con las experiencias vividas en ella.";
$lang["terapias"]["txt_20"] = "Biomagnetismo Médico™";
$lang["terapias"]["txt_21"] = "El Doctor Isaac Goiz descubrió que los microorganismos tienden a habitar en el cuerpo en pares que se complementan, lo que llama el Par Biomagnético®. Al poner imanes en ciertos puntos del cuerpo, se restablece el Ph neutro que es coadyuvante en la sanación de ambos patógenos propiciando la salud del cuerpo.";
$lang["terapias"]["txt_22"] = "Astrología";
$lang["terapias"]["txt_23"] = "La astrología es un arte milenario que ha sido compilado a través de la observación de la naturaleza humana y los movimientos de los astros y sus interrelaciones. A través de estas observaciones, se ha creado el método astrológico que brinda información acerca de la persona, su personalidad y su salud, sus tendencias de comportamiento, las lecciones a aprender y el propósito de esta vida.";
$lang["terapias"]["txt_24"] = "Bioreprogramación Cuántica";
$lang["terapias"]["txt_25"] = "A través de la kinesiología se hace un barrido de los residuos energéticos en el aura de la persona. Al limpiar el campo aúrico, las condiciones experimentadas mejoran considerablemente.";
$lang["terapias"]["txt_26"] = "Alquimia Espiritual";
$lang["terapias"]["txt_27"] = "Basado en la interpretación de Carl Jung de el antiguo arte de la Alquimia, es la transformación del espíritu humano desde su materia prima, asocaiada al plomo, a través de varios pasos en un proceso de transmutación hasta llegar a la sublimación del ser, asociado al oro.";



$lang["publicaciones"]["txt_1"] = "Publicaciones";
$lang["publicaciones"]["txt_2"] ="Artículo de U.S. News: ¿Que se siente ser diagnosticado con una enfermedad crónica?'Tu cuerpo se ha convertido en una prisión de la que no puedes escapar, excepto a través de la muerte.' Por Milly Diericx | Contribuidora Nov. 2, 2016, at 6:00 a.m.";
$lang["publicaciones"]["txt_3"] ="Artículo de U.S. News: 4 llaves para manejar las emociones de un diagnóstico de una enfermedad crónica. Primera en la lista: aceptarte y conocer a tu nuevo yo. Por Milly Diericx | Contribuidora Nov. 4, 2016, at 10:27 a.m.";
$lang["publicaciones"]["txt_4"] ="Artículo de U.S. News: 5 pasos diarios para combatir la depresión que viene con las enfermedades crónicas: Esperanza, gratitud y pensamiento positivo son parte de la lista. Por Milly Diericx | Contribuidora, Dec. 14, 2016,  6:00 a.m.";
$lang["publicaciones"]["txt_5"] ="Mind Body Green, Las úncas tres preguntas que te tienes que hacer la próxima vez que estés estresado. Por Milly Diericx, Octubre 31, 2016.";
$lang["publicaciones"]["txt_6"] ="Willowbay.com, Artículo acerca del libro Befriending the Wolf: Cómo una terapeuta encontró la salud haciendose amiga de su enfermedad incurable. Quick Adsense WordPressPlugin: http://quicksense.net/ 2a ";
$lang["publicaciones"]["txt_7"] ="Lupus Foundation of America, Mes del Lupus Artículo del libro Befiending the Wolf: The Guide to Living and Thriving with Lupus. Por Migdalia Fernandez,  27 de Enero, 2017.";
$lang["publicaciones"]["txt_8"] ="Mariah Nichols, Voces de la Comunidad con Discapacidad, Milly Diericx, 7 de Marzo, 2017";




$lang["videos"]["video1"] = "https://player.vimeo.com/video/675568948?h=fb01ccdcb1";


$lang["konscio"]["txt_1"] = "ACERCA DE KONSCIO";
$lang["konscio"]["txt_2"] = "¿Qué es el método Konscio?";
$lang["konscio"]["txt_3"] = "Los 4 niveles de Konscio";
$lang["konscio"]["txt_4"] = "¿Cómo llegar a ellos?";
$lang["konscio"]["txt_5"] = "¿Para qué llegar a ellos?";
$lang["konscio"]["txt_6"] = "Resultados";
$lang["konscio"]["txt_7"] = "¿Qué es el Método Konscio?";
$lang["konscio"]["txt_8"] = "La palabra";
$lang["konscio"]["txt_9"] = "Konscio";
$lang["konscio"]["txt_10"] = "viene del griego y significa consciencia. Los griegos fueron los primeros en hablar de la consciencia";
$lang["konscio"]["txt_11"] = " per se. ";
$lang["konscio"]["txt_12"] = "Aquí Konscio significa el estar conscientes de nosotros mismos por lo que realmente somos: seres espirituales viviendo una experiencia humana, parte del Konscio Fuente. Para tomar consciencia de esto debemos de expandir nuestra consciencia a toda la extensión del ser magnificente, inmortal y luminoso que somos.";
$lang["konscio"]["txt_13"] = "La meta del";
$lang["konscio"]["txt_14"] = "Método Konscio";
$lang["konscio"]["txt_15"] = "es ayudar a traer la consciencia de la humanidad a esta verdad intrínseca. Para recordar quiénes somos, debemos de limpiar la basura que ha nublado nuestra percepción de nuestra magnificencia. Necesitamos sanar nuestros pensamientos y emociones negativas y las situaciones que los crearon. Cuando hacemos esto, nuestra consciencia naturalmente se expande, nos damos cada vez más cuenta de nuestra naturaleza de amor y luz.";
$lang["konscio"]["txt_16"] = "Los 4 niveles de Konscio";
$lang["konscio"]["txt_17"] = "Dividí a Konscio en cuatro niveles para que llegar a ellos sea más fácil. Estos niveles fueron escogidos a raíz de mi experiencia con ellos, ya que Konscio es un continuo, no está en etapas, sin embargo, conseguirla en escalones resulta más sencillo. Este modelo implica tener un primer nivel sano para llegar al segundo, un segundo nivel sano para llegar al tercero y un tercero sano para que el cuarto se pueda manifestar.";
$lang["konscio"]["txt_18"] = "Egoica";
$lang["konscio"]["txt_19"] = "Primer Nivel,";
$lang["konscio"]["txt_20"] = "Konscio Egoica.";
$lang["konscio"]["txt_21"] = "Esta es la consciencia despierta, con la que estamos familiarizados. Aquí tenemos un cuerpo, un género, una identidad, un nombre, en resumen, un Ego. Este nivel nos permite navegar la vida con todo lo que se requiere en ella, como pertenecer a una sociedad, trabajar, saber quiénes somos. Este nivel está sano cuando logramos aceptarnos tal cual somos y aceptar nuestra historia de vida tal cual es, es decir, amarnos incondicionalmente.";
$lang["konscio"]["txt_22"] = "Segundo Nivel,";
$lang["konscio"]["txt_23"] = "Konscio Empática.";
$lang["konscio"]["txt_24"] = "Es mi idea y esperanza que la humanidad está entrando a este nivel, y por lo que estamos viendo, es la única manera, pienso yo, de sobrevivir a las crisis que estamos experimentando (ambiental, sanitaria, política y económica). Este nivel nos permite empatizar con los demás y con todas las especies con las que compartimos nuestro hogar.";
$lang["konscio"]["txt_25"] = "Empatizar no significa poner los intereses de los demás por encima de los nuestros, significa conectar con la sabiduría del corazón y pasar cada decisión por esta sabiduría, preguntando, “¿Qué impacto tendrá esto en los demás, otras especies, en el planeta?” y eligiendo hacer el menor daño a lo que nos rodea a través de nuestras acciones. Necesitamos tener un ego sano, un primer nivel resuelto, para no actuar desde la ambición, desde un sentimiento de ser más o menos que nadie o nada más, desde la culpa, o de ser salvadores, sino desde solo nosotros, una parte esencial del Todo. En este nivel sano, nos sentimos conectados con todo y todos, expandimos el amor incondicional de nosotros mismos a todos los seres con los que compartimos la tierra.";
$lang["konscio"]["txt_26"] = "Empática";
$lang["konscio"]["txt_27"] = "Unificada";
$lang["konscio"]["txt_28"] = "Tercer Nivel,";
$lang["konscio"]["txt_29"] = "Konscio Unificada.";
$lang["konscio"]["txt_30"] = "En este nivel nos expandimos más allá de sentirnos conectados con todo y todos, nos sentimos Uno con todo lo que es.  Los caminos espirituales han creado métodos para intentar llegar a este nivel. Aquí experimentamos la unicidad, la iluminación, la paz. Aquí expandimos el amor incondicional que ahora sentimos por todas las especies y profundizamos en él, de manera que ahora somos parte de todo lo que hay. Aquí el amor deja de ser un verbo, algo que hacemos por nosotros mismos y los demás, y se convierte en un estado del ser, comenzamos a ser energía de Amor, conectada a todo.";
$lang["konscio"]["txt_31"] = "Cuarto Nivel,";
$lang["konscio"]["txt_32"] = "Konscio Fuente.";
$lang["konscio"]["txt_33"] = "En este nivel somos conscientes de ser la Fuente misma; experimentamos ser Konscio Fuente. No hay métodos probados para llegar a este nivel, las personas que lo han tocado lo han hecho espontáneamente, en un momento sin tiempo. Haber logrado los tres niveles anteriores te prepara para llegar a este nivel sagrado de consciencia, aunque nada garantiza que lo harás. Debes de estar abierto y listo para la experiencia. Aquí experimentas la paz absoluta, el amor absoluto, la quietud y la nada. Aquí ya no hay identidad, eres la Fuente misma, consciencia pura, amor absoluto e incondicional completo, la Fuente de esta energía amorosa creativa que subyace a todo.";
$lang["konscio"]["txt_34"] = "Fuente";
$lang["konscio"]["txt_35"] = "¿Cómo llegar a ellos?";
$lang["konscio"]["txt_36"] = "Primer Nivel, Konscio Egoica.";
$lang["konscio"]["txt_37"] = "El que necesita más trabajo es el primer nivel, ya que aquí yacen nuestros traumas, nuestro dolor, sufrimiento, pensamientos y emociones negativas. El primer paso es sanar este nivel y el segundo llega naturalmente. Si estás tratando de ser empático y no está funcionando, aún hay cosas que resolver en el primer nivel. Los métodos para sanar este nivel son: físicos, teniendo comportamientos saludables para nuestro cuerpo; emocionales, aprendiendo a lidiar, resolver y encauzar positivamente nuestros cuadros emocionales; y mentales, sanando las creencias limitantes que tenemos de nosotros mismos. Existen muchos tipos de terapias que tratan con estos aspectos. Yo propongo y utilizo con mis clientes, aquéllas con las que yo he experimentado y me han funcionado.";
$lang["konscio"]["txt_38"] = "Segundo Nivel, Konscio Empática.";
$lang["konscio"]["txt_39"] = "Una vez que la empatía es parte natural de ti, pasas cada decisión a través de la sabiduría del corazón, haces el menor daño posible a lo que te rodea, y eres feliz y te sientes satisfecho ayudando a los demás, has logrado el segundo nivel. Normalmente hay muchas vueltas entre el primer y el segundo nivel. No hay culpa en esto, la consciencia se expande mientras sanamos, y se contrae cuando algo surge para ser sanado. Es un proceso, y debemos de ser compasivos con nosotros mismos durante el mismo. Aquí hace mucha falta la sanación del corazón. El corazón es el centro energético -chakra- del segundo nivel, y si ha sido roto con abuso o trauma emocional, debemos de sanarlo antes de lograr este nivel. Debemos de perdonar a todos los que nos han lastimado, para poder experimentar el amor incondicional hacia nosotros mismos, y hacia ellos, verlos con compasión y comprensión.";
$lang["konscio"]["txt_40"] = "Tercer Nivel, Konscio Unificada.";
$lang["konscio"]["txt_41"] = "Una vez que la empatía es nuestra segunda naturaleza, y no es ningún esfuerzo pasar las decisiones a través del corazón, nuestra meditación y prácticas espirituales nos traerán sin mucho esfuerzo al tercer nivel, a la consciencia unificada o espiritual. Vamos a querer pasar más y más tiempo en este espacio, ya que aquí encontramos la paz. Hemos logrado este nivel cuando ya no perdemos la conciencia unificada en nuestra vida cotidiana, cuando ya no nos contraemos con las cosas que antes nos hacían reaccionar. Mientras continuamos reaccionando, aún hay cosas que sanar en el nivel uno y dos. Está bien. Este es un viaje, no un destino. Cuando te encuentras en este nivel la mayor parte del tiempo sin esfuerzo; estás iluminado y estás listo para que el cuarto nivel se manifieste. Iluminación significa simplemente estar conscientes del hecho de que todo es uno, que no hay separación.";
$lang["konscio"]["txt_42"] = "Cuarto Nivel, Konscio Fuente.";
$lang["konscio"]["txt_43"] = "Este nivel es elusivo, y el mas fácil de todos si estás listo. Los que han reportado llegar a él ha sido espontáneamente. Le ha llegado a personas sin entrenamiento espiritual y ha eludido a santos. La cuestión es que si no estás listo, puede ser una experiencia abrumadora, por eso pienso que es mejor hacerlo paso a paso y dejar que la experiencia llegue cuando estés listo para ella. Es el más sencillo pues es nuestra verdadera escencia, todos somos Konscio Fuente, así que es como regresar a casa. La Konscio Fuente está en cada partícula de la creación, en el espacio entre los átomos, manteniendo a las galaxias unificadas. Convertirse en este estado del ser es ser conscientes constamente que Konscio Fuente es todo lo que hay, expresándose en un sinfín de formas.";
$lang["konscio"]["txt_44"] = "¿Para qué llegar a ellos?";
$lang["konscio"]["txt_45"] = "Este proceso claramente no es para cualquiera. El paso del primer al segundo nivel, en mi opinión, es inevitable en el mediano plazo si vamos a sobrevivir como especie, es el siguiente paso en la evolución de la humanidad. Sin embargo, los que voluntariamente toman el camino son almas especiales, los que desean ser parte de la solución. Esas valientes almas empáticas están despertando a la Konscio Empática. Juntos vamos a crear una masa crítica, suficientes individuos sintiendo, pensando y actuando diferente, creando una nueva realidad para que los demás se nos puedan unir. En esta nueva realidad habrá mucho menos división, menos agresión, guerra, nosotros contra ellos, injusticia, mucho más respeto por los demás y por la naturaleza, por el mundo entero.";
$lang["konscio"]["txt_46"] = "Los niveles tres y cuatro son para aquéllos que sienten que quieren algo más, que han sentido el llamado del mundo espiritual, almas viejas listas para regresar a la Fuente. Por esto dividí mi obra en dos libros. El primero dedicado al cambio del nivel uno al dos. Esto es suficiente para la mayoría de nosotros; este cambio verá nacer una nueva humanidad. El segundo libro es para buscadores espirituales que están buscando seriamente concientizarse de lo profundo y enorme de su ser. Estos niveles aún son un camino personal en este estado de nuestro desarrollo. Llegaremos a ellos en masa más adelante en nuestra historia. ¡Primero debemos de pasar la prueba de cambiar al segundo nivel como sociedad!";
$lang["konscio"]["txt_47"] = "Resultados";
$lang["konscio"]["txt_48"] = "Los resultados de hacer este trabajo son muy placenteros. Sanar la Konscio Egioca nos da paz mental, apertura del corazón, amor incondicional por nosotros mismos, menos sufrimiento, una meta clara, un sistema de valores personal, y la adquisición de las cosas que nuestro corazón desea, y por ende gozo. Aquí nos sentimos verdaderamente agradecidos por quienes somos, por nuestra historia.";
$lang["konscio"]["txt_49"] = "Sanar la Konscio Empática trae amor incondicional por todos los seres, perdón, paz en el corazón, gozo y alegría en nuestras relaciones y en nosotros mismos. Aquí nos sentimos agradecidos por las personas en nuestra vida, y nos volvemos agentes activos en crear la sociedad en la que queremos vivir, una forma de vida más compasiva, amorosa y justa.";
$lang["konscio"]["txt_50"] = "La obtención del tercer nivel, la Konscio Unificada, nos da una sensación de pertenecer que jamás hemos soñado. Somos parte del todo y en este nivel estamos conscientes de ello todo el tiempo. Esta consciencia trae completa paz interior, gozo absoluto, amor incondicional por todas las cosas y personas, ya que sabemos que todos somos Uno.";
$lang["konscio"]["txt_51"] = "El cuarto nivel, la Konscio Fuente, es éxtasis absoluto. El éxtasis de ser todo y nada al mismo tiempo, de la quietud completa, de ser la Fuente del amor, de la vida, de la creación.";
$lang["konscio"]["txt_52"] = "Es un camino lleno de reveses, ya que hemos acumulado tantos dolores y temas que resolverlos todos toma una gran dedicación y compromiso. Sin embargo, al final del camino está el premio más grande que podemos imaginar, regresar a la Fuente en consciencia, voluntariamente regresar a nuestra propia escencia. Para los Budistas es el Nirvana: cuando nos disolvemos en la Fuente de todo lo que es y cesamos la rueda kármica.";
$lang["konscio"]["txt_53"] = "";




$lang["libros"]["txt_1"] = "Libros";
$lang["libros"]["txt_2"] = "A lo largo de mi experiencia, he escrito los siguientes libros";

$lang["libros"]["txt_3"] = "EL Método Konscio. Sanación. Expansion. Evolución  ";
$lang["libros"]["txt_4"] = "He dividido mi trabajo en dos libros: El primero trata de sanar el primer y segundo niveles de Konscio. Este cambio de consciencia es lo que necesita la humanidad para sobrevivir los tiempos turbados en que estamos viviendo.";
$lang["libros"]["txt_5"] = "Este libro es para aquéllos que quieren ser parte de la solución, siendo punta de lanza del cambio de consciencia. Necesitamos llegar a una masa crítica de individuos que piensen y actuen diferente para traer un estado más evolucionado de conciencia a la cultura convencional.
El segundo libro trata de el tercer y cuarto nivel de consciencia y está pensado para personas con aspiraciones trascendentales.
Konscio es el término griego para consciencia. Los griegos fueron los primeros en hablar de la consciencia";
$lang["libros"]["txt_6"] = " per se, ";
$lang["libros"]["txt_7"] = "aunque en tiempos antiguos utilizaban la palabra";
$lang["libros"]["txt_8"] = " psyche, ";
$lang["libros"]["txt_9"] = "que incluía los aspectos espiritual y mental. Konscio se utiliza aquí para referirnos a sabernos lo que realmente somos: seres espirituales viviendo una experiencia humana. Para saber esto debemos primero expandir nuestra consciencia a toda la extensión de nuestro ser—al ser enorme, magnificente, imortal de luz que realmente somos. El proyecto Konscio es para ayudar a traer a la conciencia de la humanidad a su verdad.
Para explicar fácilmente este clavado a la consciencia, elejí dividirla en cuatro grandes niveles. Cada uno podía ser subdividido de una miríada de maneras, pero para mí estas categorías son claras. Así que pensemos en nosotros como una Matryoshka—una serie de cuatro muñecas anidadas, una dentro de la otra.  Nos expandimos de la primera y más pequeña, La Konscio Egioca, que describe nuestros cuerpos físicos y la vida que estamos viviendo; a la Konscio Empática, la segunda más grande, que contiene nuestros cuerpos emocional y mental y que nos conecta con otros seres humanos, sus mentes y emociones;  a la Konscio Unificada, el siguiente tamaño, que alberga a nuestro ser espiritual, nuestro cuerpo espiritual y nos conecta a través del campo unificado a todas las cosas, capacitánonos para saber que somos Uno; a la muñeca más grande, la Konscio Fuente, que ya no se encuentra dentro del cuerpo, no tiene un cuerpo";
$lang["libros"]["txt_10"] = " per se. ";
$lang["libros"]["txt_11"] = "Es el conocimiento de que ya somos parte de la Fuente. Me he guiado a mí misma y a mis clientes a traves de los nivles de consciencia, encontrando tesoros de significado cada vez mayor en cada uno. Sólo he tocado la Konscio Fuente una vez, y fue una experiencia que cambió mi vida profundamente.";
$lang["libros"]["txt_21"] = "Diericx Milly, El Método Konscio: Sanción. Expansión. Evolución, Seek Editorial, 2022";
$lang["libros"]["txt_23"] = "Comprar ahora";

$lang["libros"]["txt_12"] = "Befriending The Wolf";
$lang["libros"]["txt_13"] = "Mi cuerpo se había convertido en mi enemigo. Estaba lenta y seguramente matándome implacablemente. Lo único que sentía era un vacío en el estómago, una sensación de desesperanza tan profundo que era un agujero negro, engullendo todo lo demás. No tenía emociones de ningún tipo, no podía sentir enojo, preocupación o miedo. No podía amar ni me importaba nadie, ni yo misma…";
$lang["libros"]["txt_14"] = "Muchas veces sólo quería que mi cuerpo traicionero terminara de una vez con lo que había emprendido. El dolor era horrendo. Mi piel se irritaba en las sábanas. No podía moverme para nada. Estaba desprotegida como un ratón recién nacido, expuesta, vulnerable, débil, en la obscuridad. 
…mi atención estaba completamente enfocada en un sólo pensamiento: prefiero morir que vivir así…";
$lang["libros"]["txt_22"] = "Diericx, Milly, Befriending the Wolf: The Guide to Living with Lupus, Morgan James, N.Y., 2016.";
$lang["libros"]["txt_24"] = "Comprar ahora";


$lang["libros"]["txt_15"] = "Be Love: A Conscious Shift to Birthing the Future";
$lang["libros"]["txt_16"] = "Encontré que el balance es la clave para estar sana, porque si uno de los aspectos de tu vida están fuera de equilibrio, todo empieza a salir mal. Esa es la visión de la Medicina Tradicional China- todo tiene que estar en equilibrio. Eso fue lo que hice con todas estas técnicas. Con paciencia y dedicación pude balancear mis emociones, patrones de pensamiento, y claro, mi cuerpo—el cuerpo y todo lo que necesita para estar bien—y también, el ser completo, que incluye el aspecto espiritual…";
$lang["libros"]["txt_17"] = "Cuando se dice que una persona esta curada, esa sanación tiene que ver con ella completa, es decir, el cuerpo la mente, las emociones y el espíritu.  Ahí es cuando la persona puede accesar a todo su poder, su propia sanación, y su propósito y significado existencial, puede ir hacia sus metas y manifestar lo que vino a manifestar a esta vida, lo cual es asombroso de observar…
Este es el legado que quiero dejar: Todos somos seres extremadamente poderosos; todo lo que nos fijamos como meta lo podemos lograr. Sólo esa idea en nuestras mentes puede inciar la reacción en cadena del cambio.
";

$lang["libros"]["txt_18"] = "En la compra del libro el Método Konscio obtén un regalo. Click aquí.";
$lang["libros"]["txt_19"] = "Escribe tus datos";
$lang["libros"]["txt_20"] = "Adjunta tu pantalla de compra del libro El Método Konscio en Amazon y te enviaremos a tu correo un cuaderno de ejercicios completamente gratis. ";




$lang["cursos_talleres"]["txt_1"] = "CURSOS Y TALLERES";
$lang["cursos_talleres"]["txt_2"] = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean";
$lang["cursos_talleres"]["txt_3"] = "commodo ligula eget dolor. Aenean massa.";
$lang["cursos_talleres"]["txt_4"] = "Inscribirme";
$lang["cursos_talleres"]["txt_5"] = "Quiero Inscribirme";
$lang["cursos_talleres"]["txt_6"] = "Nombre";
$lang["cursos_talleres"]["txt_7"] = "Telefono";
$lang["cursos_talleres"]["txt_8"] = "Correo electrónico";
$lang["cursos_talleres"]["txt_9"] = "Mensaje";
$lang["cursos_talleres"]["txt_10"] = "ENVIAR";


$lang["eventos"]["txt_1"] = "EVENTOS";
$lang["eventos"]["txt_2"] = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean";
$lang["eventos"]["txt_3"] = "commodo ligula eget dolor. Aenean massa.";


$lang["testimonios"]["txt_1"] = "Testimonios";

$lang["cv"]["txt_1"] = "Acreditaciones";
$lang["cv"]["txt_2"] = "Educación";
$lang["cv"]["txt_3"] = "Universidad Iberoamericana: Licenciada en Sociología 13 de Octubre, 1993. Mención honorífica.";
$lang["cv"]["txt_4"] = "Centro Ericksoniano de Mexico, Maestría en Psicoterapia Ericksoniana, 15 de Julio, 2014";
$lang["cv"]["txt_5"] = "Instituto Mexicano de Psicooncología, Doctorado en Tanatología, 10 de Diciembre 2019.";
$lang["cv"]["txt_6"] = "Diplomados";
$lang["cv"]["txt_7"] = "Desarrollo-E: Diplomado en Eneagrama, 9 de Noviembre, 2005.";
$lang["cv"]["txt_8"] = "Asociación de Holographic Repatterning, Sanado el Sistema Familiar, 10 de Febrero, 2006.";
$lang["cv"]["txt_9"] = "Instituto de Gestalt, Reprogramación Energética para la Salud, 18 de Febrero, 2007.";
$lang["cv"]["txt_10"] = "Mary Burmeister, Certificado de Asistencia en el arte de Jin Shin Jyutsu, Agosto 2007.";
$lang["cv"]["txt_11"] = "Rami Rion, Maestría de Reiki, 28 de Octubre, 2008.";
$lang["cv"]["txt_12"] = "Instituto de Resonance Repatterning, Instructor de Un Cambio Cuántico, 10 de Julio, 2010";
$lang["cv"]["txt_13"] = "Omega NYC: Reflexión, Reconexión, Renovación. Tocando el mundo invisible: Descubriendo tu ser espiritual. 20 de Abril, 2012.";
$lang["cv"]["txt_14"] = "Weiss Institute, Entrenamineto de Vidas Pasadas, 8-12 de Octubre, 2012.";
$lang["cv"]["txt_15"] = "The Newton Institute, Terapeuta Certificado en Regresiones a Vida entre Vidas, Enero, 2015.";
$lang["cv"]["txt_16"] = "Access Consiousness, Certificado de Cumplimiento, 28 de Abril, 2016";
$lang["cv"]["txt_17"] = "Stanford Business School,Programa Ejecutivo para Emprendedores Sociales, 8 de Febrero 2019";
$lang["cv"]["txt_18"] = "Bio-Reprogramación Cuántica. 16 de Enero, 2020";
$lang["cv"]["txt_19"] = "Institute for Quantum Consciousness, Entrenamiento de facilitador de Viaje a otras Dimensiones”. 7 de Abril, 2019. Facilitador de Consciencia Universal, 20 de Enero, 2022.";
$lang["cv"]["txt_20"] = "Centre of Excellence, Diploma en Alquimia, 2 de Noviembre, 2022";
$lang["cv"]["txt_21"] = "Método de Resonance Reppatterning por Chloe Wordsworth:";
$lang["cv"]["txt_22"] = "La RR Educación Educativa AC, Facilitador Certificado de  Resonance Repatterning, 3 de Febrero, 2004.";
$lang["cv"]["txt_23"] = "The Holographic Repatterning Association, Facilitador Certificado Nivel 2, Sept 2004";
$lang["cv"]["txt_24"] = "RR Asociación Educativa AC, Certificado, 1 de Febrero 2014";
$lang["cv"]["txt_25"] = "Resonance Repatterning Institute, Facilitador Certificado Avanzado, Abril 2022. ";
$lang["cv"]["txt_26"] = "Otros estudios";
$lang["cv"]["txt_27"] = "Biomagnetismo médico con Isaac Goiz Durán";
$lang["cv"]["txt_28"] = "Astrología con Olvido Ramos, maestra independiente.";


$lang["entrevistas"]["txt_1"] = "Entrevistas";
$lang["entrevistas"]["txt_2"] = "ENTREVISTAS DE RADIO";
$lang["entrevistas"]["txt_3"] = "MyNDTALK, Miércoles de Mujeres - Befriending The Wolf - Entrevista a Milly Diericx, 7 de Diciembre,  2016.";
$lang["entrevistas"]["txt_4"] = "The Dona Seebo Show, Entrevista acerca de Befriending the Wolf, 30 de Marzo, 2017.";
$lang["entrevistas"]["txt_5"] = "It’s Your Health with Lisa Davies, entrevista a Milly Diericx,16 de Diciembre, 2016.";
$lang["entrevistas"]["txt_6"] = "Grupo Fórmula con Leonardo Cursio, entrevista acerca de El Método Konscio,16 de Febrero, 2022. ";
$lang["entrevistas"]["txt_7"] = "Avance Humano con Beatriz Godinez, entrevista acerca de El Método Konscio, Febrero, 2022.";
$lang["entrevistas"]["txt_8"] = "ENTREVISTAS DE TV";
$lang["entrevistas"]["txt_9"] = "NY1 Noticias, La incansable batalla femenina contra el lupus,  11 de Enero, 2017.";
$lang["entrevistas"]["txt_10"] = "Frente al País con Ana Paula Ordorica, reportaje acerca de Clínica Bienestar 360,  16 de Julio, 2018.";
$lang["entrevistas"]["txt_11"] = "Entrevista en Expert TV en el programa La Era de la Nueva Mujer con Elisa Eraña, acerca de Salud Alternativa, martes 7 de Mayo, 2019: ";

$lang["congresos"]["txt_1"] = "Congresos";
$lang["congresos"]["txt_2"] = "XIII Congreso Internacional de tanatología del siglo XXI, con la ponencia “Modelo de la atención tanatológica en pacientes con enfermedades no transmisibles”, 22 de Octubre de 2021.";

$lang["blog"]["txt_1"] = "BLOG";
$lang["blog"]["txt_2"] = "Leer más";


$lang["preguntas"]["txt_1"] = "PREGUNTAS FRECUENTES";
$lang["preguntas"]["txt_2"] = "¿Qué problemas resuelve el Método Konscio?";
$lang["preguntas"]["txt_3"] = "El Método Konscio es un camino de sanación. Debemos de sanar el dolor y sufrimiento que ha causado los problemas en nuestra vida. En este método ya somos perfectos y estamos completos; solo lo hemos olvidado. La sanación es como barrer la basura acumulada que ha nublado nuestra comprensión de nuestra verdadera escencia. Los problemas que tenemos son mala información causada por creencias y emociones limitantes que traen comportamientos negativos y sus consecuencias. ";
$lang["preguntas"]["txt_4"] = "Cuando sanamos las creencias y emociones negativas, los comportamientos negativos cesan y los problemas causados por ellos dejan de ser un obstáculo.";
$lang["preguntas"]["txt_5"] = "¿Qué conlleva el Método Konscio?";
$lang["preguntas"]["txt_6"] = "Este método es un viaje de sanación. Cada persona tiene que hacer el trabajo personal de sanar desde dentro. El método provee las herramientas necesarias para hacerlo, pero cada uno debe de hacer el trabajo. La cantidad de tiempo que se requiere depende de la persona, qué tan listo está para hacerlo, sus mecanismos de defensa y la cantidad de temas involucrados. Es un camino personal, no una competencia.";
$lang["preguntas"]["txt_7"] = "¿Cuánto tarda el Método Konscio?";
$lang["preguntas"]["txt_8"] = "Depende del nivel de compromiso de cada individuo y el número de temas que tenga que resolver. No hay respuesta fácil, pues ha habido personas que tienen una especie de revelación que resuelve todo de una vez, mientras que la mayoría de nosotros debemos de trabajar mucho para lograrlo.";
$lang["preguntas"]["txt_9"] = "Los problemas tienden a ser como capas de una cebolla, resuleves uno y aparece el que sigue, resuleves ese y aparece otro más. Para la mayoría de nosotros, esta es una jornada de vida.";
$lang["preguntas"]["txt_10"] = "¿Cuáles son los resultados del Método Konscio?";
$lang["preguntas"]["txt_11"] = "El incremento en bienestar, salud, abundancia y paz interior son regalos en sí mismos, y seguirán llegando, en incrementos, cada vez que sanas un aspecto de tu vida. Este método es gratificante en todos los momentos. La cantidad de sanación interna que cada persona esté dispuesta a hacer será su premio en sí mismo. Puedes hacerlo al ritmo que sea bueno para ti. Esto es lo que todas las modalidades de terapia y auto ayuda pretenden, sanar diferentes aspectos de la vida.";
$lang["preguntas"]["txt_12"] = "Cada vez que sanas algo, te sientes mejor, y conectas más con tu verdadera escencia.";
$lang["preguntas"]["txt_13"] = "¿Los cambios del Método Konscio son duraderos?";
$lang["preguntas"]["txt_14"] = "¡Sí! Cuando aprendemos algo nuevo, no podemos desaprenderlo. Cuando observamos un nuevo aspecto de nosotros mismos, no podemos des-verlo. Cuando eliminamos un obstáculo caminamos más libremente. Podriámos tratar de regresar a lo que éramos antes, pero tomaría mucho más esfuerzo que aceptar el nuevo camino. Nadie que no quiera un cambio emprenderá algún tipo de camino de transformación y sanación, así cuando algo se ha sanado, ten fe, está sanado. ";
$lang["preguntas"]["txt_15"] = "Si vuelve a surgir, es que no estaba completamente sanado, necesita más trabajo, pero jamás regresará de la misma manera; el trabajo hecho lo habrá modificado, habremos aprendido y crecido con el la experiencia y el esfuerzo realizado.";
$lang["preguntas"]["txt_16"] = "¿El Método Konscio es seguro?";
$lang["preguntas"]["txt_17"] = "Sí lo es. Traerá temas a la superficie para ser sanados. Además es un viaje personal, así que cada persona lo hará a su ritmo y a su tiempo. Si alguno de los ejercicios te suena peligroso, no lo hagas. Si algún acontecimiento es muy fuerte, por favor, pide ayuda. Hay muchos terapeutas que pueden ayudarte con los aspectos complicados, en la escuela que a ti te acomode. Sanar siempre es bueno, aunque a veces el proceso sea un poco doloroso. ";
$lang["preguntas"]["txt_18"] = "Pide cualquier ayuda que necesites; ninguno estamos solos, estamos en esto juntos, y vamos a sanar juntos.";


$lang["contacto"]["txt_1"] = "Envíanos un mensaje";
$lang["contacto"]["txt_2"] = "Con gusto responderémos";
$lang["contacto"]["txt_3"] = "todas tus dudas";
$lang["contacto"]["txt_4"] = "Nombre";
$lang["contacto"]["txt_5"] = "Teléfono";
$lang["contacto"]["txt_6"] = "Correo electrónico";
$lang["contacto"]["txt_7"] = "Mensaje";
$lang["contacto"]["txt_8"] = "ENVIAR";
$lang["contacto"]["txt_9"] = "Aviso de Privacidad";
$lang["contacto"]["txt_17"] = "./assets/pdf/AVISO_DE_PRIVACIDAD.pdf";
$lang["contacto"]["txt_10"] = "Diseño Web: Caja Creativa";

$lang["contacto"]["txt_11"] = "Mensaje enviado";
$lang["contacto"]["txt_12"] = "Mensaje enviado correctamente";
$lang["contacto"]["txt_13"] = "Enviado";
$lang["contacto"]["txt_14"] = "Gracias por contactarnos, nos pondremos en contacto contigo.";

$lang["contacto"]["txt_15"] = "Error";
$lang["contacto"]["txt_16"] = "Intenta actualizar la página.";
<?php 

class Clientes extends CI_Model{
	public $table = "clientes";
    public $table_id = "id";

    //Paginacion a nivel general de los clientes para el admin
    function get_pagination_admin($offset = 0, $posted = 'Si', $order = 'desc', $c_url_clean = null) {
        /*"SELECT * 
        FROM clientes
        WHERE id NOT IN (SELECT DISTINCT id FROM main_clientes)"*/
        $this->db->select();
        $this->db->from("$this->table");
        $this->db->order_by("created_at", $order);
        $this->db->limit(PAGE_SIZE, $offset);
        $query = $this->db->get();

        return $query->result();
    }

    function count_post_admin(){
        $count = $this->db->query("SELECT $this->table_id from $this->table");
        return $count->num_rows();
    }

    function todos(){
        $this->db->select();
        $this->db->from("$this->table");
        $this->db->order_by("created_at", 'desc');
        
        $query = $this->db->get();

        return $query->result();
    }

	function get_pagination($offset = 0, $posted = 'Si', $order = 'desc', $c_url_clean = null) {
        $this->db->from($this->table );
        //$this->db->join("categories as c", "c.category_id = p.category_id");
        if($posted!='admin')
            $this->db->where("posted", $posted);
        if (isset($c_url_clean))
            $this->db->where("c.url_clean", $c_url_clean);
        $this->db->order_by("created_at", $order);
        $this->db->limit(PAGE_SIZE, $offset);
        $query = $this->db->get();

        return $query->result();
    }

	function GetByUrlClean($url_clean, $posted = 'Si') {

        $this->db->select();
        $this->db->from($this->table);
        //$this->db->join("categories as c", "c.category_id = p.category_id");
        $this->db->where("posted", $posted);
        $this->db->where("url_clean", $url_clean);

        $query = $this->db->get();

        return $query->row();
    }

    /*function countByCUrlClean($c_url_clean, $posted = 'Si') {

        $this->db->select('COUNT(p.id) as count');
        $this->db->from("$this->table as p");
        $this->db->join("categories as c", "c.category_id = p.category_id");
        $this->db->where("posted", $posted);
        $this->db->where("c.url_clean", $c_url_clean);
        $query = $this->db->get();
        return $query->row()->count;
    }*/
    function countByCUrlClean($category_id, $posted = 'Si') {
        $this->db->distinct();
        $this->db->select('COUNT(p.id) as count');
        $this->db->from("$this->table as p");
        $this->db->join("categories_clientes as c", "c.id = p.id");
        $this->db->where("posted", $posted);
        $this->db->where("c.category_id", $category_id);
        $this->db->where("p.id NOT IN (SELECT DISTINCT id FROM main_clientes)");
        //$this->db->order_by("p.created_at", "desc");
        $query = $this->db->get();
        return $query->row()->count;
    }

    function count_posted(){
        $this->db->select($this->table_id);
        $this->db->from($this->table);
        $this->db->where('posted','si');
        $count = $this->db->get();
        return $count->num_rows();
	}

	
}
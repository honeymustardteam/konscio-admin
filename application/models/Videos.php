<?php

class Videos extends CI_Model{
	public $table = "videos";
    public $table_id = "post_id";

    //Paginacion a nivel general de los videos para el admin
    function get_pagination_admin($offset = 0, $posted = 'Si', $order = 'asc', $c_url_clean = null) {
        /*"SELECT *
        FROM videos
        WHERE post_id NOT IN (SELECT DISTINCT post_id FROM main_videos)"*/
        $this->db->select();
        $this->db->from("$this->table");
        $this->db->order_by("orden", $order);
        $this->db->limit(PAGE_SIZE, $offset);
        $query = $this->db->get();

        return $query->result();
    }

    function count_post_admin(){
        $count = $this->db->query("SELECT $this->table_id from $this->table");
        return $count->num_rows();
    }

	function get_pagination($offset = 0, $posted = 'Si', $order = 'asc', $c_url_clean = null) {
        $this->db->from($this->table );
        //$this->db->join("categories as c", "c.category_id = p.category_id");
        
        $mostrarIdioma = 'es';
        if($this->session->userdata('site_lang') == 'english'){
            $mostrarIdioma = 'en';
        }


        if($posted!='admin')
            $this->db->where("posted", $posted);
            $this->db->where("show_in", $mostrarIdioma);
            $this->db->or_where("show_in", 'ambos');
        if (isset($c_url_clean))
            $this->db->where("c.url_clean", $c_url_clean);
        $this->db->order_by("orden", $order);
        $this->db->limit(PAGE_SIZE, $offset);
        $query = $this->db->get();

        return $query->result();
    }

	function GetByUrlClean($url_clean, $posted = 'Si') {

        $this->db->select();
        $this->db->from($this->table);
        //$this->db->join("categories as c", "c.category_id = p.category_id");
        $this->db->where("posted", $posted);
        $this->db->where("url_clean", $url_clean);

        $query = $this->db->get();

        return $query->row();
    }


    function find_by_orden($orden) {

        $this->db->select();
        $this->db->from($this->table);
        //$this->db->join("categories as c", "c.category_id = p.category_id");
        $this->db->where("orden", $orden);

        $query = $this->db->get();

        return $query->row();
    }

    /*function countByCUrlClean($c_url_clean, $posted = 'Si') {

        $this->db->select('COUNT(p.post_id) as count');
        $this->db->from("$this->table as p");
        $this->db->join("categories as c", "c.category_id = p.category_id");
        $this->db->where("posted", $posted);
        $this->db->where("c.url_clean", $c_url_clean);
        $query = $this->db->get();
        return $query->row()->count;
    }*/
    function countByCUrlClean($category_id, $posted = 'Si') {
        $this->db->distinct();
        $this->db->select('COUNT(p.post_id) as count');
        $this->db->from("$this->table as p");
        $this->db->join("categories_videos as c", "c.post_id = p.post_id");
        $this->db->where("posted", $posted);
        $this->db->where("c.category_id", $category_id);
        $this->db->where("p.post_id NOT IN (SELECT DISTINCT post_id FROM main_videos)");
        //$this->db->order_by("p.created_at", "desc");
        $query = $this->db->get();
        return $query->row()->count;
    }

    function count_posted(){
        $this->db->select($this->table_id);
        $this->db->from($this->table);
        $this->db->where('posted','si');
        $count = $this->db->get();
        return $count->num_rows();
	}

    function get_all($offset = 0)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->limit(10,$offset);
        $query = $this->db->get();
        $result = $query->result();
        $videos = array();
        $contador = 0;
        foreach($result as $row){
            $video = $row;
            if($contador == 0){
                $video->es_primero = true;
            }
            $contador++;


            $videos[] = $video;
        }

        if(count($videos)){
            $videos[count($videos)-1]->es_ultimo = true;
        }
        return array(
            'Data' => $videos,
            'TotalRows' => count($videos)
        );
    }


}

<div class="row">
	<div class="col-4 col-sm-4 col-xs-12 text-center text-sm-left mb-0">
		<h3 class="page-title">Clientes</h3>
	</div>
	<div class="col-8 col-sm-8 col-xs-12 text-center text-sm-right mb-0 ">
		<a class="btn btn-primary " href="<?php echo base_url() ?>admin/createExcel">Descargar en excel</a>
	</div>
</div>
<div class="row">
	
	<div class="col-12">
		<div class="card card-small mb-4">
			<div class="card-body p-0 pb-3 text-center table-responsive">
				<table class="table mb-0">
					<thead class="bg-light">
						<tr>
							<th scope="col" class="border-0">ID</th>
							<th scope="col" class="border-0">Nombre</th>
							<th scope="col" class="border-0">Correo</th>
							<th scope="col" class="border-0">Telefono</th>
						</tr>
					</thead>
					<tbody>

						<?php foreach($posts as $key => $p) : ?> 
							<tr>
								<td><?= $p->id; ?></td>
								<td><?= $p->nombre ?></td>
								<td><?= $p->correo ?></td>
								<td><?= $p->telefono ?></td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<?php
if ($pagination):

    $prev = $current_page - 1;
    $next = $current_page + 1;

    if ($prev < 1)
        $prev = 1;

    if ($next > $last_page)
        $next = $last_page;
    ?>
    <ul class="pagination pagination-sm row d-flex justify-content-center">
        <!--<li class="page-link"><a class="page-item" href = "<?php //echo base_url() . $token_url . $prev ?>">Prev</a></li>-->
        <?php for ($i = 1; $i <= $last_page; $i++) { ?>
            <li class="page-item"><a class="page-link" style="color:#a1a1a1" href ="<?php echo base_url() . $token_url . $i; ?> "> <?php echo $i; ?></a></li>
        <?php } ?>

        <?php if ($current_page != $next) { ?>
            <!--<li class="page-link"><a class="page-item" href = "<?php //echo base_url() . $token_url . $next; ?> ">Sig</a></li>
            -->
            <?php } ?>
    </ul>
<?php endif; ?>




<script>
	
	var post_id = 0;
	var button_delete;
	var title = '';

</script>
<div class="row">
	<div class="col-lg-9 col-md-12">
		<!-- Add New Post Form -->
		<?php echo form_open('','class="add-new-post" enctype="multipart/form-data"') ?>
		<div class="card card-small mb-3">
			<div class="card-body">
				

				<div class="form-group">
					<?php echo form_label('Título', 'titulo_spanish'); ?> 
					<?php
					$text_input = array(
						'name' => 'titulo_spanish',
						'autofocus' => true,
						'placeholder' => 'Escriba el título del post',
						'required' => true,
						'minlength' => '5',
						'maxlength' => '255',
						'id' => 'titulo_spanish',
						'value' => $titulo_spanish,
						'class' => 'form-control input-lg'
					);
					echo form_input($text_input);
					?> 
					<?php echo form_error('titulo_spanish','<div class="text-error">','</div>'); ?>
				</div>


				<div class="form-group">
					<?php echo form_label('Fecha y hora español', 'fecha_spanish'); ?> 
					<?php
					$text_input = array(
						'name' => 'fecha_spanish',
						'autofocus' => true,
						'placeholder' => 'Escriba el la fecha y hora',
						'minlength' => '5',
						'maxlength' => '255',
						'id' => 'fecha_spanish',
						'value' => $fecha_spanish,
						'class' => 'form-control input-lg'
					);
					echo form_input($text_input);
					?> 
					<?php echo form_error('fecha_spanish','<div class="text-error">','</div>'); ?>
				</div>


				<div class="form-group">
					<?php echo form_label('Duración', 'duracion'); ?> 
					<?php
					$text_input = array(
						'name' => 'duracion',
						'autofocus' => true,
						'placeholder' => 'Escriba la duración',
						'minlength' => '5',
						'maxlength' => '255',
						'id' => 'duracion',
						'value' => $duracion,
						'class' => 'form-control input-lg'
					);
					echo form_input($text_input);
					?> 
					<?php echo form_error('duracion','<div class="text-error">','</div>'); ?>
				</div>

				<div class="form-group">
					<?php echo form_label('Precio', 'precio'); ?> 
					<?php
					$text_input = array(
						'name' => 'precio',
						'autofocus' => true,
						'placeholder' => 'Escriba el precio',
						'minlength' => '5',
						'maxlength' => '255',
						'id' => 'precio',
						'value' => $precio,
						'class' => 'form-control input-lg'
					);
					echo form_input($text_input);
					?> 
					<?php echo form_error('precio','<div class="text-error">','</div>'); ?>
				</div>

				<div class="form-group">
					 
					<?php
					$text_input = array(
						'name' => 'url_clean',
						'id' => 'url_clean',
						'type' => 'hidden',
						'value' => $url_clean,
						'class' => 'form-control input-lg'
					);
					echo form_input($text_input);
					?> 
					<?php echo form_error('url_clean','<div class="text-error">','</div>'); ?>
				</div>

				<div class="form-group">
					<?php echo form_label('Contenido', 'contenido_spanish'); ?> 
					<?php
					$text_area = array(
						'name' => 'contenido_spanish',
						'required' => true,
						'id' => 'contenido_spanish',
						'maxlength' => '715',
						'value' => $contenido_spanish,
						'class' => 'form-control input-lg'
					);
					echo form_textarea($text_area);
					?> 
					<?php echo form_error('contenido_spanish','<div class="text-error">','</div>'); ?>
				</div>





				<!-- Inglés-->
				<div class="form-group">
					<?php echo form_label('Title', 'titulo_english'); ?> 
					<?php
					$text_input = array(
						'name' => 'titulo_english',
						'autofocus' => true,
						'placeholder' => 'Write the title of the post',
						'required' => true,
						'minlength' => '5',
						'maxlength' => '255',
						'id' => 'titulo_english',
						'value' => $titulo_english,
						'class' => 'form-control input-lg'
					);
					echo form_input($text_input);
					?> 
					<?php echo form_error('titulo_english','<div class="text-error">','</div>'); ?>
				</div>

				<div class="form-group">
					<?php echo form_label('Fecha y hora ingles', 'fecha_english'); ?> 
					<?php
					$text_input = array(
						'name' => 'fecha_english',
						'autofocus' => true,
						'placeholder' => 'Escriba el la fecha y hora',
						'minlength' => '5',
						'maxlength' => '255',
						'id' => 'fecha_english',
						'value' => $fecha_english,
						'class' => 'form-control input-lg'
					);
					echo form_input($text_input);
					?> 
					<?php echo form_error('fecha_english','<div class="text-error">','</div>'); ?>
				</div>


				<div class="form-group">
					<?php echo form_label('Content', 'contenido_english'); ?> 
					<?php
					$text_area = array(
						'name' => 'contenido_english',
						'required' => true,
						'id' => 'contenido_english',
						'maxlength' => '715',
						'value' => $contenido_english,
						'class' => 'form-control input-lg'
					);
					echo form_textarea($text_area);
					?> 
					<?php echo form_error('contenido_english','<div class="text-error">','</div>'); ?>
				</div>
				<div class="form-group">
					<?php echo form_label('Imagen de portada', 'cover_image'); ?> 
					<?php
					$input_cover = array(
						'name' => 'cover_upload',
						'id' => 'cover_upload',
						'value' => '',
						'type' => 'file',
						'accept' => '.jpg, .png, .jpeg',
						'class' => 'form-control input-lg'
					);
					echo form_input($input_cover);
					?> 
					<?php echo form_error('cover_upload','<div class="text-error">','</div>'); ?>
					<?php echo $cover_image!="" ? '<img class="img_post img-thumbnail img-presentation-small" src="'.base_url().'uploads/cover_post/'.$cover_image.'"></img>' : ''; ?>
				</div>

				<?php echo form_submit('mySubmit', 'Guardar', 'class="btn btn-primary"'); ?> 

				 
			</div>
		</div>
		<!-- / Add New Post Form -->
	</div>

	<div class="col-lg-3 col-md-12">
		<!-- Post Overview -->
		<div class='card card-small mb-3'>
			<div class="card-header border-bottom">
				<h6 class="m-0">Acciones</h6>
			</div>
			<div class='card-body p-0'>
				<ul class="list-group list-group-flush">
					<li class="list-group-item p-3">
						<div class="form-group">
								<i class="material-icons mr-1">visibility</i>
								<?php echo form_label('Publicado', 'posted'); ?>
								<?php echo form_dropdown('posted', $data_posted, $posted, 'class="form-control input-lg"'); ?>  
								<?php echo form_error('posted','<div class="text-error">','</div>'); ?>
						</div>
					</li>

						</ul>
					</div>
				</div>
		</div>
		<?php echo form_close() ?>
		<!-- / Post Overview -->
	</div>
</div>

<script>
	
	$(function(){
		CKEDITOR.replace( 'contenido_spanish', {
			height:400,
		    filebrowserBrowseUrl: "<?php echo base_url() ?>admin/images_server",
		    filebrowserUploadUrl: "<?php echo base_url() ?>admin/upload",
		});

		CKEDITOR.replace( 'contenido_english', {
			height:400,
		    filebrowserBrowseUrl: "<?php echo base_url() ?>admin/images_server",
		    filebrowserUploadUrl: "<?php echo base_url() ?>admin/upload",
		});
	});

	//Validar tamaño de las imagenes

	var uploadCoverImage = document.getElementById("cover_upload");

	uploadCoverImage.onchange = function() {
		if(this.files[0].size > 1048576){
		alert("EL tamaño del archivo debe ser menor a 1mb");
		this.value = "";
		};
	};

</script>
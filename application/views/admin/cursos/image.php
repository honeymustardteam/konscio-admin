<!DOCTYPE html>
<html>
<head>
	<title>Admin</title>
	<meta name="description" content="A high-quality &amp; free Bootstrap admin dashboard template pack that comes with lots of templates and components.">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" id="main-stylesheet" data-version="1.1.0" href="<?php echo base_url() ?>assets/css/shards-dashboards.1.1.0.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/extras.1.1.0.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/custom.css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
</head>
<body>

	<?php foreach($images as $key=> $i) : ?>
		<img class="img-presentation-small img-thumbnail" src="<?php echo '/'.PROJECT_FOLDER.'/uploads/post/'.$i; ?>">
	<?php endforeach; ?>

	<script>
		var fileUrl = '';
		$('img').click(function(event) {
			fileUrl = $(this).attr("src");
			returnFileUrl();
		});

		function getUrlParam( paramName ) {
            var reParam = new RegExp( '(?:[\?&]|&)' + paramName + '=([^&]+)', 'i' );
            var match = window.location.search.match( reParam );

            return ( match && match.length > 1 ) ? match[1] : null;
        }
        // Simulate user action of selecting a file to be returned to CKEditor.
        function returnFileUrl() {
        	if(fileUrl==""){
        		return
        	}
            var funcNum = getUrlParam( 'CKEditorFuncNum' );
            window.opener.CKEDITOR.tools.callFunction( funcNum, fileUrl );
            window.close();
        }
	</script>

</body>
</html>
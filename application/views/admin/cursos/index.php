<div class="row">
	<div class="col-4 col-sm-4 col-xs-12 text-center text-sm-left mb-0">
		<h3 class="page-title">Cursos</h3>
	</div>
	<div class="col-8 col-sm-8 col-xs-12 text-center text-sm-right mb-0 ">
		<a class="btn btn-primary " href="<?php echo base_url() ?>admin/curso">Crear curso</a>
	</div>
</div>
<div class="row">
	
	<div class="col-12">
		<div class="card card-small mb-4">
			<div class="card-body p-0 pb-3 text-center table-responsive">
				<table class="table mb-0">
					<thead class="bg-light">
						<tr>
							<th scope="col" class="border-0">Título</th>
							<th scope="col" class="border-0">Fecha</th>
							<th scope="col" class="border-0">Imagen</th>
							<th scope="col" class="border-0">Publicado</th>
							<th scope="col" class="border-0">Acciones</th>
						</tr>
					</thead>
					<tbody>

						<?php foreach($posts as $key => $p) : ?> 
							<tr>
								<td><?php echo word_limiter($p->titulo_spanish,4) ?></td>
								<td><?php echo $p->fecha_spanish ?></td>
								<td><?php echo $p->cover_image!="" ? '<img class="img_post img-thumbnail img-presentation-small" src="'.base_url().'uploads/cover_post/'.$p->cover_image.'"></img>' : ''; ?></td>
								<td><?php echo $p->posted ?></td>
								<td>
									<!--Editar-->
									<a class="btn btn-sm btn-primary" title="Editar" href="<?php echo base_url()."admin/curso/".$p->post_id ?>">
										<i class="material-icons mr-1">edit</i>
									</a>
									<!--Principal-->
									<!--<a class="btn btn-sm btn-primary" title="Establecer en principales" href="#" data-title="<?php //echo $p->titulo_spanish; ?>"
									data-postid="<?php //echo$p->post_id ?>" data-toggle="modal" data-target="#establecerPrincipalModal">
										<i class="material-icons mr-1">filter</i>
									</a>-->
									<!--Eliminar-->
									<a class="btn btn-sm btn-danger" title="Eliminar" href="#" data-title="<?php echo $p->titulo_spanish; ?>" 
									data-postid="<?php echo$p->post_id ?>" data-toggle="modal" data-target="#deleteModal">
										<i class="material-icons mr-1">delete</i>
									</a>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<?php
if ($pagination):

    $prev = $current_page - 1;
    $next = $current_page + 1;

    if ($prev < 1)
        $prev = 1;

    if ($next > $last_page)
        $next = $last_page;
    ?>
    <ul class="pagination pagination-sm row d-flex justify-content-center">
        <!--<li class="page-link"><a class="page-item" href = "<?php //echo base_url() . $token_url . $prev ?>">Prev</a></li>-->
        <?php for ($i = 1; $i <= $last_page; $i++) { ?>
            <li class="page-item"><a class="page-link" style="color:#a1a1a1" href ="<?php echo base_url() . $token_url . $i; ?> "> <?php echo $i; ?></a></li>
        <?php } ?>

        <?php if ($current_page != $next) { ?>
            <!--<li class="page-link"><a class="page-item" href = "<?php //echo base_url() . $token_url . $next; ?> ">Sig</a></li>
            -->
            <?php } ?>
    </ul>
<?php endif; ?>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="titulo">Post</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!--<div class="modal-body">

      </div>-->
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" id="borrar_post">Eliminar</button>
      </div>
    </div>
  </div>
</div>

<!--<div class="modal fade" id="establecerPrincipalModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title-principal" id="titulo-principal">Post</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="establecer_principal">Aceptar</button>
      </div>
    </div>
  </div>
</div>
-->

<script>
	
	var post_id = 0;
	var button_delete;
	var title = '';
	//Abrir modal para eliminar
	$('#deleteModal').on('show.bs.modal', function (event) {
	  button_delete = $(event.relatedTarget) // Button that triggered the modal
	  post_id = button_delete.data('postid') // Extract info from data-* attributes
	  title = button_delete.data('title')
	  var modal = $(this)
	  modal.find('.modal-title').text('¿Seguro que desea eliminar el evento ' + title+" ?");
	  //modal.find('.modal-body input').val(post_id)
	})

	//Click boton eliminar
	$("#borrar_post").click(function(event) {
		//console.log("Borrar post " + post_id)
		$.ajax({
			url: '<?php echo base_url() ?>admin/curso_delete/'+post_id,
			//type: 'POST',
		})
		.done(function(resp) {
			console.log("success");
			console.log(resp)
			if(resp==1){
				$(button_delete).parent().parent().remove();
			}
		})
		.fail(function(error) {
			alert("Ocurrió un error inserperado")
			console.log("error");
			console.log(error);
		})
		
	});

	//Abrir modal para establecer pincipal
	/*
	$('#establecerPrincipalModal').on('show.bs.modal', function (event) {
	  button_delete = $(event.relatedTarget) // Button that triggered the modal
	  post_id = button_delete.data('postid') // Extract info from data-* attributes
	  title = button_delete.data('title')
	  var modal = $(this)
	  modal.find('.modal-title-principal').text('¿Seguro que desea establecer como principal el post ' + title+" ?");
	  //modal.find('.modal-body input').val(post_id)
	})

	//Click boton eliminar
	$("#establecer_principal").click(function(event) {
		//console.log("Borrar post " + post_id)
		$.ajax({
			url: '<?php //echo base_url() ?>admin/post_principal/'+post_id,
			//type: 'POST',
		})
		.done(function(resp) {
			console.log("success")
			console.log(resp)
			if(resp==1){
				alert("El post ha sido añadido a los principales")
			}
		})
		.fail(function(error) {
			alert("Ocurrió un error inserperado")
			console.log("error")
			console.log(error)
		})
		
	});
	*/
</script>
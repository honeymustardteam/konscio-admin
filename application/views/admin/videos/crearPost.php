<div class="row">
	<div class="col-lg-9 col-md-12">
		<!-- Add New Post Form -->
		<?php echo form_open('','class="add-new-post" enctype="multipart/form-data"') ?>
		<div class="card card-small mb-3">
			<div class="card-body">
				

				<div class="form-group">
					<?php echo form_label('Título', 'titulo_spanish'); ?> 
					<?php
					$text_input = array(
						'name' => 'titulo_spanish',
						'autofocus' => true,
						'placeholder' => 'Escriba el título del video.',
						
						'minlength' => '5',
						'maxlength' => '255',
						'id' => 'titulo_spanish',
						'value' => $titulo_spanish,
						'class' => 'form-control input-lg'
					);
					echo form_input($text_input);
					?> 
					<?php echo form_error('titulo_spanish','<div class="text-error">','</div>'); ?>
				</div>

				<div class="form-group">
					 
					<?php
					$text_input = array(
						'name' => 'url_clean',
						'id' => 'url_clean',
						'type' => 'hidden',
						'value' => $url_clean,
						'class' => 'form-control input-lg'
					);
					echo form_input($text_input);
					?> 
					<?php echo form_error('url_clean','<div class="text-error">','</div>'); ?>
				</div>

				<div class="form-group">
					<?php echo form_label('URL youtube español', 'url_spanish'); ?> 
					<?php
					$text_area = array(
						'name' => 'url_spanish',
						'id' => 'url_spanish',
						'value' => $url_spanish,
						'class' => 'form-control input-lg'
					);
					echo form_input($text_area);
					?> 
					<?php echo form_error('url_spanish','<div class="text-error">','</div>'); ?>
				</div>
				<div class="form-group">
					<?php echo form_label('Imagen de portada español', 'cover_image_es'); ?> 
					<?php
					$input_cover = array(
						'name' => 'cover_upload_es',
						'id' => 'cover_upload_es',
						'value' => '',
						'type' => 'file',
						'accept' => '.jpg, .png, .jpeg',
						'class' => 'form-control input-lg'
					);
					echo form_input($input_cover);
					?> 
					<?php echo form_error('cover_upload_es','<div class="text-error">','</div>'); ?>
					<?php echo $cover_image_es!="" ? '<img class="img_post img-thumbnail img-presentation-small" src="'.base_url().'uploads/cover_post/'.$cover_image_es.'"></img>' : ''; ?>
				</div>

				<br><br><br>








				<!-- Inglés-->
				<div class="form-group">
					<?php echo form_label('Title', 'titulo_english'); ?> 
					<?php
					$text_input = array(
						'name' => 'titulo_english',
						'autofocus' => true,
						'placeholder' => 'Write the title of the video',
					
						'minlength' => '5',
						'maxlength' => '255',
						'id' => 'titulo_english',
						'value' => $titulo_english,
						'class' => 'form-control input-lg'
					);
					echo form_input($text_input);
					?> 
					<?php echo form_error('titulo_english','<div class="text-error">','</div>'); ?>
				</div>


				<div class="form-group">
					<?php echo form_label('URL youtube English', 'url_english'); ?> 
					<?php
					$text_area = array(
						'name' => 'url_english',
					
						'id' => 'url_english',
						'value' => $url_english,
						'class' => 'form-control input-lg'
					);
					echo form_input($text_area);
					?> 
					<?php echo form_error('url_english','<div class="text-error">','</div>'); ?>
				</div>
				


				<div class="form-group">
					<?php echo form_label('Cover image english', 'cover_image_en'); ?> 
					<?php
					$input_cover = array(
						'name' => 'cover_upload_en',
						'id' => 'cover_upload_en',
						'value' => '',
						'type' => 'file',
						'accept' => '.jpg, .png, .jpeg',
						'class' => 'form-control input-lg'
					);
					echo form_input($input_cover);
					?> 
					<?php echo form_error('cover_upload_en','<div class="text-error">','</div>'); ?>
					<?php echo $cover_image_en!="" ? '<img class="img_post img-thumbnail img-presentation-small" src="'.base_url().'uploads/cover_post/'.$cover_image_en.'"></img>' : ''; ?>
				</div>

				<?php echo form_submit('mySubmit', 'Guardar', 'class="btn btn-primary"'); ?> 

				 
			</div>
		</div>
		<!-- / Add New Post Form -->
	</div>

	<div class="col-lg-3 col-md-12">
		<!-- Post Overview -->
		<div class='card card-small mb-3'>
			<div class="card-header border-bottom">
				<h6 class="m-0">Acciones</h6>
			</div>
			<div class='card-body p-0'>
				<ul class="list-group list-group-flush">
					<li class="list-group-item p-3">
						<div class="form-group">
								<i class="material-icons mr-1">visibility</i>
								<?php echo form_label('Publicado', 'posted'); ?>
								<?php echo form_dropdown('posted', $data_posted, $posted, 'class="form-control input-lg"'); ?>  
								<?php echo form_error('posted','<div class="text-error">','</div>'); ?>
						</div>
					</li>


					<li class="list-group-item p-3">
						<div class="form-group">
								<i class="material-icons mr-1">visibility</i>
								<?php echo form_label('Idioma', 'show_in'); ?>
								<?php echo form_dropdown('show_in', $data_mostrar, $show_in, 'class="form-control input-lg"'); ?>  
								<?php echo form_error('show_in','<div class="text-error">','</div>'); ?>
						</div>
					</li>

						</ul>
					</div>
				</div>
		</div>
		<?php echo form_close() ?>
		<!-- / Post Overview -->
	</div>
</div>

<script>
	

	//Validar tamaño de las imagenes

	

</script>
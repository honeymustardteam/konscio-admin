 <!-- AUTORA PORTADA. -->
 <div class="container-fluid">
     <div class="row align-items-between">
         <div class="col-lg-12 col-md-12 Autoraportada">
             <img class="ondas1" src="./assets/img/Autora/portadaAurora1.png">
         </div>
     </div>
 </div>

 <!-- Acerca de laAutora -->
 <div class="container-fluid justify-content-center">
     <div class="row align-items-between justify-content-center Autora">
         <div class="col-lg-5 col-md-12 "> </div>
         <div class="col-lg-7 col-md-12 ">
             <h1 class="h1Acercade">
                 <?=$this->lang->line("acerca")["txt_1"];?>
             </h1>
         </div>
     </div>
     <div class="row align-items-between justify-content-center ">
         <div class="col-lg-5 col-md-12 ">
             <img class="FotoAutora1" style=" margin-top: 0; width: 80%;" src="./assets/img/Autora/FotoAutora.png">
         </div>
         <div class="col-lg-7 col-md-12 ">
             <p class="AutoraText">
                 <!-- <b><?=$this->lang->line("acerca")["txt_2"];?></b> -->
                 <br><?=$this->lang->line("acerca")["txt_3"];?> <br><br>
                 <?=$this->lang->line("acerca")["txt_4"];?><br><br>

                 <!-- <a id="link" href="#contacto" target=""> <?=$this->lang->line("acerca")["txt_4b"];?></a><br> -->

                 <?=$this->lang->line("acerca")["txt_5"];?>
                 <b><?=$this->lang->line("acerca")["txt_6"];?></b>
                 <?=$this->lang->line("acerca")["txt_7"];?>

                 <!-- <a id="link" href="https://www.Konscio.com">www.konscio.mx </a> -->
             </p>
         </div>



     </div>

 </div>

 <!-- Terapias -->
 <div id="terapias"></div>
 <div>
     <div class="row align-items-between justify-content-center">
         <div class="col-lg-4 col-md-12 "> </div>
         <div class="col-lg-4 col-md-12 ">
             <h1 class="subAcercaDe">
                 <?=$this->lang->line("terapias")["txt_1"];?>
             </h1>
         </div>
         <div class="col-lg-4 col-md-12 "> </div>
     </div>
     <div class="row align-items-between justify-content-center">
         <div class="col-lg-9 col-md-12 ">
             <h4 class="subTerapias">
                 <?=$this->lang->line("terapias")["txt_2"];?>
             </h4>
             <p class="txtTerapias">
                 <?=$this->lang->line("terapias")["txt_3"];?>
             </p>
             <h4 class="subTerapias">
                 <?=$this->lang->line("terapias")["txt_4"];?>
             </h4>
             <p class="txtTerapias">
                 <?=$this->lang->line("terapias")["txt_5"];?>
             </p>
             <h4 class="subTerapias">
                 <?=$this->lang->line("terapias")["txt_6"];?>
             </h4>
             <p class="txtTerapias">
                 <?=$this->lang->line("terapias")["txt_7"];?>
             </p>
             <h4 class="subTerapias">
                 <?=$this->lang->line("terapias")["txt_8"];?>
             </h4>
             <p class="txtTerapias">
                 <?=$this->lang->line("terapias")["txt_9"];?>
             </p>
             <h4 class="subTerapias">
                 <?=$this->lang->line("terapias")["txt_10"];?>
             </h4>
             <p class="txtTerapias">
                 <?=$this->lang->line("terapias")["txt_11"];?>
             </p>
             <h4 class="subTerapias">
                 <?=$this->lang->line("terapias")["txt_12"];?>
             </h4>
             <p class="txtTerapias">
                 <?=$this->lang->line("terapias")["txt_13"];?>
             </p>
             <h4 class="subTerapias">
                 <?=$this->lang->line("terapias")["txt_14"];?>
             </h4>
             <p class="txtTerapias">
                 <?=$this->lang->line("terapias")["txt_15"];?>
             </p>
             <h4 class="subTerapias">
                 <?=$this->lang->line("terapias")["txt_16"];?>
             </h4>
             <p class="txtTerapias">
                 <?=$this->lang->line("terapias")["txt_17"];?>
             </p>
             <h4 class="subTerapias">
                 <?=$this->lang->line("terapias")["txt_18"];?>
             </h4>
             <p class="txtTerapias">
                 <?=$this->lang->line("terapias")["txt_19"];?>
             </p>
             <h4 class="subTerapias">
                 <?=$this->lang->line("terapias")["txt_20"];?>
             </h4>
             <p class="txtTerapias">
                 <?=$this->lang->line("terapias")["txt_21"];?>
             </p>
             <h4 class="subTerapias">
                 <?=$this->lang->line("terapias")["txt_22"];?>
             </h4>
             <p class="txtTerapias">
                 <?=$this->lang->line("terapias")["txt_23"];?>
             </p>
             <h4 class="subTerapias">
                 <?=$this->lang->line("terapias")["txt_24"];?>
             </h4>
             <p class="txtTerapias">
                 <?=$this->lang->line("terapias")["txt_25"];?>
             </p>
             <h4 class="subTerapias">
                 <?=$this->lang->line("terapias")["txt_26"];?>
             </h4>
             <p class="txtTerapias">
                 <?=$this->lang->line("terapias")["txt_27"];?>
             </p>

         </div>
     </div>

 </div>

 <!-- Testimonios -->
 <div id="testimonios"></div>
 <div class="container-fluid justify-content-center TestimoniosBack">
     <div class="row align-items-between justify-content-center">
         <div class="col-lg-12 col-md-12 ">
             <div class="espacioTest">
                 <h1 class="subAcercaDe">
                     <?=$this->lang->line("testimonios")["txt_1"];?>
                 </h1>
             </div>
         </div>
     </div>
     <div class="testimonios_wrap">
         <?php foreach ($posts_testimonios as $key => $p) : $titulo = 'titulo_'.$ses; $contenido = 'contenido_'.$ses;?>
         <div class="row align-items-between  testimonios">
             <div class="col-lg-2 col-md-12 testimonioIzq">
                 <img class="imgTest"
                     src="<?php echo $p->cover_image !=""? base_url()."/uploads/cover_post/".$p->cover_image :'' ?>">
             </div>
             <div class="col-lg-10 col-md-12 testimonioDer">
                 <span class="nomTest"> <?= $p->$titulo ?></span><br>
                 <p class="Comillas">
                     <img class="imgComillas" src="./assets/img/Testimonios/comillas.png">
                     <span class="contTest"><?=$p->$contenido;?></span>
                 </p>
             </div>
         </div>
         <?php endforeach; ?>
     </div>

 </div>

 <!-- CV -->
 <div id="cv"></div>
 <div class="container-fluid justify-content-center ">
     <div class="row align-items-between justify-content-center">
         <div class="col-lg-12 col-md-12 ">
             <div class="espacioTest">
                 <h1 class="subAcercaDe">
                     <?=$this->lang->line("cv")["txt_1"];?>
                 </h1>
             </div>
         </div>
     </div>

     <div class="row align-items-between justify-content-center">
         <div class="col-lg-9 col-md-12 ">
             <h4 class="subTerapias">
                 <?=$this->lang->line("cv")["txt_2"];?>
             </h4>
             <ul>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("cv")["txt_3"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("cv")["txt_4"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("cv")["txt_5"];?>
                     </p>
                 </li>
             </ul>
             <h4 class="subTerapias">
                 <?=$this->lang->line("cv")["txt_6"];?>
             </h4>
             <ul>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("cv")["txt_7"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("cv")["txt_8"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("cv")["txt_9"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("cv")["txt_10"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("cv")["txt_11"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("cv")["txt_12"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("cv")["txt_13"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("cv")["txt_14"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("cv")["txt_15"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("cv")["txt_16"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("cv")["txt_17"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("cv")["txt_18"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("cv")["txt_19"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("cv")["txt_20"];?>
                     </p>
                 </li>
             </ul>
             <h4 class="subTerapias">
                 <?=$this->lang->line("cv")["txt_21"];?>
             </h4>
             <ul>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("cv")["txt_22"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("cv")["txt_23"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("cv")["txt_24"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("cv")["txt_25"];?>
                     </p>
                 </li>
             </ul>

             <h4 class="subTerapias">
                 <?=$this->lang->line("cv")["txt_26"];?>
             </h4>
             <ul>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("cv")["txt_27"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("cv")["txt_28"];?>
                     </p>
                 </li>
             </ul>
         </div>
     </div>
 </div>

 <!-- Libros -->
 <div id="libros"></div>
 <div class="container-fluid justify-content-center bkg">
     <!-- LIBROS titulo y texto -->
     <div class="row align-items-between justify-content-center">
         <div class="col-lg-12 col-md-12 ">
             <div class="espacioTest">
                 <h1 class="subAcercaDe">
                     <?=$this->lang->line("libros")["txt_1"];?>
                 </h1>
             </div>
         </div>
     </div>

     <div class="row align-items-between justify-content-center rowLibros">
         <div class="col-lg-4 col-md-12 colLibros">
             <div class="libro">
                 <img class="imgLibros" style="width: 200px;height: 263px;" src="./assets/img/Libros/libro-konscio.png">
             </div>
             <div class="infoLibro">
                 <p class="nomLibro">
                     <img class="iconoLibro" style="width: 43px;height: 20px;"
                         src="./assets/img/LogosIconos/icono-libro.png">
                     <span class="nomLibro_titulo"> <?=$this->lang->line("libros")["txt_3"];?><br></span>
                 </p>

                 <div class="description-section">
                     <p class="resLibro">
                         <span><?=$this->lang->line("libros")["txt_21"];?></span>
                     </p>
                     <a href="https://swiy.co/konsciopago" target="_blank"> <span style="color: #0d6efd;"><?=$this->lang->line("libros")["txt_23"];?></span></a>


                 </div>
             </div>
         </div>

         <div class="col-lg-4 col-md-12 colLibros">
             <div class="libro">
                 <img class="imgLibros" style="width: 200px;height: 263px;" src="./assets/img/Libros/libro-wolf.png">
             </div>
             <div class="infoLibro">
                 <p class="nomLibro">
                     <img class="iconoLibro" style="width: 43px;height: 20px;"
                         src="./assets/img/LogosIconos/icono-libro.png">
                     <span class="nomLibro_titulo"> <?=$this->lang->line("libros")["txt_12"];?><br></span>
                 </p>
                 <div class="description-section">

                     <p class="resLibro">
                         <span><?=$this->lang->line("libros")["txt_22"];?></span>
                     </p>
                     
                     <a href="https://www.amazon.com/Befriending-Wolf-Guide-Living-Thriving/dp/1630478741/ref=sr_1_1?ie=UTF8&qid=1461281537&sr=8-1&keywords=befriending+the+wolf" target="_blank"> <span style="color: #0d6efd;"><?=$this->lang->line("libros")["txt_24"];?></span></a>

                 </div>

             </div>
         </div>



     </div>
 </div>

 <!-- Publicaciones -->
 <div id="publicaciones"></div>
 <div>
     <div class="row align-items-between justify-content-center">
         <div class="col-lg-4 col-md-12 "> </div>
         <div class="col-lg-4 col-md-12 ">
             <h1 class="subAcercaDe">
                 <?=$this->lang->line("publicaciones")["txt_1"];?>
             </h1>
         </div>
         <div class="col-lg-4 col-md-12 "> </div>
     </div>
     <div class="row align-items-between justify-content-center">
         <div class="col-lg-9 col-md-12 ">
             <ul>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("publicaciones")["txt_2"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("publicaciones")["txt_3"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("publicaciones")["txt_4"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("publicaciones")["txt_5"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("publicaciones")["txt_6"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtCV">
                         <?=$this->lang->line("publicaciones")["txt_7"];?>
                     </p>
                 </li>

             </ul>
         </div>
     </div>

 </div>

 <!-- Entrevistas -->
 <div id="entrevistas"></div>
 <div>
     <div class="row align-items-between justify-content-center">
         <div class="col-lg-4 col-md-12 "> </div>
         <div class="col-lg-4 col-md-12 ">
             <h1 class="subAcercaDe">
                 <?=$this->lang->line("entrevistas")["txt_1"];?>
             </h1>
         </div>
         <div class="col-lg-4 col-md-12 "> </div>
     </div>
     <div class="row align-items-between justify-content-center">
         <div class="col-lg-9 col-md-12 ">

             <h4 class="subTerapias">
                 <?=$this->lang->line("entrevistas")["txt_2"];?>
             </h4>
             <ul>
                 <li>
                     <p class="txtTerapias">
                         <?=$this->lang->line("entrevistas")["txt_3"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtTerapias">
                         <?=$this->lang->line("entrevistas")["txt_4"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtTerapias">
                         <?=$this->lang->line("entrevistas")["txt_5"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtTerapias">
                         <?=$this->lang->line("entrevistas")["txt_6"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtTerapias">
                         <?=$this->lang->line("entrevistas")["txt_7"];?>
                     </p>
                 </li>
             </ul>
             <h4 class="subTerapias">
                 <?=$this->lang->line("entrevistas")["txt_8"];?>
             </h4>
             <ul>
                 <li>
                     <p class="txtTerapias">
                         <?=$this->lang->line("entrevistas")["txt_9"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtTerapias">
                         <?=$this->lang->line("entrevistas")["txt_10"];?>
                     </p>
                 </li>
                 <li>
                     <p class="txtTerapias">
                         <?=$this->lang->line("entrevistas")["txt_11"];?>
                     </p>
                 </li>
                
             </ul>
         </div>
     </div>

 </div>

 <!-- Congresos -->
 <div id="congresos"></div>
 <div>
     <div class="row align-items-between justify-content-center">
         <div class="col-lg-4 col-md-12 "> </div>
         <div class="col-lg-4 col-md-12 ">
             <h1 class="subAcercaDe">
                 <?=$this->lang->line("congresos")["txt_1"];?>
             </h1>
         </div>
         <div class="col-lg-4 col-md-12 "> </div>
     </div>
     <div class="row align-items-between justify-content-center">
         <div class="col-lg-9 col-md-12 ">
             <ul>
                 <li>
                     <p class="txtTerapias">
                         <?=$this->lang->line("congresos")["txt_2"];?>
                     </p>
                 </li>
             </ul>
         </div>
     </div>

 </div>

 <!-- Líneas -->
 <div class="col-lg-12 col-md-12 contenido">
     <img class="ondas" src="./assets/img/Autora/ondasAzules.png">
 </div>
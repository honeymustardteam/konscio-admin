<!-- PORTADA. -->
<div class="container-fluid">
    <div class="row align-items-between justify-content-center " >    
    <div class="col-lg-12 col-md-12 " style="padding:0;">              
    <img class="imgportadaArticulo" src="<?php echo $post->cover_image !=""? base_url()."/uploads/cover_post/".$post->cover_image :'' ?>">  
               
    <p class="tituloArticulo">
        <?php if($ses==='spanish') :?>
            <?= $post->title ?>
        <?php else :?>
            <?= $post->titlee ?>
        <?php endif;?>
        </p>                                                  
    </div>   
    
    </div>
    <div class="row align-items-between justify-content-center contenidoArt">   
    <div class="col-lg-11 col-md-12 contArt "> 
        <p class="fechaArt">
            <?php if($ses==='spanish') :?>
                <?= format_date_espa($post->created_at) ?>
            <?php else :?>
                <?= format_date($post->created_at) ?>
            <?php endif;?>
        </p> </div> 
    <div class="col-lg-9 col-md-12 contArt ">              
        <p class="textoArticulo">
            <?php if($ses==='spanish') :?>
                <?= nl2br($post->content) ?>
            <?php else :?>
                <?= nl2br($post->content1) ?>
            <?php endif;?>
        </p>                                     
    </div>               
    </div>
</div>

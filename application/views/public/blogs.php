 <!-- CURSOS Y TALLERES -->  
 <div class="container-fluid justify-content-center Blog">
          
          <div class="row align-items-between justify-content-center">    
            <div class="col-lg-12 col-md-12 ">  
            <div class="espacioBlog">
              <h1 class="h1Blog">
              <?=$this->lang->line("blog")["txt_1"];?>
              </h1>
            </div>                      
            </div>
            </div>

      

          

          <?php foreach ($posts as $key => $p) : ?>

            <div class="row align-items-between justify-content-center topBlog" >              
              <div class="col-lg-6 col-md-4"> 
                <img class="imgBlog" src="<?php echo $p->cover_image !=""? base_url()."/uploads/cover_post/".$p->cover_image :'' ?>">  
              </div>

              <div class="col-lg-5 col-md-4 infoBlog">                 
                <p class="nombreBlog">
                  <?php if($ses==='spanish') :?>
                      <?= $p->title ?>
                  <?php else :?>
                      <?= $p->titlee ?>
                  <?php endif;?>
                </p> 
                <p class="fechaBlog">
                  <?php if($ses==='spanish') :?>
                     <?= format_date_espa($p->created_at) ?>
                  <?php else :?>
                      <?= format_date($p->created_at) ?>
                  <?php endif;?><br>
                </p>
                <p class="resBlog">
                  <?php if($ses==='spanish') :?>
                      <?= word_limiter ($p->content,81) ?>
                  <?php else :?>
                      <?= word_limiter ($p->content1,81) ?>
                  <?php endif;?>
                </p>
                <p class="leermas">
                  <a href="<?php echo base_url() . 'articulo/'. $p->url_clean ?>" class="linkBlog"><?=$this->lang->line("blog")["txt_2"];?></a>
                </p>
              </div>
            </div>

          <?php endforeach; ?>




      </div>  
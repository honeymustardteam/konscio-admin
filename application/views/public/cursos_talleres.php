<!-- CURSOS Y TALLERES -->
<div class="container-fluid justify-content-center HomeCursos">

    <div class="row align-items-between justify-content-center">
        <div class="col-lg-12 col-md-12 ">
            <div class="espacioCT">
                <h1 class="h1CursosT">
                    <?=$this->lang->line("cursos_talleres")["txt_1"];?>
                </h1>
            </div>
            <p class="HomeTextCursos">
                <!--<?=$this->lang->line("cursos_talleres")["txt_2"];?> <br><?=$this->lang->line("cursos_talleres")["txt_3"];?> -->
            </p>
        </div>
    </div>

    <div class="row align-items-between justify-content-center rowCursos">


        <?php foreach ($posts as $key => $p) : $titulo = 'titulo_'.$ses; $contenido = 'contenido_'.$ses;?>
        <div class="col-lg-4 col-md-4 colCursos">
            <div class="rectanguloCT">
                <img class="imgCursos"
                    src="<?php echo $p->cover_image !=""? base_url()."/uploads/cover_post/".$p->cover_image :'' ?>">
                <p class="nomCurso">
                    <?=  $p->$titulo ?>
                </p>
                <p class="calCurso duracion">
                    <span>
                        <?php if($ses==='spanish') :?>

                        Duración: <?= $p->duracion ?>
                        <?php else :?>
                        Duration: <?= $p->duracion ?>
                        <?php endif;?><br>
                    </span>
                    <span> <?php if($ses==='spanish') :?>

                        Precio: <?= $p->precio ?>
                        <?php else :?>
                        Price: <?= $p->precio ?>
                        <?php endif;?><br>
                    </span>
                </p>
                <div class="conten">
                    <input id="collapsible<?= $key?>" class="toggle" type="checkbox">
                    <div class="collapsible-content">
                        <div class="content-inner">
                            <p class="resCurso">
                                <?= $p->$contenido ?>
                            </p>
                        </div>
                    </div>
                    <?php if($ses==='spanish') :?>
                    <label for="collapsible<?= $key?>" class="lbl-toggle"> </label>
                    <?php else :?>
                    <label for="collapsible<?= $key?>" class="lbl-toggle ingles"> </label>

                    <?php endif;?><br>
                </div>
                <p class="inscribirme">
                    <a href="#" class="linkCursos" data-bs-toggle="modal"
                        onclick="openModal('<?=  $p->$titulo ?>')"><?=$this->lang->line("cursos_talleres")["txt_4"];?></a>
                </p>



            </div>
        </div>

        <?php endforeach; ?>


        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            <?=$this->lang->line("cursos_talleres")["txt_5"];?></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form class="enviar_correoz" action="enviar.php" method="post">
                            <div class="row ">
                                <div class="col">
                                    <input type="hidden" placeholder="curso" class="nombre_curso" name="curso"
                                        value="Formulario general">
                                    <input type="text" required class="form-control camposForm" id="Nombre"
                                        name="nombre" value=""
                                        placeholder="<?=$this->lang->line("cursos_talleres")["txt_6"];?>">
                                    <input type="tel" required class="form-control camposForm" id="Telefono"
                                        name="telefono" value=""
                                        placeholder="<?=$this->lang->line("cursos_talleres")["txt_7"];?>"
                                        pattern="[0-9]{10}">
                                    <input type="email" required class="form-control camposForm" id="Email"
                                        name="correo" value=""
                                        placeholder="<?=$this->lang->line("cursos_talleres")["txt_8"];?>">
                                    <input type="text" style="height: 98px;" required class="form-control camposForm"
                                        id="Mensaje" name="mensaje" value=""
                                        placeholder="<?=$this->lang->line("cursos_talleres")["txt_9"];?>">
                                    <!--<button type="submit" class=" btnEnviar"><?=$this->lang->line("cursos_talleres")["txt_8"];?></button>  -->
                                    <input type="submit" id=""
                                        value="<?=$this->lang->line("cursos_talleres")["txt_10"];?>"
                                        class="boton_contacto btn-enviar2 btnEnviar">
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>



    </div>

</div>


<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>



<script>
function openModal(titulo) {
    console.log(titulo);
    $(".nombre_curso").prop('value', titulo);
    $('#exampleModal').modal('show');
}


$(".enviar_correoz").submit(function(e) {

    //prevent Default functionality
    e.preventDefault();
    var actionurl = 'https://konscio.mx/ws/enviar_correo.php';
    $(".btn-enviar2").prop('value', ' ...');
    $(":submit").attr("disabled", true);

    console.log('valor de correo: ', $('#Email').val());

    $.ajax({
        url: actionurl,
        type: 'post',
        data: $(".enviar_correoz").serialize(),
        success: function(data) {
            console.log('data correo: ', data);
            // if(data == 'Mensaje enviado'){
            if (data == 'Mensaje enviado') {
                //console.log('Mensaje enviado correctamente')
                //console.log('<?=$this->lang->line("contacto")["txt_12"];?>')
                Swal.fire(
                    '<?=$this->lang->line("contacto")["txt_13"];?>',
                    '<?=$this->lang->line("contacto")["txt_14"];?>',
                    'success'
                );
            } else {
                console.log('ERORR');
                Swal.fire(
                    '<?=$this->lang->line("contacto")["txt_15"];?>',
                    '<?=$this->lang->line("contacto")["txt_16"];?>',
                    'error'
                );

            }
            $(".btn-enviar2").prop('value', '<?=$this->lang->line("contacto")["txt_8"];?>');
            $(":submit").removeAttr("disabled");
            $('.enviar_correoz').trigger("reset");

        }
    });

});
</script>
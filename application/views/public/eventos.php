<!-- EVENTOS -->
<div class="container-fluid justify-content-center HomeCursos">

    <div class="row align-items-between justify-content-center">
        <div class="col-lg-12 col-md-12 ">
            <div class="espacioEven">
                <h1 class="h1Eventos">
                    <?=$this->lang->line("eventos")["txt_1"];?>
                </h1>
            </div>
            <p class="HomeTextCursos">
                <!-- <?=$this->lang->line("eventos")["txt_2"];?> <br><?=$this->lang->line("eventos")["txt_3"];?>  -->
            </p>
        </div>
    </div>

    <div class="row align-items-between justify-content-center rowEventos">


        <?php foreach ($posts as $key => $p) : $titulo = 'titulo_'.$ses; $contenido = 'contenido_'.$ses;?>
        <div class="col-lg-4 col-md-4 colCursos">
            <div class="rectanguloEventos">
                <img class="imgEvento"
                    src="<?php echo $p->cover_image !=""? base_url()."/uploads/cover_post/".$p->cover_image :'' ?>">
                <p class="nomEvento">
                    <?=  $p->$titulo ?>
                </p>
                <p class="calEvento">
                    <?php if($ses==='spanish') :?>
                    <?= $p->fecha_spanish ?>
                    <?php else :?>
                    <?= $p->fecha_english ?>
                    <?php endif;?><br>
                </p>
                <div class="resEvento">
                    <?= $p->$contenido ?>
                </div>
            </div>
        </div>
        <?php endforeach; ?>




    </div>




</div>
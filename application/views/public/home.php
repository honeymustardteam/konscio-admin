 <!-- PORTADA. -->

 <!--<div class ="container-fluid">
  <div class ="row align-items-between" >    
    <div class ="videocontainer " style="background:#000">
      <div id="videoSection" class ="videoWrapper">
              <iframe src="<?=$this->lang->line("home")["video1"];?>" frameborder="0" 
              allow ="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
  </div>
</div>-->


 <div class="container-fluid justify-content-center videosPag">
     <div class="row align-items-between">
         <div class="videocontainer ">
             <div id="videoSection" class="videoWrapper">
                 <iframe src="<?=$this->lang->line("home")["video1"];?>" width="640" height="263" frameborder="0"
                     allow="autoplay;  picture-in-picture" allowfullscreen></iframe>
             </div>
         </div>
     </div>
 </div>


 <!-- ¿Qué es el Método Konscio? -->
 <a name="MétodoKonscio"></a>
 <div class="container-fluid justify-content-center">
     <div class="row align-items-between justify-content-center ">
         <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 ">
             <div class="encabezadoHome">
                 <h1>
                     <?=$this->lang->line("home")["txt_3"];?>
                 </h1>
             </div>
             
             <p class="MetodoKonscioText">
                 <?=$this->lang->line("home")["txt_4"];?>
                 <b><?=$this->lang->line("home")["txt_5"];?></b>
                 <?=$this->lang->line("home")["txt_6"];?>
                 <span style="font-style:italic;"><?=$this->lang->line("home")["txt_7"];?></span>
                 <?=$this->lang->line("home")["txt_8"];?>
                 <b><?=$this->lang->line("home")["txt_9"];?></b>
                 <?=$this->lang->line("home")["txt_10"];?>
                 <br> <br>
                 <?=$this->lang->line("home")["txt_11"];?>
             </p>
         </div>
     </div>
 </div>


 <!-- 4 Niveles -->
 <a name="Niveles"></a>
 <div class="container-fluid justify-content-center Niveles">
     <div class="row align-items-between justify-content-center ">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
             <div class="encabezadoAutora">
                 <h1 style="color:white">
                     <?=$this->lang->line("home")["txt_12"];?>
                 </h1>
             </div>
         </div>
     </div>

     <div class="row align-items-between justify-content-center rowBracket">
         <div class="col-lg-3 col-md-12 colBracket">
             <div class="bracket">
                 <span class="left"></span>
                 <span class="right"></span>
                 <p class="num"> 1 </p>
                 <p class="NivKonscio"> <?=$this->lang->line("home")["txt_5"];?> </p>
                 <p class="nivelNom"> <?=$this->lang->line("home")["txt_13"];?></p>
             </div>
         </div>
         <div class="col-lg-3 col-md-12 colBracket ">
             <div class="bracket">
                 <span class="left"></span>
                 <span class="right"></span>
                 <p class="num"> 2 </p>
                 <p class="NivKonscio"> <?=$this->lang->line("home")["txt_5"];?> </p>
                 <p class="nivelNom"> <?=$this->lang->line("home")["txt_14"];?></p>
             </div>
         </div>
         <div class="col-lg-3 col-md-12 colBracket">
             <div class="bracket">
                 <span class="left"></span>
                 <span class="right"></span>
                 <p class="num"> 3 </p>
                 <p class="NivKonscio"> <?=$this->lang->line("home")["txt_5"];?> </p>
                 <p class="nivelNom"> <?=$this->lang->line("home")["txt_15"];?></p>
             </div>
         </div>
         <div class="col-lg-3 col-md-12 colBracket">
             <div class="bracket">
                 <span class="left"></span>
                 <span class="right"></span>
                 <p class="num"> 4 </p>
                 <p class="NivKonscio"> <?=$this->lang->line("home")["txt_5"];?> </p>
                 <p class="nivelNom"> <?=$this->lang->line("home")["txt_16"];?></p>
             </div>
         </div>
         <a class="conocermas" href="konscio">
             <?=$this->lang->line("home")["txt_17"];?>
         </a>
     </div>
 </div>


 <!-- CURSOS Y TALLERES -->
 <div class="container-fluid justify-content-center HomeCursos">

     <div class="row align-items-between justify-content-center">
         <div class="col-lg-12 col-md-12 ">
             <div class="encabezadoHCursos">
                 <h1 style="color:#000">
                     <?=$this->lang->line("home")["txt_18"];?>
                 </h1>
             </div>
             <p class="HomeTextCursos">
                 <!--  <?=$this->lang->line("home")["txt_19"];?> -->
             </p>
         </div>
     </div>

     <div class="row align-items-between justify-content-center rowCursos">
         <?php foreach ($cursos as $key => $p) : $titulo = 'titulo_'.$ses; $contenido = 'contenido_'.$ses;?>
         <div class="col-lg-4 col-md-10 colCursos">
             <div class="rectanguloCursos">
                 <img class="imgCursos"
                     src="<?php echo $p->cover_image !=""? base_url()."/uploads/cover_post/".$p->cover_image :'' ?>">
                 <p class="nomCurso">
                     <?=  $p->$titulo ?>
                 </p>
                 <p class="calCurso">
                     <img class="IconoCal" src="./assets/img/Home/calendario.png">
                     <?php if($ses==='spanish') :?>
                     <?= $p->fecha_spanish ?>
                     <?php else :?>
                     <?= $p->fecha_english ?>
                     <?php endif;?><br>
                 </p>
                 <p class="calCurso duracion">
                     <span>
                         <?php if($ses==='spanish') :?>

                         Duración: <?= $p->duracion ?>
                         <?php else :?>
                         Duration: <?= $p->duracion ?>
                         <?php endif;?><br>
                     </span>
                     <span> <?php if($ses==='spanish') :?>

                         Precio: <?= $p->precio ?>
                         <?php else :?>
                         Price: <?= $p->precio ?>
                         <?php endif;?><br>
                     </span>
                 </p>
                 <div class="conten">
                     <input id="collapsible<?= $key?>" class="toggle" type="checkbox">
                     <div class="collapsible-content">
                         <div class="content-inner">
                             <p class="resCurso">
                                 <?= $p->$contenido ?>
                             </p>
                         </div>
                     </div>
                     <?php if($ses==='spanish') :?>
                     <label for="collapsible<?= $key?>" class="lbl-toggle"> </label>
                     <?php else :?>
                     <label for="collapsible<?= $key?>" class="lbl-toggle ingles"> </label>

                     <?php endif;?><br>
                 </div>

                 <p class="inscribirme">
                     <a href="#" class="linkCursos" data-bs-toggle="modal"
                         onclick="openModal('<?=  $p->$titulo ?>')"><?=$this->lang->line("cursos_talleres")["txt_4"];?></a>
                 </p>






             </div>
         </div>
         <?php endforeach; ?>


     </div>


     <!-- Modal -->
     <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog">
             <div class="modal-content">
                 <div class="modal-header">
                     <h5 class="modal-title" id="exampleModalLabel"><?=$this->lang->line("cursos_talleres")["txt_5"];?>
                     </h5>
                     <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                 </div>
                 <div class="modal-body">
                     <form class="enviar_correoz" action="enviar.php" method="post">
                         <div class="row ">
                             <div class="col">
                                 <input type="hidden" placeholder="curso" class="nombre_curso" name="curso"
                                     value="Formulario general">
                                 <input type="text" required class="form-control camposForm" id="Nombre" name="nombre"
                                     value="" placeholder="<?=$this->lang->line("cursos_talleres")["txt_6"];?>">
                                 <input type="number" required class="form-control camposForm" id="Telefono"
                                     name="telefono" value=""
                                     placeholder="<?=$this->lang->line("cursos_talleres")["txt_7"];?>"
                                     pattern="[0-9]{10}">
                                 <input type="email" required class="form-control camposForm" id="Email" name="correo"
                                     value="" placeholder="<?=$this->lang->line("cursos_talleres")["txt_8"];?>">
                                 <input type="text" style="height: 98px;" required class="form-control camposForm"
                                     id="Mensaje" name="mensaje" value=""
                                     placeholder="<?=$this->lang->line("cursos_talleres")["txt_9"];?>">
                                 <!-- <button type="submit" class=" btnEnviar"><?=$this->lang->line("cursos_talleres")["txt_10"];?></button>  -->
                                 <input type="submit" id="" value="<?=$this->lang->line("cursos_talleres")["txt_10"];?>"
                                     class="boton_contacto btn-enviar2 btnEnviar">
                             </div>
                         </div>
                     </form>
                 </div>

             </div>
         </div>
     </div>



 </div>


 <!-- EVENTOS -->
 <div class="container-fluid justify-content-center HomeEventos">

     <div class="row align-items-between justify-content-center">
         <div class="col-lg-12 col-md-12 ">
             <div class="encabezadoHCursos">
                 <h1 style="color:#000">
                     <?=$this->lang->line("home")["txt_21"];?>
                 </h1>
             </div>
             <p class="HomeTextCursos">
                 <!--  <?=$this->lang->line("home")["txt_22"];?>  -->
             </p>
         </div>
     </div>

     <div class="row align-items-between justify-content-center rowEventos">

         <?php foreach ($eventos as $key => $p) : $titulo = 'titulo_'.$ses; $contenido = 'contenido_'.$ses;?>
         <div class="col-lg-4 col-md-10 colCursos">
             <div class="rectanguloEventos">
                 <img class="imgEvento"
                     src="<?php echo $p->cover_image !=""? base_url()."/uploads/cover_post/".$p->cover_image :'' ?>">
                 <p class="nomEvento">
                     <?=  $p->$titulo ?>
                 </p>
                 <p class="calEvento">
                     <?php if($ses==='spanish') :?>
                     <?= $p->fecha_spanish ?>
                     <?php else :?>
                     <?= $p->fecha_english ?>
                     <?php endif;?><br>
                 </p>
                 <div class="resEvento">
                     <?=  $p->$contenido ?>
                 </div>
             </div>
         </div>
         <?php endforeach; ?>

     </div>

 </div>

 <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

 <script src="./assets/js/headerVideo.js"></script>

 <script>
function openModal(titulo) {
    console.log(titulo);
    $(".nombre_curso").prop('value', titulo);
    $('#exampleModal').modal('show');
}


$(".enviar_correoz").submit(function(e) {

    //prevent Default functionality
    e.preventDefault();
    var actionurl = 'https://konscio.mx/ws/enviar_correo.php';
    $(".btn-enviar2").prop('value', ' ...');
    $(":submit").attr("disabled", true);

    console.log('valor de correo: ', $('#Email').val());

    $.ajax({
        url: actionurl,
        type: 'post',
        data: $(".enviar_correoz").serialize(),
        success: function(data) {
            console.log('data correo: ', data);
            if (data == 'Mensaje enviado') {
                console.log('Mensaje enviado correctamente')
                Swal.fire(
                    'Enviado',
                    'Gracias por contactarnos, nos pondremos en contacto contigo.',
                    'success'
                );
            } else {
                console.log('ERORR');
                Swal.fire(
                    'Error',
                    'Intenta actualizar la página.',
                    'error'
                );
            }
            $(".btn-enviar2").prop('value', '<?=$this->lang->line("contacto")["txt_8"];?>');
            $(":submit").removeAttr("disabled");
            $('.enviar_correoz').trigger("reset");

        }
    });

});
 </script>
<!-- PORTADA. -->
<div class="col-lg-12 col-md-12 portadaAK">
    <p class="textoPortadaAK"><?=$this->lang->line("konscio")["txt_1"];?></p>
    <!--<ul class="list-group list-group-item listportada ">
              <li class="list-group-item">¿Qué es el método Konscio?</li>
              <li class="list-group-item">Los 4 niveles de Konscio  </li>
              <li class="list-group-item">¿Cómo alcanzarlos?</li>
              <li class="list-group-item">¿Por qué alcanzarlos?</li>
              <li class="list-group-item">Resultados</li>
            </ul>  -->

    <ul class="list-group list-group-item listportada ">
        <li class=""><a class="pasos menudeux" href="#MetodoKonscio2"><?=$this->lang->line("konscio")["txt_2"];?></a>
        </li>
        <li class=""><a class="pasos menudeux" href="#NivelesMK"><?=$this->lang->line("konscio")["txt_3"];?></a></li>
        <li class=""><a class="pasos menudeux" href="#comollegar"><?=$this->lang->line("konscio")["txt_4"];?></a></li>
        <li class=""><a class="pasos menudeux" href="#paraque"><?=$this->lang->line("konscio")["txt_5"];?></a></li>
        <li class=""><a class="pasos menudeux" href="#resultados"><?=$this->lang->line("konscio")["txt_6"];?> </a></li>
        <!--<span><?=$this->lang->line("konscio")["txt_7"];?></span> <?=$this->lang->line("konscio")["txt_8"];?>-->
    </ul>
</div>

<!-- ¿Qué es el Método Konscio? -->
<!-- <a name="MétodoKonscio"></a>-->
<div class="container-fluid justify-content-center" id="MetodoKonscio2">
    <div class="row align-items-between justify-content-center descripcionK">
        <div class="col-lg-8 col-md-12 ">
            <div class="encabezado">
                <h1 class="h1AcercaDeKon">
                    <?=$this->lang->line("konscio")["txt_7"];?>
                </h1>
            </div>
            <p class="textQueesMK">
                <?=$this->lang->line("konscio")["txt_8"];?>
                <b><?=$this->lang->line("konscio")["txt_9"];?></b>
                <?=$this->lang->line("konscio")["txt_10"];?>
                <span style="font-style:italic;"><?=$this->lang->line("konscio")["txt_11"];?> </span>
                <?=$this->lang->line("konscio")["txt_12"];?><br><br>
                <?=$this->lang->line("konscio")["txt_13"];?>
                <b><?=$this->lang->line("konscio")["txt_14"];?></b>
                <?=$this->lang->line("konscio")["txt_15"];?>
            </p>
        </div>
    </div>
</div>


<!-- 4 Niveles -->
<a name="Niveles"></a>
<div class="container-fluid justify-content-center NivelesMK" id="NivelesMK">
    <div class="row align-items-between justify-content-center ">
        <div class="col-lg-12 col-md-12 ">
            <div class="encabezadoMKN">
                <h1 style="color:white">
                    <?=$this->lang->line("konscio")["txt_16"];?>
                </h1>
            </div>
        </div>
    </div>

    <div class="row align-items-between justify-content-center ">
        <div class="col-lg-7 col-md-12 ">
            <p class="textQueesMK" style="color:white">
                <?=$this->lang->line("konscio")["txt_17"];?>
            </p>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 colBracketMK">
        <div class="row align-items-between justify-content-center bracketMK1">
            <span class="left"></span>
            <span class="right"></span>
            <div class="col-lg-3 col-md-12 ">
                <p class="numMK"> 1 </p>
                <p class="NivKonscioMK"> <?=$this->lang->line("konscio")["txt_9"];?> </p>
                <p class="nivelNomMK"> <?=$this->lang->line("konscio")["txt_18"];?></p>
                <p class="NivKonscioMK" Style="margin-top:2%"> <?=$this->lang->line("konscio")["txt_53"];?></p>
            </div>
            <div class="col-lg-9 col-md-12 ">
                <p class="textQueesMK1" style="color:white">
                    <?=$this->lang->line("konscio")["txt_19"];?> <b><?=$this->lang->line("konscio")["txt_20"];?></b>
                    <?=$this->lang->line("konscio")["txt_21"];?> </p>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 colBracketMK">
        <div class="row align-items-between justify-content-center bracketMK2">
            <span class="left"></span>
            <span class="right"></span>
            <div class="col-lg-9 col-md-12 ordertexto">
                <p class="textQueesMK2" style="color:white">
                    <?=$this->lang->line("konscio")["txt_22"];?> <b><?=$this->lang->line("konscio")["txt_23"];?></b>
                    <?=$this->lang->line("konscio")["txt_24"];?> <br><br>
                    <?=$this->lang->line("konscio")["txt_25"];?>
                </p>
            </div>
            <div class="col-lg-3 col-md-12 ordernum">
                <p class="numMK"> 2 </p>
                <p class="NivKonscioMK"> <?=$this->lang->line("konscio")["txt_9"];?> </p>
                <p class="nivelNomMK"> <?=$this->lang->line("konscio")["txt_26"];?></p>
                <p class="NivKonscioMK" Style="margin-top:2%"> <?=$this->lang->line("konscio")["txt_53"];?></p>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 colBracketMK">
        <div class="row align-items-between justify-content-center bracketMK1">
            <span class="left"></span>
            <span class="right"></span>
            <div class="col-lg-3 col-md-12 ">
                <p class="numMK"> 3 </p>
                <p class="NivKonscioMK"> <?=$this->lang->line("konscio")["txt_9"];?> </p>
                <p class="nivelNomMK"> <?=$this->lang->line("konscio")["txt_27"];?></p>
                <p class="NivKonscioMK" Style="margin-top:2%"> <?=$this->lang->line("konscio")["txt_53"];?></p>
            </div>
            <div class="col-lg-9 col-md-12 ">
                <p class="textQueesMK1" style="color:white">
                    <?=$this->lang->line("konscio")["txt_28"];?> <b><?=$this->lang->line("konscio")["txt_29"];?>
                    </b><?=$this->lang->line("konscio")["txt_30"];?> </p>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 colBracketMK">
        <div class="row align-items-between justify-content-center bracketMK2">
            <span class="left"></span>
            <span class="right"></span>
            <div class="col-lg-9 col-md-12 ordertexto">
                <p class="textQueesMK2" style="color:white">
                    <?=$this->lang->line("konscio")["txt_31"];?> <b><?=$this->lang->line("konscio")["txt_32"];?></b>
                    <?=$this->lang->line("konscio")["txt_33"];?>
                </p>
            </div>
            <div class="col-lg-3 col-md-12 ordernum">
                <p class="numMK"> 4 </p>
                <p class="NivKonscioMK"> <?=$this->lang->line("konscio")["txt_9"];?> </p>
                <p class="nivelNomMK"> <?=$this->lang->line("konscio")["txt_34"];?></p>
                <p class="NivKonscioMK" Style="margin-top:2%"> <?=$this->lang->line("konscio")["txt_53"];?></p>
            </div>
        </div>
    </div>

</div>



<!-- ¿Cómo llegar a ellos? -->
<div class="container-fluid justify-content-center" id="comollegar">
    <div class="row align-items-between justify-content-center sec4">
        <div class="col-lg-5 col-md-10 ">
        </div>
        <div class="col-lg-7 col-md-10" style="padding: 0;">
            <div class="encabezadoMK">
                <h1 class="h1left">
                    <?=$this->lang->line("konscio")["txt_35"];?>
                </h1>

                <img class="imgSec4" src="./assets/img/AcercaDe/buda.png">
            </div>
            <p class="textLeft">
                <b><?=$this->lang->line("konscio")["txt_36"];?> </b>
                <?=$this->lang->line("konscio")["txt_37"];?><br><br>
                <b><?=$this->lang->line("konscio")["txt_38"];?> </b>
                <?=$this->lang->line("konscio")["txt_39"];?><br><br>
                <b><?=$this->lang->line("konscio")["txt_40"];?> </b>
                <?=$this->lang->line("konscio")["txt_41"];?><br><br>
                <b><?=$this->lang->line("konscio")["txt_42"];?> </b> <?=$this->lang->line("konscio")["txt_43"];?>
            </p>
        </div>
    </div>
</div>

<!-- ¿Para qué llegar a ellos? -->
<div class="container-fluid justify-content-center" id="paraque">
    <div class="row align-items-between justify-content-center sec5">

        <div class="col-lg-6 col-md-10 ">
            <div class="encabezadoMK">
                <h1 class="h1left">
                    <?=$this->lang->line("konscio")["txt_44"];?>
                </h1>
                <img class="imgSec5" src="./assets/img/AcercaDe/flor-solo.png">

            </div>
            <p class="textLeft">
                <?=$this->lang->line("konscio")["txt_45"];?><br><br>
                <?=$this->lang->line("konscio")["txt_46"];?>
            </p>
        </div>
        <div class="col-lg-6 col-md-10 ">
        </div>
    </div>
</div>

<!-- Resultados -->
<div class="container-fluid justify-content-center " id="resultados">
    <div class="row align-items-between justify-content-center ">
        <div class="col-lg-5 col-md-10 ordertexto">
            <img class="imgSec6" src="./assets/img/AcercaDe/img-resultados.png">

        </div>
        <div class="col-lg-7 col-md-10 ordernum">
            <div class="encabezadoMK">
                <h1 class="h1left">
                    <?=$this->lang->line("konscio")["txt_47"];?>
                </h1>
            </div>
            <p class="textLeft">
                <?=$this->lang->line("konscio")["txt_48"];?><br><br>
                <?=$this->lang->line("konscio")["txt_49"];?><br><br>
                <?=$this->lang->line("konscio")["txt_50"];?><br><br>
                <?=$this->lang->line("konscio")["txt_51"];?><br><br>
                <?=$this->lang->line("konscio")["txt_52"];?>
            </p>
        </div>
    </div>
</div>


<script>
jQuery(document).ready(function() {
    jQuery('.menudeux').on('click', function() { // Au clic sur un élément
        var page = jQuery(this).attr('href'); // Page cible
        var speed = 150; // Durée de l'animation (en ms)
        var offset = 100;
        console.log(jQuery(page));
        jQuery('html, body').animate({
            scrollTop: jQuery(page).offset().top - offset
        }, speed); // Go
        return false;
    });
})
</script>
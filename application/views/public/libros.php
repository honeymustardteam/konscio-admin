<div class="container-fluid justify-content-center HomeCursos">
          <!-- LIBROS titulo y texto -->  
            <div class="row align-items-between justify-content-center">    
              <div class="col-lg-12 col-md-12 ">  
              <div class="espacioLib">
                <h1 class="h1Libros">
                <?=$this->lang->line("libros")["txt_1"];?>
                </h1>
              </div>   
              <p class="HomeTextCursos">
              <?=$this->lang->line("libros")["txt_2"];?>
              </p>      
              </div>
              </div>

              <!-- LIBROS img principal-->  
              <div class="row align-items-between justify-content-center">    
                <div class="col-lg-7 col-md-12 imgCenter">                  
                <img class="imgInicio" src="./assets/img/Libros/libros2.png">                     
                </div>
              </div>

              <br>
              <div class="row align-items-between justify-content-center">    
                <div class="col-lg-7 col-md-12 imgCenter botonRegalo" style="cursor:pointer" onclick="openModal('archivo')">                  
                <?=$this->lang->line("libros")["txt_18"];?>                
                </div>
              </div>
  
          <div class="row align-items-between justify-content-center rowLibros" >                
            <div class="col-lg-4 col-md-12 colLibros"> 
              <div class="libro">
              <img class="imgLibros" src="./assets/img/Libros/libro-konscio.png">                
              </div>              
              <div><a class="comprar" href="https://www.amazon.com/Milly-Diericx-ebook/dp/B09R6FGVG5/ref=mp_s_a_1_1?crid=23C0U2S05E8U2&keywords=the+konscio+method&qid=1643236309&sprefix=the+konscio+method%2Caps%2C193&sr=8-1" target="_blank"><img src="./assets/img/Libros/Amazonlogo.png"></a>  </div>
              <div class="infoLibro">
              <p class="nomLibro">
               <img class="iconoLibro" src="./assets/img/LogosIconos/icono-libro.png" >
              <span class="nomLibro_titulo"> <?=$this->lang->line("libros")["txt_3"];?><br></span>
              </p>

              <div class="description-section">
              <p class="resLibro">
              <?=$this->lang->line("libros")["txt_4"];?></p>
              </div>
              <div class="description-section all">               
              <p class="resLibro">
                <span><?=$this->lang->line("libros")["txt_5"];?></span>
                <span style="font-style:italic;"><?=$this->lang->line("libros")["txt_6"];?></span>
                <span><?=$this->lang->line("libros")["txt_7"];?></span>
                <span style="font-style:italic;"><?=$this->lang->line("libros")["txt_8"];?></span>
                <span><?=$this->lang->line("libros")["txt_9"];?></span> 
                <span style="font-style:italic;"><?=$this->lang->line("libros")["txt_10"];?></span>   
                <span><?=$this->lang->line("libros")["txt_11"];?></span>                
              </p>
              </div>

              <button class="read-more_btn">Leer mas &#x2B;</button>
              <button class="read-less_btn d_none">Leer menos &#x2D;</button>

               </div>
            </div>
           
            <div class="col-lg-4 col-md-12 colLibros"> 
              <div class="libro">
              <img class="imgLibros" src="./assets/img/Libros/libro-wolf.png">                
              </div>
              <div><a class="comprar" href="https://www.amazon.com/Befriending-Wolf-Guide-Living-Thriving/dp/1630478741/ref=sr_1_1?ie=UTF8&qid=1461281537&sr=8-1&keywords=befriending+the+wolf" target="_blank"><img src="./assets/img/Libros/Amazonlogo.png"></a>  </div>
              <div class="infoLibro">
              <p class="nomLibro">
               <img class="iconoLibro" src="./assets/img/LogosIconos/icono-libro.png" >
              <span class="nomLibro_titulo"> <?=$this->lang->line("libros")["txt_12"];?><br></span>
              </p>
              <div class="description-section">
              <p class="resLibro">
              <?=$this->lang->line("libros")["txt_13"];?></p>
              </div>
              <div class="description-section all">               
              <p class="resLibro">
              <?=$this->lang->line("libros")["txt_14"];?></p>
               </div>
               
              <button class="read-more_btn">Leer mas &#x2B;</button>
              <button class="read-less_btn d_none">Leer menos &#x2D;</button>

            </div>
            </div>
            
            <div class="col-lg-4 col-md-12 colLibros"> 
              <div class="libro">
              <img class="imgLibros" src="./assets/img/Libros/libro-be_love.png">                             
              </div>              
              <div><a class="comprar" href="https://www.amazon.com/Be-Love-Conscious-Birthing-Future-ebook/dp/B08HKB46Z8/ref=mp_s_a_1_1?crid=FAEP4P855W5V&keywords=Be+Love%3A+A+Conscious+Shift+to+Birthing+the+Future&qid=1640619090&sprefix=be+love+a+conscious+shift+to+birthing+the+future%2Caps%2C104&sr=8-1" target="_blank"><img src="./assets/img/Libros/Amazonlogo.png"></a>  </div>
              <div class="infoLibro">
              <p class="nomLibro">
               <img class="iconoLibro" src="./assets/img/LogosIconos/icono-libro.png" >
              <span class="nomLibro_titulo"> <?=$this->lang->line("libros")["txt_15"];?><br></span>
              </p><div class="description-section">
              <p class="resLibro">
              <?=$this->lang->line("libros")["txt_16"];?></p>
              </div>
              <div class="description-section all">               
              <p class="resLibro">
              <?=$this->lang->line("libros")["txt_17"];?></p>
               </div>               
              <button class="read-more_btn">Leer mas &#x2B;</button>
              <button class="read-less_btn d_none">Leer menos &#x2D;</button>
              </div>
               
               
            </div>
            
          </div>






          <!-- Modal -->
          <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><?=$this->lang->line("libros")["txt_19"];?> </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                  <form class="enviar_correoz_regalo" name="uploader" action="enviar.php" method="post" enctype="multipart/form-data">
                        <div class="row ">
                        <div class="col">
                            <input type="hidden" placeholder="formulario" class="formulario" name="tipo_formulario" value="libros">
                            <input type="text" required class="form-control camposForm" id="Nombre" name="nombre" value="" placeholder="<?=$this->lang->line("cursos_talleres")["txt_6"];?>">
                            <input  type="tel" required class="form-control camposForm" id="Telefono" name="telefono" value="" placeholder="<?=$this->lang->line("cursos_talleres")["txt_7"];?>" pattern="[0-9]{10}" >
                            <input type="email" required class="form-control camposForm" id="Email" name="correo" value="" placeholder="<?=$this->lang->line("cursos_talleres")["txt_8"];?>">
                            
                            <p><?=$this->lang->line("libros")["txt_20"];?> </p>
                            <input  type="file" required class="form-control " id="file" name="file" value="" >
                            <br><br>
                            <input type="submit" id="" value="<?=$this->lang->line("cursos_talleres")["txt_10"];?>" class="boton_contacto btn-enviar2 btnEnviar">
                        </div>
                        </div>
                    </form>
                  </div>
                  
                </div>
              </div>
            </div>




  
        </div> 


        <script>

      function openModal(titulo){
        console.log(titulo);  
        $(".nombre_curso").prop('value', titulo);
        $('#exampleModal').modal('show'); 
      }


      $("form[name='uploader']").on("submit", function(ev) {
    
          //prevent Default functionality
          ev.preventDefault();
          var actionurl = 'https://konscio.mx/ws/enviar_correo.php';
          $(".btn-enviar2").prop('value', ' ...');
          $(":submit").attr("disabled", true);
          
          console.log('valor de correo: ', $('#Email').val());

          var formData = new FormData(this);


          $.ajax({
              url: actionurl,
              type: 'POST',
              data: formData,
              processData: false,
              contentType: false,
              success: function(data) {
                  console.log('data correo: ', data);
                  // if(data == 'Mensaje enviado'){
                    if(data == 'Mensaje enviado'){    
                      //console.log('Mensaje enviado correctamente')
                      //console.log('<?=$this->lang->line("contacto")["txt_12"];?>')
                      Swal.fire(
                        '<?=$this->lang->line("contacto")["txt_13"];?>',
                        '<?=$this->lang->line("contacto")["txt_14"];?>',
                        'success'
                      );
                  }else{
                      console.log('ERORR');
                      Swal.fire(
                        '<?=$this->lang->line("contacto")["txt_15"];?>',
                        '<?=$this->lang->line("contacto")["txt_16"];?>',
                        'error'
                      );
              
                  }
                  $(".btn-enviar2").prop('value', '<?=$this->lang->line("contacto")["txt_8"];?>');
                  $(":submit").removeAttr("disabled");
                  $('.enviar_correoz_regalo').trigger("reset");

              }
          });

});


  jQuery('.read-more_btn').on('click', function(){
    jQuery(this).addClass('d_none');
    jQuery(this).parent().find('.read-less_btn').removeClass('d_none')
   	jQuery(this).parent().find('.description-section.all').addClass('reading');
  });
  jQuery('.read-less_btn').on('click', function(){
    jQuery(this).addClass('d_none');
    jQuery(this).parent().find('.read-more_btn').removeClass('d_none')
   	jQuery(this).parent().find('.description-section.all').removeClass('reading');
  });
</script>
<div class="container-fluid justify-content-center PregYREsp">
          <!-- PREGUNTAS FRECUENTES -->  
            <div class="row align-items-between justify-content-center">    
              <div class="col-lg-12 col-md-12 ">  
              <div class="espacioPreg">
                <h1 class="h1Preguntas">
                <?=$this->lang->line("preguntas")["txt_1"];?>
                </h1>
              </div>   
    
              </div>
              </div>

              <div class="row align-items-between justify-content-center preguntas">    
                <div class="col-lg-8 col-md-12 ">    
                  
                  <div class="accordion" id="acordionPreguntas">
                    <div class="accordion-item">
                      <h2 class="accordion-header" id="pregunta1">
                        <button class="accordion-button  collapsed preg" type="button" data-bs-toggle="collapse" data-bs-target="#collapsePreg1" aria-expanded="true" aria-controls="collapsePreg1">
                        <?=$this->lang->line("preguntas")["txt_2"];?>
                        </button>
                      </h2>
                      <div id="collapsePreg1" class="accordion-collapse collapse" aria-labelledby="pregunta1" data-bs-parent="#acordionPreguntas">
                        <div class="accordion-body">
                        <?=$this->lang->line("preguntas")["txt_3"];?> <br><?=$this->lang->line("preguntas")["txt_4"];?>  
                        </div>
                      </div>
                    </div>
                    <div class="accordion-item">
                      <h2 class="accordion-header" id="pregunta2">
                        <button class="accordion-button  collapsed preg" type="button" data-bs-toggle="collapse" data-bs-target="#collapsePreg2" aria-expanded="false" aria-controls="collapsePreg2">
                        <?=$this->lang->line("preguntas")["txt_5"];?>
                        </button>
                      </h2>
                      <div id="collapsePreg2" class="accordion-collapse collapse" aria-labelledby="pregunta2" data-bs-parent="#acordionPreguntas">
                        <div class="accordion-body">
                        <?=$this->lang->line("preguntas")["txt_6"];?></div>
                      </div>
                    </div>
                    <div class="accordion-item">
                      <h2 class="accordion-header" id="pregunta3">
                        <button class="accordion-button  collapsed preg" type="button" data-bs-toggle="collapse" data-bs-target="#collapsePreg3" aria-expanded="false" aria-controls="collapsePreg3">
                        <?=$this->lang->line("preguntas")["txt_7"];?>
                          </button>
                      </h2>
                      <div id="collapsePreg3" class="accordion-collapse collapse" aria-labelledby="pregunta3" data-bs-parent="#acordionPreguntas">
                        <div class="accordion-body">
                        <?=$this->lang->line("preguntas")["txt_8"];?> <br><?=$this->lang->line("preguntas")["txt_9"];?>  
                        </div>
                      </div>
                    </div>
                    <div class="accordion-item">
                      <h2 class="accordion-header" id="pregunta4">
                        <button class="accordion-button  collapsed preg" type="button" data-bs-toggle="collapse" data-bs-target="#collapsePreg4" aria-expanded="true" aria-controls="collapsePreg4">
                        <?=$this->lang->line("preguntas")["txt_10"];?> </button>
                      </h2>
                      <div id="collapsePreg4" class="accordion-collapse collapse" aria-labelledby="pregunta4" data-bs-parent="#acordionPreguntas">
                        <div class="accordion-body">
                        <?=$this->lang->line("preguntas")["txt_11"];?> <br><?=$this->lang->line("preguntas")["txt_12"];?>  
                        </div>
                      </div>
                    </div>
                    <div class="accordion-item">
                      <h2 class="accordion-header" id="pregunta5">
                        <button class="accordion-button  collapsed preg" type="button" data-bs-toggle="collapse" data-bs-target="#collapsePreg5" aria-expanded="false" aria-controls="collapsePreg5">
                        <?=$this->lang->line("preguntas")["txt_13"];?>
                        </button>
                      </h2>
                      <div id="collapsePreg5" class="accordion-collapse collapse" aria-labelledby="pregunta5" data-bs-parent="#acordionPreguntas">
                        <div class="accordion-body">
                        <?=$this->lang->line("preguntas")["txt_14"];?> <br><?=$this->lang->line("preguntas")["txt_15"];?>  
                        </div>
                    </div>
                    </div>
                    <div class="accordion-item">
                      <h2 class="accordion-header" id="pregunta6">
                        <button class="accordion-button  collapsed preg" type="button" data-bs-toggle="collapse" data-bs-target="#collapsePreg6" aria-expanded="false" aria-controls="collapsePreg6">
                        <?=$this->lang->line("preguntas")["txt_16"];?>
                        </button>
                      </h2>
                      <div id="collapsePreg6" class="accordion-collapse collapse" aria-labelledby="pregunta6" data-bs-parent="#acordionPreguntas">
                        <div class="accordion-body">
                        <?=$this->lang->line("preguntas")["txt_17"];?> <br><?=$this->lang->line("preguntas")["txt_18"];?>  
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  
                </div>
             </div>  


          </div>
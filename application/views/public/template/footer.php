<!-- FOOTER -->
<a name="contacto"></a>
<footer class="footerGeneral" id="contacto">
    <div class="container-fluid">
        <div class="row contactanosForm align-items-between justify-content-center">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">

                <p class="EncabezadoContacto">
                    <?=$this->lang->line("contacto")["txt_1"];?>
                </p>
            </div>
        </div>
        <div class="row contactanosForm align-items-between justify-content-center ">
            <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12 ">
                <p class="infoContacto">
                    <?=$this->lang->line("contacto")["txt_2"];?> <br> <?=$this->lang->line("contacto")["txt_3"];?>
                </p>
                <p class="camposContacto">
                    <a href="mailto:info@konscio.mx"><img src="./assets/img/LogosIconos/icn-mail.png"
                            style="margin-right: 10px;"></a>
                    <a id="link" href="mailto:info@konscio.mx">info@konscio.mx<br></a>
                </p>

                <p class="camposContacto">
                    <a href="tel:+55 15 74 59 61"><img src="./assets/img/LogosIconos/icn-phone.png"
                            style="margin-right: 10px;"></a>
                    <a id="link" href="tel:+55 15 74 59 61">55 15 74 59 61<br></a>
                </p>
                <p class="EncabezadoContacto">
                    <a href="https://www.facebook.com/Dra.MillyDiericx" target="_blank" style="margin-right:2%"><img
                            src="./assets/img/LogosIconos/facebook.png"></a>
                    <a href="https://www.instagram.com/dra.millydiericx/" target="_blank" style="margin-right:2%"><img
                            src="./assets/img/LogosIconos/instagram.png"></a>
                    <a href="https://www.youtube.com/channel/UC7yTaE49njuKlH9obxHmo8w" target="_blank"><img
                            src="./assets/img/LogosIconos/youtube.png"></a>
                </p>
            </div>

            <div class="col-lg-8 col-md-5 col-sm-12 col-xs-12 camposCal ">
                <form class="enviar_correo" action="enviar.php" method="post">
                    <div class="row ">
                        <div class="col">
                            <input type="text" required class="form-control camposForm" id="Nombre" name="nombre"
                                value="" placeholder="<?=$this->lang->line("contacto")["txt_4"];?>">
                            <input type="tel" required class="form-control camposForm" id="Telefono" name="telefono"
                                value="" placeholder="<?=$this->lang->line("contacto")["txt_5"];?>" pattern="[0-9]{10}">
                            <input type="email" required class="form-control camposForm" id="Email" name="correo"
                                value="" placeholder="<?=$this->lang->line("contacto")["txt_6"];?>">

                        </div>
                        <div class="col">
                            <input type="text" style="height: 98px;" required class="form-control camposForm"
                                id="Mensaje" name="mensaje" value=""
                                placeholder="<?=$this->lang->line("contacto")["txt_7"];?>">
                            <!--<button type="submit" class="btn-enviar btnEnviar"></button>-->
                            <input type="submit" id="" value="<?=$this->lang->line("contacto")["txt_8"];?>"
                                class="boton_contacto btn-enviar btnEnviar">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container-fluid text-center  ">
        <div class="row container-fluid containFooter">
            <ul class="list-group list-group-horizontal footerEnd">
                <li class="list-group-item">KONSCIO® 2021 </li>
                <li class="list-group-item">MÉXICO </li>
                <li class="list-group-item">
                    <a href="mailto:info@konscio.mx"></a>
                    <a class="linkfooter" href="mailto:info@konscio.mx">info@konscio.mx <br></a>
                </li>
                <li class="list-group-item"><a class="linkfooter" href="<?=$this->lang->line("contacto")["txt_17"];?>"
                        target="_blank"> <?=$this->lang->line("contacto")["txt_9"];?></a></li>
                <li class="list-group-item"><a class="linkfooter" href="https://cajacreativa.com/"
                        target="_blank"><?=$this->lang->line("contacto")["txt_10"];?></a></li>
            </ul>
        </div>
    </div>
    </div>
</footer>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
$(".enviar_correo").submit(function(e) {

    //prevent Default functionality
    e.preventDefault();
    var actionurl = 'https://konscio.mx/ws/enviar_correo.php';
    $(".btn-enviar").prop('value', ' ...');
    $(":submit").attr("disabled", true);

    $.ajax({
        url: actionurl,
        type: 'post',
        data: $(".enviar_correo").serialize(),
        success: function(data) {
            console.log('data correo: ', data);
            // if(data == 'Mensaje enviado'){
            if (data == 'Mensaje enviado') {
                //console.log('Mensaje enviado correctamente')
                console.log('<?=$this->lang->line("contacto")["txt_12"];?>')
                Swal.fire(
                    '<?=$this->lang->line("contacto")["txt_13"];?>',
                    '<?=$this->lang->line("contacto")["txt_14"];?>',
                    'success'
                );
            } else {
                console.log('ERORR');
                Swal.fire(
                    '<?=$this->lang->line("contacto")["txt_15"];?>',
                    '<?=$this->lang->line("contacto")["txt_16"];?>',
                    'error'
                );
            }
            $(".btn-enviar").prop('value', '<?=$this->lang->line("contacto")["txt_8"];?>');
            $(":submit").removeAttr("disabled");
            $('.enviar_correo').trigger("reset");

        }
    });

});
</script>
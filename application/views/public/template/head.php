<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google" content="notranslate">
    <base href="<?=base_url();?>">
    <title>Konscio by Dr. Milly Diericx</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="<?=base_url()?>/assets/stylesheet/main.css">
    <link rel="stylesheet" href="<?=base_url()?>/assets/stylesheet/<?=$nombre_css?>">
    <?php 
    if(isset($nombre_css_2)){ ?>
        <link rel="stylesheet" href="<?=base_url()?>/assets/stylesheet/<?=$nombre_css_2?>">
    <?php } ?>
    <?php 
    if(isset($nombre_css_3)){ ?>
        <link rel="stylesheet" href="<?=base_url()?>/assets/stylesheet/<?=$nombre_css_3?>">
    <?php } ?>
    <!--<link rel="icon" type="image/png" href="./asset/img/iconos/favicon.png">-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script src="<?=base_url()?>/assets/js/enviar.js"></script>
    <script src="<?=base_url()?>/assets/js/scroll.js"></script>
</head>
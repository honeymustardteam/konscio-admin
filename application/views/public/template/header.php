<header class="headerMenu">
    <nav class="navbar navbar-expand-lg static-top headerMenu1">
        <div class="container-fluid">
            <a class="navbar-brand" href=".">
                <img src="<?=base_url()?>/assets/img/LogosIconos/logo-symbol.png" class="logo">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <!-- <li class="nav-item liMenu">
                        <a class="nav-link" href="."><?=$this->lang->line("menu")["inicio"];?></a>
                    </li> -->

                    <li class="dropdown nav-item liMenu">
                        <a href="acerca" data-toggle="dropdown"
                            class="dropdown-toggle nav-link"><?=$this->lang->line("menu")["acerca_de"];?></a>
                        <ul class="dropdown-menu">
                            <li class="nav-item liMenu">
                                <a class="nav-link"
                                    href="./acerca#terapias"><?=$this->lang->line("menu")["terapias"];?></a>
                            </li>
                            <li class="nav-item liMenu">
                                <a class="nav-link"
                                    href="./acerca#testimonios"><?=$this->lang->line("menu")["testimonios"];?></a>
                            </li>
                            <li class="nav-item liMenu">
                                <a class="nav-link"
                                    href="./acerca#cv"><?=$this->lang->line("menu")["cv"];?></a>
                            </li>
                            <li class="nav-item liMenu">
                                <a class="nav-link"
                                    href="./acerca#libros"><?=$this->lang->line("menu")["libros"];?></a>
                            </li>
                            <li class="nav-item liMenu">
                                <a class="nav-link"
                                    href="./acerca#entrevistas"><?=$this->lang->line("menu")["entrevistas"];?></a>
                            </li>
                            <li class="nav-item liMenu">
                                <a class="nav-link"
                                    href="./acerca#congresos"><?=$this->lang->line("menu")["congresos"];?></a>
                            </li>
                            
                        </ul>
                    </li>



                    <li class="dropdown nav-item liMenu">
                        <a href="konscio" data-toggle="dropdown"
                            class="dropdown-toggle nav-link"><?=$this->lang->line("menu")["que_es"];?></a>
                        <ul class="dropdown-menu">
                            <li class="nav-item liMenu">
                                <a class="nav-link"
                                    href="preguntas_frecuentes"><?=$this->lang->line("menu")["preguntas_freq"];?></a>
                            </li>
                        </ul>
                    </li>


                    <li class="dropdown nav-item liMenu">
                        <a href="cursos_talleres" data-toggle="dropdown"
                            class="dropdown-toggle nav-link"><?=$this->lang->line("menu")["cursos_talleres"];?></a>
                        <ul class="dropdown-menu">
                            <li class="nav-item liMenu">
                                <a class="nav-link"
                                    href="testimonios"><?=$this->lang->line("menu")["testimonios"];?></a>
                            </li>
                        </ul>
                    </li>


                    <!-- <li class="nav-item liMenu">
                        <a class="nav-link" href="libros"><?=$this->lang->line("menu")["libros"];?></a>
                    </li> -->


                    <li class="dropdown nav-item liMenu">
                        <a href="eventos" data-toggle="dropdown"
                            class="dropdown-toggle nav-link"><?=$this->lang->line("menu")["eventos"];?></a>
                        <ul class="dropdown-menu">

                            <li class="nav-item liMenu">
                                <a class="nav-link"
                                    href="videos"><?=$this->lang->line("menu")["videos"];?></a>
                            </li>
                        </ul>
                    </li>


                    <li class="nav-item liMenu">
                        <a class="nav-link" href="blogs"><?=$this->lang->line("menu")["blog"];?></a>
                    </li>

                    <li class="nav-item liMenu">
                        <a class="nav-link" href="#contacto"><?=$this->lang->line("menu")["contacto"];?></a>
                    </li>
                    <li class="nav-item liMenu pais">
                        <a href="LangSwitch/switchLanguage/spanish">
                            <img class="bandera" src="<?=base_url()?>/assets/img/LogosIconos/banderaESP.png"><br>
                            <span class="idioma">ESP<br></span>
                        </a>
                    </li>
                    <li class="nav-item liMenu pais">
                        <a href="LangSwitch/switchLanguage/english">
                            <img class="bandera" src="<?=base_url()?>/assets/img/LogosIconos/banderaENG.png"><br>
                            <span class="idioma">ENG<br></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
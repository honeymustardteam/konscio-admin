<div class="container-fluid justify-content-center TestimoniosBack">
          <!-- PREGUNTAS FRECUENTES -->  
            <div class="row align-items-between justify-content-center">    
              <div class="col-lg-12 col-md-12 ">  
              <div class="espacioTest">
                <h1 class="h1Testimonios">
                <?=$this->lang->line("testimonios")["txt_1"];?>
                </h1>                
              </div>     
              </div>
              </div>


              <div class="testimonios_wrap">
                <?php foreach ($posts as $key => $p) : $titulo = 'titulo_'.$ses; $contenido = 'contenido_'.$ses;?>
                  <div class="row align-items-between  testimonios">    
                      <div class="col-lg-2 col-md-12 testimonioIzq">    
                      <img class="imgTest" src="<?php echo $p->cover_image !=""? base_url()."/uploads/cover_post/".$p->cover_image :'' ?>">
                      </div>
                      <div class="col-lg-10 col-md-12 testimonioDer">    
                      <span class="nomTest"> <?= $p->$titulo ?></span><br>
                      <p class="Comillas">
                      <img class="imgComillas" src="./assets/img/Testimonios/comillas.png" >
                      <span class="contTest"><?=$p->$contenido;?></span>
                      </p>
                      </div>  
                  </div>  
                <?php endforeach; ?>
              </div>
            
          </div>
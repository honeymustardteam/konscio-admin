  function story(){
      $('.cristina-mata .an').addClass('animacion');
    }
    function mision(){
      $('.mision-w .an').addClass('animacion');
    }
    function vision(){
      $('.vision-w .an').addClass('animacion');
    }
    function storyAll(){
      mision();
      vision();
    }
    function ingredients(){
      $('.info-ingredients .an').addClass('animacion');
    }
    function ingredientsDetail(){
      $('.cac .an').addClass('animacion');
    }
    function products(){
      $('.products .an').addClass('animacion');
    }
    function contact(){
      $('.contacto.an').addClass('animacion');
      $('.contacto .an').addClass('animacion');
    }
    function clean(){
      $('.an').removeClass('animacion');
      $('#inicio .fadeIn').removeClass('animacion');
      $('#inicio .fadeIn').removeClass('delay-15s');
      $('#inicio .fadeIn').removeClass('delay-2s');
    }

if ($(window).width() >= 768) {
       // fullpage customization
$('#fullpage').fullpage({
  sectionsColor: ['#33361B', '#FFFFFF', '#33361B', '#FFFFFF', '#FFFFFF'],
  sectionSelector: '.vertical-scrolling',
  slideSelector: '.horizontal-scrolling',
  navigation: true,
  slidesNavigation: true,
  controlArrows: false,
  responsiveWidth: 993,
  anchors: ['Home', 'Story', 'Ingredients', 'Products', 'Contact'],
  //menu: '.menu',

  afterLoad: function(anchorLink, index, destination, direction, slideIndex) {
    if(anchorLink == 'Home'){
      clean();
      $('#inicio .fadeIn').addClass('animacion');
    }
    if(anchorLink == 'Story'){
      clean();
      story();
      mision();
      vision();
      if($('body').hasClass('fp-viewing-Story-0')){
        $('.mision-w .an').removeClass('animacion');
        $('.vision-w .an').removeClass('animacion');
      }
      if($('body').hasClass('fp-viewing-Story-2')){
        $('.mision-w .an').removeClass('animacion');
        $('.cristina-mata .an').removeClass('animacion');
      }
      if($('body').hasClass('fp-viewing-Story-1')){
        $('.vision-w .an').removeClass('animacion');
        $('.cristina-mata .an').removeClass('animacion');
      }
    }
    if(anchorLink == 'Ingredients'){
      clean();
      ingredients();
      ingredientsDetail();
      if($('body').hasClass('fp-viewing-Ingredients-0')){
        $('.cac .an').removeClass('animacion');
      }
      if($('body').hasClass('fp-viewing-Ingredients-1')){
        $('.info-ingredients .an').removeClass('animacion');
      }
    }
    if(anchorLink == 'Products'){
      clean();
      products();
    }
    if(anchorLink == 'Contact'){
      clean();
      contact();
    }
  },

  onLeave: function(anchorLink, index, nextIndex, direction, origin, destination, anchors, section, slideIndex) {
    if(anchorLink == 'Home'){
      story();
    }
  },

  afterSlideLoad: function( anchorLink, index, slideAnchor, slideIndex,nextIndex,destination) {
    if(anchorLink == 'Story' && slideIndex == 0) {
      clean();
            story();
    }
    if(anchorLink == 'Story' && slideIndex == 1) {
      clean();
            mision();
    }
    if(anchorLink == 'Story' && slideIndex == 2) {
      clean();
           vision();
    }
    if(anchorLink == 'Ingredients' && slideIndex == 1){
      clean();
      ingredientsDetail();
    }
    if(anchorLink == 'Ingredients' && slideIndex == 0){
      clean();
      ingredients();
    }

  },


  // onSlideLeave: function( anchorLink, index, slideIndex, direction) {
  //   if(anchorLink == 'Story' && slideIndex == 0) {
  //     clean();
  //   }
  //   if(anchorLink == 'Story' && slideIndex == 1) {
  //     clean();
  //   }
  //   if(anchorLink == 'Story' && slideIndex == 2) {
  //     clean();
  //   }
  // } 
}); 
    } 



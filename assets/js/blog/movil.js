if ($(window).width() <= 767) {
	$(document).ready(function () {
    setTimeout(function(){
    	$('#inicio .fadeIn').delay(4000).removeClass('delay-15s');
    	$('#inicio .fadeIn').delay(4000).removeClass('delay-2s');
    },2000);

    //smoothscroll
    $('a.jump-link, a.icon-up-open-big, .movil a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");
        
        $('a').each(function () {
            $(this).removeClass('active');
        })
        $(this).addClass('active');
      
        var target = this.hash,
            menu = target;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top+2
        }, 500, 'swing', function () {
            window.location.hash = target;
        });
    });

});
	// init controller
	var controller = new ScrollMagic.Controller();
// Anima la sección de about
new ScrollMagic.Scene({
	triggerElement: "#about", triggerHook: 0.5, duration: "180%", offset: 0}).setClassToggle("#about .an", "animacion").addTo(controller);
// Agrega la clase active al menú
new ScrollMagic.Scene({
	triggerElement: "#about",
	triggerHook: 0.5, // show, when scrolled 10% into view
	duration: "180%", // hide 10% before exiting view (80% + 10% from bottom)
	offset: 0 // move trigger to center of element
	}).setClassToggle("#about-menu", "active").addTo(controller);

				new ScrollMagic.Scene({
									triggerElement: "#inicio",
									triggerHook: 0, // show, when scrolled 10% into view
									duration: "60%", // hide 10% before exiting view (80% + 10% from bottom)
									offset: 0 // move trigger to center of element
								})
								.setClassToggle("#inicio .fadeIn", "animacion")
								 // add indicators (requires plugin)
								.addTo(controller);
				new ScrollMagic.Scene({
									triggerElement: "#about",
									triggerHook: 0, // show, when scrolled 10% into view
									duration: "60%", // hide 10% before exiting view (80% + 10% from bottom)
									offset: 0 // move trigger to center of element
								})
								.removeClassToggle("#inicio .fadeIn", "delay-15s")
								 // add indicators (requires plugin)
								.addTo(controller);

				new ScrollMagic.Scene({
									triggerElement: "#inicio",
									triggerHook: 0, // show, when scrolled 10% into view
									duration: "60%", // hide 10% before exiting view (80% + 10% from bottom)
									offset: 0 // move trigger to center of element
								})
								.setClassToggle("#home-menu", "active")
								 // add indicators (requires plugin)
								.addTo(controller);

				new ScrollMagic.Scene({
									triggerElement: ".mision-w",
									triggerHook: 0.5, // show, when scrolled 10% into view
									duration: "80%", // hide 10% before exiting view (80% + 10% from bottom)
									offset: 0 // move trigger to center of element
								})
								.setClassToggle(".mision-w .an", "animacion")
								 // add indicators (requires plugin)
								.addTo(controller);
				new ScrollMagic.Scene({
									triggerElement: ".mision-w",
									triggerHook: 0.5, // show, when scrolled 10% into view
									duration: "80%", // hide 10% before exiting view (80% + 10% from bottom)
									offset: 0 // move trigger to center of element
								})
								.setClassToggle("#mission-menu", "active")
								 // add indicators (requires plugin)
								.addTo(controller);

				new ScrollMagic.Scene({
									triggerElement: ".vision-w",
									triggerHook: 0.5, // show, when scrolled 10% into view
									duration: "100%", // hide 10% before exiting view (80% + 10% from bottom)
									offset: 0 // move trigger to center of element
								})
								.setClassToggle(".vision-w .an", "animacion")
								 // add indicators (requires plugin)
								.addTo(controller);

				new ScrollMagic.Scene({
									triggerElement: ".vision-w",
									triggerHook: 0.5, // show, when scrolled 10% into view
									duration: "100%", // hide 10% before exiting view (80% + 10% from bottom)
									offset: 0 // move trigger to center of element
								})
								.setClassToggle("#vision-menu", "active")
								 // add indicators (requires plugin)
								.addTo(controller);

				new ScrollMagic.Scene({
									triggerElement: ".info-ingredients",
									triggerHook: 0.5, // show, when scrolled 10% into view
									duration: "60%", // hide 10% before exiting view (80% + 10% from bottom)
									offset: 0 // move trigger to center of element
								})
								.setClassToggle(".info-ingredients .an", "animacion")
								 // add indicators (requires plugin)
								.addTo(controller);

				new ScrollMagic.Scene({
									triggerElement: ".info-ingredients",
									triggerHook: 0.5, // show, when scrolled 10% into view
									duration: "310%", // hide 10% before exiting view (80% + 10% from bottom)
									offset: 0 // move trigger to center of element
								})
								.setClassToggle("#ingre-menu", "active")
								 // add indicators (requires plugin)
								.addTo(controller);

				new ScrollMagic.Scene({
									triggerElement: "#cac",
									triggerHook: 0.5, // show, when scrolled 10% into view
									duration: "230%", // hide 10% before exiting view (80% + 10% from bottom)
									offset: 0 // move trigger to center of element
								})
								.setClassToggle("#cac .an", "animacion")
								//.addIndicators() // add indicators (requires plugin)
								.addTo(controller);


				new ScrollMagic.Scene({
									triggerElement: "#productos",
									triggerHook: 0.5, // show, when scrolled 10% into view
									duration: "80%", // hide 10% before exiting view (80% + 10% from bottom)
									offset: 0 // move trigger to center of element
								})
								.setClassToggle("#productos .an", "animacion")
								 // add indicators (requires plugin)
								.addTo(controller);

				new ScrollMagic.Scene({
									triggerElement: "#productos",
									triggerHook: 0.5, // show, when scrolled 10% into view
									duration: "80%", // hide 10% before exiting view (80% + 10% from bottom)
									offset: 0 // move trigger to center of element
								})
								.setClassToggle("#productos-menu", "active")
								 //.addIndicators()
								.addTo(controller);
				new ScrollMagic.Scene({
									triggerElement: "#contacto",
									triggerHook: 0.9, // show, when scrolled 10% into view
									duration: "110%", // hide 10% before exiting view (80% + 10% from bottom)
									offset: -50 // move trigger to center of element
								})
								.setClassToggle("#contacto.an", "animacion")
					//.addIndicators() // add indicators (requires plugin)
								.addTo(controller);
				new ScrollMagic.Scene({
									triggerElement: "#contacto",
									triggerHook: 0.9, // show, when scrolled 10% into view
									duration: "110%", // hide 10% before exiting view (80% + 10% from bottom)
									offset: -50 // move trigger to center of element
								})
								.setClassToggle("#contacto .an", "animacion")
					//.addIndicators() // add indicators (requires plugin)
								.addTo(controller);

				new ScrollMagic.Scene({
									triggerElement: "#contacto",
									triggerHook: 0.5, // show, when scrolled 10% into view
									duration: "100%", // hide 10% before exiting view (80% + 10% from bottom)
									offset: 0 // move trigger to center of element
								})
								.setClassToggle("#contacto-menu", "active")
								 // add indicators (requires plugin)
								.addTo(controller);

}
document.addEventListener('DOMContentLoaded', () => {
    iniciarAPP();
})

function iniciarAPP() {
    //console.log("iniciando app")
    consultarApi();
}

async function consultarApi() {
    try {
        const url = 'http://localhost/konscio-admin/VideosInicio';
        const results = await fetch(url);
        const videos = await results.json();
        //console.log(videos);
        mostrarVideos(videos);
    } catch (e) {
        console.error(e);
    }
}

const links = document.querySelector(".links-pagination");
const section = document.querySelector("#videoSection");
function mostrarVideos(videos) {
    console.log(videos, "prueba videos desde function")
    videos.Data.forEach(video => {
        const contenedor = document.createElement('DIV')
        contenedor.classList.add("col-3")
        contenedor.classList.add("videoindi")

        const segunConte = document.createElement("DIV")
        segunConte.setAttribute('id',"light")

        const a = document.createElement("A")
        a.setAttribute("id","boxclose")
        a.classList.add("boxclose")
        a.setAttribute("onclick","lightbox_close()")

        const tagVideo = document.createElement("VIDEO")
        tagVideo.setAttribute("id","boxclose")
        tagVideo.setAttribute("width","600")
        tagVideo.setAttribute("controls","controls")
        tagVideo.setAttribute("id","VisaChipCardVideo")

        const source = document.createElement("SOURCE")
        source.setAttribute("src",`https://www.youtube.com/embed/Ubsseiyfhfo`)
        source.setAttribute('type', 'video/mp4');

        const fade = document.createElement("DIV")
        fade.setAttribute("onclick","lightbox_close()")

        const imgContent = document.createElement("DIV")
        const imgA = document.createElement("A");
        imgA.setAttribute("onclick","lightbox_open()")

        const img = document.createElement("IMG")
        img.classList.add("imgVideo")
        img.setAttribute("src",`./uploads/cover_post/${video.cover_image_es}`)

        imgA.appendChild(img);
        imgContent.appendChild(imgA)
        tagVideo.appendChild(source)
        contenedor.appendChild(segunConte)
        contenedor.appendChild(fade);
        segunConte.appendChild(a)
        segunConte.appendChild(tagVideo)
        contenedor.appendChild(imgContent)
        section.appendChild(contenedor);
    })
    console.log(section,"prueba")

}

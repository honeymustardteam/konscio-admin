window.document.onkeydown = function(e) {
  if (!e) {
    e = event;
  }
  if (e.keyCode == 27) {
    lightbox_close();
  }
}

function lightbox_open(urlYoutube) {
  const videoId = getId(urlYoutube);
  window.scrollTo(0, 0);
  document.getElementById('light').style.display = 'block';
  document.getElementById('fade').style.display = 'block';

  loadIframe('iframeYoutube', 'https://www.youtube.com/embed/'+videoId)
  //lightBoxVideo.play();
}

function lightbox_close() {
  document.getElementById('light').style.display = 'none';
  document.getElementById('fade').style.display = 'none';
  //lightBoxVideo.pause();
  loadIframe('iframeYoutube', '')
}


function loadIframe(iframeName, url) {
  var $iframe = $('#' + iframeName);
  if ( $iframe.length ) {
      $iframe.attr('src',url);   
      return false;
  }
  return true;
}

function getId(url) {
  const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
  const match = url.match(regExp);

  return (match && match[2].length === 11)
    ? match[2]
    : null;
}

<?php 

class Admin extends MY_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->database();
		//Importamos librerias
		$this->load->library("parser");
		$this->load->library("Form_validation");
		//Importamos helpers
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('Post_helper');
		$this->load->helper('Date_helper');
		$this->load->helper('text');
		//Importamos los modelos
		$this->load->model('Post');
		//Acemos que la autorizacion de este controlador sea solo por el admin
		$this->init_seccion_auto(9);
	}

	/*****************************************************
	**********************Posts**********************
	*****************************************************/

	public function index()
	{
		redirect(base_url().'admin/posts');
		//$view["body"] = $this->load->view('admin/index',null,true);
		//$view["title"] = "Inicio";
		//$this->parser->parse("template/body",$view);
	}

	public function posts($num_page = 1){

		$num_page--;
        $num_post = $this->Post->count_post_admin();
        $last_page = ceil($num_post / PAGE_SIZE);

        if ($num_page < 0) {
            $num_page = 0;
        } elseif ($num_page > $last_page) {
            // TODO
            $num_page = 0;
        }

        $offset = $num_page * PAGE_SIZE;
		
		$data['last_page'] = $last_page;
		$data['current_page'] = $num_page;
		$data['last_page'] = $last_page;
		$data['pagination'] = true;
		$data['token_url'] = 'admin/posts/';
		$data["posts"] = $this->Post->get_pagination_admin($offset,'admin');
		//$data['main_posts'] = $this->Main_Post->get_main_posts();
		$view["body"] = $this->load->view('admin/post/index',$data,true);
		$view["title"] = "Posts";
		$this->parser->parse("template/body",$view);
	}

	public function post($post_id=null){
		if($post_id==null){
			$main_posts = [];
			//Crear post
			$data['title'] = $data['content'] = $data['description'] 
			= $data['posted'] = $data['url_clean'] = $data['cover_image'] = '';
		} else {
			//Editar post
			$post = $this->Post->find($post_id);

			if(is_array($post)){

				if(count($post)==0){
					show_404();
					return ;
				}

			}

			$data['title'] = $post->title;
			$data['content'] = $post->content;
			$data['posted'] = $post->posted;
			$data['url_clean'] = $post->url_clean;
			$data['cover_image'] = $post->cover_image;
		}

		if($this->input->server('REQUEST_METHOD')=="POST"){
			$this->form_validation->set_rules('title','Titulo','required|min_length[5]|max_length[255]');
			$this->form_validation->set_rules('content','Contenido','required|min_length[10]');
			$this->form_validation->set_rules('posted','Publicado','required');

			//Llenar campos con la información anterior
			$data['title'] = $this->input->post('title');
			$data['subtitle'] = $this->input->post('subtitle');
			$data['content'] = $this->input->post('content');
			$data['description'] = $this->input->post('description');
			$data['posted'] = $this->input->post('posted');
			$data['url_clean'] = $this->input->post('url_clean');

			if($this->form_validation->run()){

				$url_clean = clean_name($this->input->post('title'));

				$save = array(
					'content' => $this->input->post('content'),
					'title' => $this->input->post('title'),
					'posted' => $this->input->post('posted'),
					'url_clean' => $url_clean,
				);
				if($post_id==null){
					$post_id = $this->Post->insert($save);
				} else {
					$this->Post->update($post_id, $save);
				}
				if (isset($_FILES['cover_upload']) && $_FILES['cover_upload']['name'] != ''){
					$this->upload($post_id,$this->input->post('title'),'cover_upload','cover_image');
				}
				//Una vez subidas las imagenes se reedirecciona a la url
				redirect(base_url()."admin/posts");
			}
		}
		//Si no hay una peticion POST entonces mostrarmos la vista de crear
		$data["data_posted"] = posted();
		$view["body"] = $this->load->view('admin/post/crearPost',$data,true);
		$this->parser->parse("template/body",$view);
	}

	public function post_delete($post_id=null){
		if($post_id==null){
			echo "0";
		} else {
			$this->Post->delete($post_id);
			echo "1";
		}
	}

	function images_server(){
		$data['images'] = all_images();
		//var_dump($data);
		$this->load->view("admin/post/image",$data);
	}

	public function upload($post_id = null,$title = null,$image="upload",$name_image='image'){
		//Configuramos los datos de la imagen
		//echo "<script>console.log('SUBIENDO IMAGEN..')</script>";
		$type_img = ($image=="upload") ? '' : 'cover_';
		$image = $image;
		if($title!=null)
			$title = clean_name($title);

		$config['upload_path'] = "uploads/".$type_img."post";

		if($title!=null)
			$config['file_name'] = $title; //$type_img.clean_name($title);

		$config['allowed_types'] = "gif|jpg|png";
		$config['max_size'] = 50000;
		$config['overwrite'] = true;
		//Cargamos libreria de subir archivos
		$this->load->library('upload',$config);
		$this->upload->initialize($config);
		if ($this->upload->do_upload($image)) {
			//echo "Se sube la imagen ".$title."<br>";
			//echo "Imagen ".$image;
			//Se carga la imagen
			$data = $this->upload->data();
			if($title!=null && $post_id!=null){
				$save = array($name_image => $title.$data["file_ext"]);
				$this->Post->update($post_id,$save);
			} else{
				$title = $data['file_name'];
				echo json_encode(array("fileName" => $title, "uploaded" => 1,"url" => "/".PROJECT_FOLDER."/uploads/post/".$title));
			}
			$this->resize_image($data['full_path'],$title.$data["file_ext"]);
		} else {
			//echo "No se sube la imagen";
		}
	}

	public function resize_image($path_image,$image_name){
		$config['image_library'] = 'gd2';
		$config['source_image'] = $path_image;
		//$config['new_image'] = 'uploads/';
		$config['maintain_ratio'] = TRUE;
		//$config['create_thumb'] = TRUE;
		//$config['width'] = 500;
		//$config['height'] = 500;

		$this->load->library('image_lib',$config);
		$this->image_lib->resize();

	}

}
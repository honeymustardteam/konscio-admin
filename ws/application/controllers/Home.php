<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Home extends CI_Controller {



	public function __construct(){

		parent::__construct();

		$this->load->database();

        $this->load->helper('url');

		$this->load->library("parser");
		
		$this->load->library('session');

		//Helpers

		$this->load->helper('Post_helper');

		$this->load->helper('text');

		$this->load->helper('Date_helper');

		//Importamos los modelos

		$this->load->model('Post');
		//Lenguajes
		$this->lang->load("home","spanish");
		$this->lang->load("home","english");
		

	}

	public function index($num_page = 1) {



		$num_page--;

        $num_post = $this->Post->count_posted();

        $last_page = ceil($num_post / PAGE_SIZE);



        if ($num_page < 0) {

            $num_page = 0;

        } elseif ($num_page > $last_page) {

            // TODO

            $num_page = 0;

        }



		$offset = $num_page * PAGE_SIZE;



		$data['posts'] = $this->Post->get_pagination($offset);

		$data['last_page'] = $last_page;

        $data['current_page'] = $num_page;

		$data['token_url'] = 'blog/';

		$data['pagination'] = true;

		//Traduccion cargadas desde la carpeta Language y su respectivo lenguaje(carpeta)
		$this->session->userdata('site_lang');
		//navbar traducido
		$data["msg1"] = $this->lang->line("biogr");
		$data["msg2"] = $this->lang->line("edcu");
		$data["msg3"] = $this->lang->line("the");
		$data["msg4"] = $this->lang->line("public");
		$data["msg5"] = $this->lang->line("entre");
		$data["msg6"] = $this->lang->line("cour");
		$data["language_msg38"] = $this->lang->line("contact");

		$data["language_msg"] = $this->lang->line("intro");
		$data["language_msg1"] = $this->lang->line("bio");
		$data["ver"] = $this->lang->line("ver");
		$data["language_msg2"] = $this->lang->line("biography");
		$data["language_msg3"] = $this->lang->line("biography1");	
		$data["language_msg4"] = $this->lang->line("education");
		$data["language_msg5"] = $this->lang->line("lic");
		$data["language_msg6"] = $this->lang->line("date");
		$data["language_msg7"] = $this->lang->line("honor");
		$data["language_msg8"] = $this->lang->line("master");
		$data["language_msg9"] = $this->lang->line("date2");
		$data["language_msg10"] = $this->lang->line("milton");
		$data["language_msg11"] = $this->lang->line("doctor");
		$data["language_msg12"] = $this->lang->line("date3");
		$data["language_msg13"] = $this->lang->line("thanos");
		$data["ver2"] = $this->lang->line("ver2");
		
		$data["language_msg14"] = $this->lang->line("thera");
		$data["language_msg15"] = $this->lang->line("date4");
		$data["language_msg16"] = $this->lang->line("mary");

		$data["language_msg17"] = $this->lang->line("date5");
		$data["language_msg18"] = $this->lang->line("gesalt");
		$data["language_msg19"] = $this->lang->line("gesalt1");
		
		$data["language_msg20"] = $this->lang->line("date6");
		$data["language_msg21"] = $this->lang->line("weiss");

		$data["language_msg22"] = $this->lang->line("date7");
		$data["language_msg23"] = $this->lang->line("quant");
	
		$data["language_msg24"] = $this->lang->line("mange");
		$data["language_msg25"] = $this->lang->line("mange1");
		
		$data["language_msg26"] = $this->lang->line("reiki");
		$data["language_msg27"] = $this->lang->line("date8");
		$data["language_msg28"] = $this->lang->line("reiki1");
		
		$data["language_msg29"] = $this->lang->line("date9");
		$data["language_msg30"] = $this->lang->line("nyc");
		
		$data["language_msg31"] = $this->lang->line("date10");
		$data["language_msg32"] = $this->lang->line("new");
		
		$data["language_msg33"] = $this->lang->line("repro");
		$data["language_msg34"] = $this->lang->line("date11");
		$data["language_msg35"] = $this->lang->line("repro1");
		
		$data["language_msg36"] = $this->lang->line("pattern");
		$data["language_msg37"] = $this->lang->line("pattern1");
		
		$data["language_msg38"] = $this->lang->line("contact");
		$data["language_msg39"] = $this->lang->line("name");
		$data["language_msg40"] = $this->lang->line("phone");
		$data["language_msg41"] = $this->lang->line("email");
		$data["language_msg42"] = $this->lang->line("messa");
		$data["language_msg43"] = $this->lang->line("succ");
		$data["language_msg44"] = $this->lang->line("error");
		$data["language_msg45"] = $this->lang->line("send");
		$view["nav"] = $this->load->view('home/template/nav',$data,true);
		
		$view["content"] = $this->load->view('home/home',$data,true);
			
        $this->parser->parse('home/template/base',$view);
		//$this->session->set_userdata('site_lang');
		
	}




	public function blog($clean_url){



		if (!isset($clean_url)) {

            show_404();

        }



		$post = $this->Post->GetByUrlClean($clean_url);



        if (!isset($post)) {

            show_404();

		}

		

		$data['post'] = $post;



		$view['content'] = $this->load->view("home/blog", $data, TRUE);

        $this->parser->parse('home/template/base',$view);

		

	}



	public function cursos() {
		$this->session->userdata('site_lang');
		$data["msg1"] = $this->lang->line("biogr");
		$data["msg2"] = $this->lang->line("edcu");
		$data["msg3"] = $this->lang->line("the");
		$data["msg4"] = $this->lang->line("public");
		$data["msg5"] = $this->lang->line("entre");
		$data["msg6"] = $this->lang->line("cour");
		$data["language_msg38"] = $this->lang->line("contact");
		//traducción de la página
		$data["course"] = $this->lang->line("course");
		$data["cur"] = $this->lang->line("cur");
		$data["cur1"] = $this->lang->line("cur1");	
		$data["cur2"] = $this->lang->line("cur2");
		$data["cur3"] = $this->lang->line("cur3");
		$data["cur4"] = $this->lang->line("cur4");
		$data["ct0"] = $this->lang->line("ct0");
		$data["ct1"] = $this->lang->line("ct1");	
		$data["ct2"] = $this->lang->line("ct2");
		$data["ct3"] = $this->lang->line("ct3");
		$data["ct4"] = $this->lang->line("ct4");
		
		$view["nav"] = $this->load->view('home/template/nav',$data,true);
		$view["content"] = $this->load->view('home/cursos',null,true);
		
        $this->parser->parse('home/template/base',$view);

	}



	public function links(){
		//nav y sesion
		$this->session->userdata('site_lang');
		$data["msg1"] = $this->lang->line("biogr");
		$data["msg2"] = $this->lang->line("edcu");
		$data["msg3"] = $this->lang->line("the");
		$data["msg4"] = $this->lang->line("public");
		$data["msg5"] = $this->lang->line("entre");
		$data["msg6"] = $this->lang->line("cour");
		$data["language_msg38"] = $this->lang->line("contact");
		
		$data["link"] = $this->lang->line("link");
		$data["link1"] = $this->lang->line("link1");
		$data["book1"] = $this->lang->line("book1");

		$view["nav"] = $this->load->view('home/template/nav',$data,true);
		$view["content"] = $this->load->view('home/links',null,true);

        $this->parser->parse('home/template/base',$view);

	}



	public function entrevistas(){
		$this->session->userdata('site_lang');
		$data["msg1"] = $this->lang->line("biogr");
		$data["msg2"] = $this->lang->line("edcu");
		$data["msg3"] = $this->lang->line("the");
		$data["msg4"] = $this->lang->line("public");
		$data["msg5"] = $this->lang->line("entre");
		$data["msg6"] = $this->lang->line("cour");
		$data["language_msg38"] = $this->lang->line("contact");

		$data["inter"] = $this->lang->line("inter");
		$data["edate"] = $this->lang->line("edate");
		$data["edate1"] = $this->lang->line("edate1");
		$data["edate2"] = $this->lang->line("edate2");
		$data["edate3"] = $this->lang->line("edate3");
		$data["edate4"] = $this->lang->line("edate4");
		$data["edate5"] = $this->lang->line("edate5");
		$data["en1"] = $this->lang->line("en1");
		$data["en2"] = $this->lang->line("en2");
		$data["en3"] = $this->lang->line("en3");
		$data["en4"] = $this->lang->line("en4");
		$data["en5"] = $this->lang->line("en5");
		//tv
		$data["tvt"] = $this->lang->line("tvt");
		$data["tv"] = $this->lang->line("tv");
		$data["tv1"] = $this->lang->line("tv1");
		$data["tv2"] = $this->lang->line("tv2");
		$data["tv3"] = $this->lang->line("tv3");
		$data["tv4"] = $this->lang->line("tv4");
		$data["tdate"] = $this->lang->line("tdate");
		$data["tdate1"] = $this->lang->line("tdate1");
		$data["tdate2"] = $this->lang->line("tdate2");
		$data["tdate3"] = $this->lang->line("tdate3");
		$data["tdate3"] = $this->lang->line("tdate3");
		
		$view["nav"] = $this->load->view('home/template/nav',$data,true);
		$view["content"] = $this->load->view('home/entrevistas',null,true);

        $this->parser->parse('home/template/base',$view);

	}



	public function terapias(){	
		$this->session->userdata('site_lang');

		$view["content"] = $this->load->view('home/terapias',null,true);

        $this->parser->parse('home/template/base',$view);

	}



	public function educacion(){
		$this->session->userdata('site_lang');
		$view["content"] = $this->load->view('home/educacion',null,true);

        $this->parser->parse('home/template/base',$view);

	}	



	public function publicaciones(){
		//cargar la variable de lenguaje y navbar traducido
		$this->session->userdata('site_lang');
		
		$data["msg1"] = $this->lang->line("biogr");
		$data["msg2"] = $this->lang->line("edcu");
		$data["msg3"] = $this->lang->line("the");
		$data["msg4"] = $this->lang->line("public");
		$data["msg5"] = $this->lang->line("entre");
		$data["msg6"] = $this->lang->line("cour");
		$data["language_msg38"] = $this->lang->line("contact");
		//publicaciones

		$data["pub"] = $this->lang->line("pub");
		$data["book"] = $this->lang->line("book");
		$data["text"] = $this->lang->line("text");
		$data["text1"] = $this->lang->line("text1");

		$data["book1"] = $this->lang->line("book1");
		$data["text2"] = $this->lang->line("text2");
		$data["text3"] = $this->lang->line("text3");
		$data["text4"] = $this->lang->line("text4");
		$data["text5"] = $this->lang->line("text5");		

		$view["nav"] = $this->load->view('home/template/nav',$data,true);
		$view["content"] = $this->load->view('home/publicaciones',null,true);
        $this->parser->parse('home/template/base',$view);

	}





}


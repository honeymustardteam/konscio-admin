<?php
$lang["intro"] = "The next step in human evolution is one of consciousness.";
$lang["ver"] = "SEE THERAPIES";
$lang["bio"] = "BIO";
$lang["biography"] = "My name is Milly Diericx and I have dedicated my life to healing myself and others. My story begins with my having lupus and facing the very real chance that I would die in a couple of days. The medical prognosis being so negative, I had two choices, either accept it and give away my power to others and live in fear, or do something about it. I decided to live my life in my own terms and seek the ways I could heal myself, and eventually, when I had done this, to help others do the same. In my journey to healing I learned various techniques that brought me back to health and that I now guide others through. However, if I had to boil my recovery to a single thing, it would be that each one of us has the power to heal, that the body is designed to be healthy and well, all we need to do is give it the tools to do so. The techniques are just that, tools that help put the body back in balance, so that it can heal itself.";
$lang["biography1"] = "The most powerful tool in this set is to achieve a change in consciousness, being aware of our own power, our own worth, and to love and accept ourselves unconditionally. Only from acceptance can we do something aboutour circumstances.";

$lang["education"] = "EDUCATION";
$lang["lic"] = "BA in Sociology";
$lang["date"] = "October 13,1993";
$lang["honor"] = "Honors.Sociology studies the tendencies in societies, what we do and why we do it.";
$lang["master"] = "Masters in Ericksonian Psycotherapy";
$lang["date2"] = "July 15, 2014";
$lang["milton"] = " Milton Erickson is the founder of modern hypnosis. His method is an easyand fast way to access the unconcious mind and get it on board with our projects and use the tools it holds for our healing.";

$lang["doctor"] = "Ph.D in Thanatology, December 10";
$lang["date3"] = "December 10, 2019";
$lang["thanos"] ="Thanatology is the relationship of man with loss and grief. Life is full of small,great and even devastating losses, and they all need to be dealt with as a grieving process.";
$lang["ver2"] = "SEE MORE";
$lang["thera"] = "Therapies";
//terapias
$lang["date4"] = "August, 2007";
$lang["mary"] =" Jin Shin Jyutsu is a blueprint of energy points that reminds the body of it’s creation, of it’s perfect functioning. During life, these energy locks get blocked and we need to unblock them to go back to the proper function of our bodies.";

$lang["date5"] = "February 18, 2007";
$lang["gesalt"] ="Gestalt Institute, Energetic Reprogramming for Health";
$lang["gesalt1"] = "This therapy is designed to alleviate allergic reactions. When we have an allergy, it is our body overrracting to a substance that shouldn’t incite that recation. By treating the substance with it’s corresponding emotion, you deprogram the overreaction of the body.";

$lang["date6"] = "October 8-12, 2012";
$lang["weiss"] = "This regression accesses the soul memories of past life experiences, helping us understand and resolve the issues presenting themselves persistently in this life time that seem to have no explanation, like very difficult relationships, circumstances and phobias.";

$lang["date7"] = "7 de Abril, 2019";
$lang["quant"] = "Based on the principles of quantum physics, it is a different hypnotic technique in which we expand our consciousness to the different levels of our being, where we can see our true magnificence as multidimensional beings and touch on our eternal consciousness.";

$lang["mange"] = "Medical biomagnetism with Isaac Goiz Durán";
$lang["mange1"] = "Biomagnestism is a very useful tool in elliminating virus, bacteria, fungi and parasites through the placing of magnets in certain locations throughout the body.";

$lang["reiki"] = "Rami Rion, Reiki Master";
$lang["date8"] = "October 28, 2008";
$lang["reiki1"] = "Through energetic transmission, the Reiki practitioner acts as a channel of healing energy for the receiver, whose system uses the healing energy where it needs it. Reiki acts as an accelerator of thenatural healing energies of the body.";

$lang["date9"] = "April 20, 2012";
$lang["nyc"] = "Sometimes we are in contact with disencarnate energies that can’t leave the earth plane for some reason. This contact helps them resolve whatever they need to move on.";

$lang["date10"] = "January  2015";
$lang["new"] ="This hypnotic regression to the soul’s memories acceses the life we live as souls, when we are not in the body. In this state of consciousness we can meet our spiritual guides, find our mission and purpose and the evolutionary trajectory of our souls.";

$lang["repro"] ="Quantum Bio Reprogramming";
$lang["date11"] = "January 16, 2020";
$lang["repro1"] = "This technique is a cleansing of the obstacles and blockages found in the energy field. Through kinsesiological verification, we can clear the energetic debris left by our experiences in this life from our energetic bodies.";

$lang["pattern"] = "Resonance Reppatterning Method by Chloe Wordsworth";
$lang["pattern1"] = "Resonance Reppatterning looks for the root cause of the problem we want to resolve through kinesiologic verification, and when the root cause of the negative beliefs that are causing the obstacles in our lives is found, it changes our resonancewith them. What we resonate with is what we experience, so we need to be resonating with what we do want in this life and stop resonating with what we don’t want to live and experience.";
//contacto
$lang["contact"] = "Contact";
$lang["name"] ="Name";
$lang["phone"] ="Telephone";
$lang["email"] = "Email";
$lang["messa"] = "Message";
$lang["succ"] = "The message has been sent";
$lang["error"] = "An error has occurred ";
$lang["send"] = "Send";
//nav
$lang["biogr"] ="Biography";
$lang["edcu"] ="Education";
$lang["the"] ="Therapies";
$lang["public"] ="Publications";
$lang["entre"] ="Interviews";
$lang["cour"] ="Courses";
// pagina de publicaciones
$lang["pub"] ="PUBLICATIONS";
$lang["book"] = "Books";
$lang["text"] ="My body had become my enemy. It was slowly but surely killing me relentlessly. All I could feel was emptiness in my gut, a sense of hoplesness so profound that it was a black hole, swallowing everything else… I had no feelings whatsoever, I could not be angry or worried or afraid. I could not love, and I could not care for anyone, not even myself…
My life had absolutley no purpose: many times all I wanted was for my treacherous body to get on with it and finish the job. The pain was horrendous. My skin chaffed between the sheets. I could not move at all. I was helpless as a newborn mouse, exposed, vulnerable, weak, in the dark. 
…my attention was completely taken over by a single thought: I would rather die than live like this…";
$lang["text1"] ="";

$lang["book1"] ="Consciousness and Evolution";
$lang["text2"] ="The next step in human evolution is one of conciousness";
$lang["text3"] ="Consciousness is hard to define, it’s still being debated as to what it is precisely. But we do know at least three things: 
<br> 1.	We can be conscious of ourselves as sentient beings, as Descartes, would say, “I think, therefore I am”. 
<br> 2.	We can question ourselves, as to who we are, where we come from, where we are going, we are curious and like to discover how things work and why. 
<br> 3.	We can choose from a variety of options, what to believe, what to do, how to react. 
";
$lang["text4"] ="So I define consciousness as these three things: self awareness, questioning and choice. In the book I will go into the levels of consciousness and how to shift from one to the other. To me they are:
   <br> First level: Ego Consciousness, 
   <br> Second level: Empathic Consciousness, 
   <br> Third level: One Consciousness and 
   <br> Fourth level: Source Consciousness.
    ";
$lang["text5"] ="The higher you go, the more you become the master of your destiny through choice. To shift from one to another, first, you have to become aware that you’re not your physical body only, that you are much, much more than that. You’re like a Russian doll, with your physical body being the smallest doll of the four. In this Egoic Consciousness, it’s all about me, mine. The next doll up is the Empathic Consciousness, here it’s no longer me, mine, but us, ours. The world is not here to serve my ego, but I’m here to be part of the world, and everything I do has an effect on the others sharing this planet with me, people, plants, animals, every sentient being. Here we realize we are connected and we can start to feel as the other, and choose accordingly.
In the third level, One Consciousness, we are even more aware, not only that we are connected and affect eachother, but that in fact we are all one, so whatever I do to anyone/anything else, I am in fact, doing to myself, and I choose accordingly. In the last level, Source Consciouness, we are the Creator. We realize that everything that is is only one energy, Source Energy, and so we are Source Energy and we can choose from here, as Source. 
But let’s walk before we run. In the first level, start shifting your consciousness from outside to within, from what’s happening “out there”, to what is happening “in here”: what I’m thinking, how I’m feeling, how my body feels, shift consciousness from different parts of your body, and then to your emotions, feel them in your body, name them, be aware of them. The same with your thoughts, discover your patterns of thought, the way they make you feel, change your thought patterns at will. When you master this, then you can shift to the second level, Empathic Consciousness, the wisdom of the heart.
Put your awareness into your heart space, ask your heart how would this choice affect others (from other people to animals, plants, the whole planet). This shift from the first to the second level is what we are being asked to do right now by the circumstances. The crisis we are facing will not be resolved from the Egoic Consciousness, which created the problem in the first place. We have to unite to solve the grave problems facing humanity. So we have to choose. Either we shift and solve the crisis together, or we don’t and we get to the point of no return. It is ultimately our choice, each and every one of us must choose";
//cursos
$lang["course"] ="COURSES";

$lang["cur"] ="Know your body, Enhance your Health";
$lang["ct0"] ="A course to learn about ourselves and become healthier. With the cummulative knowledge of 20 years of being a therapist and seeing the results in myself and others, I created this course that condenses my learning about the body and what it needs to be healthy and the simplified techniques we can all do at home. The content is:
    <br> Health/Illness
    <br> Learn to know your body
    <br> The Mind and the Emotions
    <br> Habits- Introducing positive habits";
$lang["cur1"] ="Learn to Work with your Energy";
$lang["ct1"] ="We are not only a physical body. We are energetic beings manifested in a physical body. When we know how to manage the elevated levels of energy, the results in our lives are much faster and lasting.";
$lang["cur2"] ="From Grief to Spirit";
$lang["ct2"] ="As a Ph.D in Thanatology I know that the loss of a loved one is devastating. To be able to come through the sequels left by such a circumstance, we must appeal to the higher levels of our being, the spiritual level. Leaving aside religious connotations, we are all spiritual beings, and it is in this level that we can find the relief that is hard to find in the physical plane.";
$lang["cur3"] ="The Science of Possibility, Quantum Conciousness";
$lang["ct3"] ="Introduction to quantum physics, and how, if we extrapolate its premises to psychic levels, it can help us connect to the field of infinite possibilities, where everything is possible.";
$lang["cur4"] ="The Levels of Consciousness";
$lang["ct4"] ="Learn what the levels of consciousness are about and how to access them. The critical situation we find ourselves in is demanding a shift in consciousness, a higher consciousness to overcome the present worldwide crisis. The next step in human evolution is one of consciousness.";
$lang["link"] ="Related websites";
$lang["link1"] ="Social media";
//entrevistas
$lang["inter"] ="INTERVIEWS";

$lang["edate"] ="7 de December, 2016";

$lang["en1"] ="It’s Your Health with Lisa Davies, Interview to Milly Diericx";
$lang["edate1"] ="December, 2016";

$lang["en2"] = "The Dona Seebo Show, Interview <i>Befriending the Wolf</i>";
$lang["edate2"] ="Marzo 30, 2017";

$lang["en3"] = "Adrenalina Radio in the program Luces y Sombras with Nuria Becerril,  interview about the the Holistic Road to Balance";
$lang["edate3"] ="Thursday April 25, 2019";

$lang["en4"] = "Voice America interview with Dr. Jean Marie Farish in the program Love Light, living in the Spirit of Love with the theme of “Raising Consciousness to the Vibration of Love”";
$lang["edate4"] ="Friday 22nd of May, 2020";

$lang["en5"] = "Expert Insights Radio, Interview for the book Be Love: A Conscious Shift to Birthing the Future, about the theme Heal Yourself, Heal the World";
$lang["edate5"] ="September 24 / October 1, 2019";

$lang["tvt"] ="On TV";

$lang["tv"] ="NY1 News, <i>La incansable batalla femenina contra el lupus</i>";
$lang["tdate"] ="January 11, 2017";

$lang["tv1"] ="Frente al País with Ana Paula Ordorica, News story about Clínica Bienestar 360";
$lang["tdate1"] ="July 16, 2018";

$lang["tv2"] ="Entrevista en Expert TV en el programa La Era de la Nueva Mujer con Elisa Eraña, acerca de Salud Alternativa";
$lang["tdate2"] ="May 7, 2019";

$lang["tv3"] ="Be Free Be Fun Be Fearless! Episode 2: How To heal yourself from Lupus & other auto-immune diseases";
$lang["tdate3"] ="Septiembre 14, 2020";

$lang["tv4"] ="Eighteen months ago, Bill Bennett went around the globe to create a movie about fear.He interviewed a wide range of people, including the experts on incluidos los principales expertos en miedo";

$lang[""] ="";
$lang[""] ="";
$lang[""] ="";
$lang[""] ="";
<?php
$lang["intro"] = "El siguiente paso en la evolución del ser humano, es uno de consciencia.";
$lang["ver"] = "VER TERAPIAS";
$lang["bio"] = "BIOGRAFÍA";
$lang["biography"] = "Mi nombre es Milly Diericx, y he dedicado mi vida adulta a encontrar métodos de sanación alternativos que funcionen. Primero lo hice por mí, para salir de un Lupus Eritematoso Sistémico bastante grave, y al encontrar alivio y eventualmente el regreso de mi salud, compartiéndolo con otras personas como terapeuta. En este camino he acumulado muchas técnicas probadas en mí y mis queridos consultantes que funcionan, no sólo para sentirse mejor, sino efectivamente para mejorar la vida en general, desde la salud, las relaciones personales, el trabajo y el bienestar general.";
$lang["biography1"] = "Ahora estoy trabajando en un proyecto más ambicioso, ayudar a quien lo desee a subir el nivel de consciencia, ya que he llegado a la conclusión que el siguiente paso en la evolución del ser humano, es uno de consciencia.";

$lang["education"] = "EDUCACIÓN";
$lang["lic"] = " Licenciada en Sociología";
$lang["date"] = "13 de Octubre, 1993";
$lang["honor"] = "Mención honorífica.La sociología estudia las tendencias sociales, lo que hacemos y por que lo hacemos.";
$lang["master"] = "Maestría en Psicoterapia Ericksoniana";
$lang["date2"] = "15 de Julio, 2014";
$lang["milton"] = "Milton Erickson es el padre de la hipnosis moderna. Su método es una manera rápida y fácil de accesar a la mente inconsciente y ponerla de acuerdo con nuestros proyectos y usar las herramientas que se encuentran en ella para utilizarla para sanarnos.";

$lang["doctor"] = "Doctorado en Tanatología";
$lang["date3"] = "10 de Diciembre 2019";
$lang["thanos"] = "La Tanatología trata con la relación del hombre con la pérdida y el duelo. La vida está llena de pérdidas, pequeñas, significativas y devastadoras. Todas deben de ser tratadas como procesos de duelo.";
$lang["ver2"] = "VER MÁS";
$lang["thera"] = "Terapias";
//terapias
$lang["date4"] = "Agosto 2007";
$lang["mary"] =  "Jin Shin Jyutsu, método traído a Occidente por Mary Burmeister, es un plano de puntos energéticos que recuerdan al cuerpo su creación, su función perfecta. Durante la vida estos candados energéticos se bloquean y necesitamos desbloquearlos para regresar al funcionamiento apropiado de nuestro cuerpo.";

$lang["date5"] = "18 de Febrero, 2007";
$lang["gesalt"] ="Instituto de Gestalt, Reprogramación Energética para la Salud";
$lang["gesalt1"] = "Esta terapia tomada por Sandy Radomski de N.A.E.T., está diseñada para aliviar las reacciones alérgicas. Cuando tenemos una reacción alérgica, es el cuerpo sobre reaccionando a una sustancia que no debería de provocar esa reacción. Al tratar la sustancia con su emoción correspondiente, se desprograma la sobre reacción del cuerpo.";

$lang["date6"] = "October 8-12, 2012";
$lang["weiss"] = "Técnica desarrollada por Brian Weiss. Esta regresión accesa las memorias del alma de las experiencias de vidas pasadas, ayudándonos a resolver los problemas persistentes en esta vida que parecen no tener explicación, como relaciones conflictivas, circunstancias muy difíciles o fobias.";

$lang["date7"] = "7 de Abril, 2019";
$lang["quant"] = "Basada en los principios de la física cuántica. Es una técnica hipnótica diferente, desarrollada por Peter Smith, en la que expandimos la consciencia a los diferentes niveles de nuestro ser, donde podemos ver nuestra verdadera magnificencia como seres multidimensionales y tocar nuestra esencia eterna.";

$lang["mange"] = "Biomagnetismo";
$lang["mange1"] = "Método creado por Isaac Goiz. A través de imanes se crea un ambiente interno en el cuerpo que inhibe la reproducción de virus, bacterias y patógenos.";

$lang["reiki"] = "Master Reiki";
$lang["date8"] = "28 de Octubre, 2008";
$lang["reiki1"] = "Impartida por Rami Rion. A través de la transmisión energética, el facilitador de Reiki actúa como canal de la energía curativa para el receptor, cuyo sistema utiliza donde la necesita. El Reiki actúa como acelerador de las energías sanadoras naturales del cuerpo.";

$lang["date9"] = "20 de Abril 2020";
$lang["nyc"] = "Impartido por James Van Praagh. A veces estamos en contacto con energías desencarnadas que no pueden dejar el plano terrestre por alguna razón. Este contacto les ayuda a resolver las situaciones pendientes para poder seguir adelante.";

$lang["date10"] = "Enero 2015";
$lang["new"] ="Michael Newton desarrolla esta regresión hipnótica a las memorias del alma que accesa la vida que llevamos como almas, cuando no estamos en el cuerpo. En este estado de consciencia podemos conocer a nuestros guías espirituales, nuestra misión y propósito de vida y la trayectoria evolutiva de nuestra alma.";

$lang["repro"] ="Bio Reprogramación Cuántica";
$lang["date11"] = "16 de Enero, 2020";
$lang["repro1"] = "Esta técnica de Claudio Caliendi es una limpieza de los obstáculos y bloqueos que se encuentran en el campo energético. A través de la verificación kinesiológica podemos barrer los residuos energéticos de las experiencias de vida de nuestro campo.";

$lang["pattern"] = "Transformación de Patrones de Resonancia";
$lang["pattern1"] = "Busca la causa de los problemas utilizando la kinesiología y a través de técnicas curativas de varias disciplinas, cambia nuestra resonancia con ellas, haciendo que el problema actual deje de ser un obstáculo. Aquello con lo que resuenas es lo que vives.";
//contacto
$lang["contact"] = "Contacto";
$lang["name"] ="Nombre";
$lang["phone"] ="Teléfono";
$lang["email"] = "Correo electrónico";
$lang["messa"] = "Mensaje";
$lang["succ"] = "El mensaje ha sido enviado correctamente";
$lang["error"] = "Ocurrió un error inesperado";
$lang["send"] = "Enviar";
//nav
$lang["biogr"] ="Biografía";
$lang["edcu"] ="Educación";
$lang["the"] ="Terapias";
$lang["public"] ="Publicaciones";
$lang["entre"] ="Entrevistas";
$lang["cour"] ="Cursos";
//pagina de publicaciones
$lang["pub"] ="PUBLICACIONES";
$lang["book"] = "Libros";
$lang["text"] ="Mi cuerpo se había convertido en mi enemigo. Estaba lenta y seguramente matándome implacablemente. Lo único que sentía era un vacío en el estómago, una sensación de desesperanza tan profundo que era un agujero negro, engullendo todo lo demás. No tenía emociones de ningún tipo, no podía sentir enojo, preocupación o miedo. No podía amar ni me importaba nadie, ni yo misma…

Muchas veces sólo quería que mi cuerpo traicionero terminara de una vez con lo que había emprendido. El dolor era horrendo. Mi piel se irritaba en las sábanas. No podía moverme para nada. Estaba desprotegida como un ratón recién nacido, expuesta, vulnerable, débil, en la obscuridad. 

…mi atención estaba completamente enfocada en un sólo pensamiento: prefiero morir que vivir así…";
$lang["text1"] ="La Transformación de Patrones de Resonancia busca la causa raíz de los problemas que queremos resolver a través de la verificación kinesiológica, y cuando encontramos la raíz de las creencias negativas que causan obstáculos en nuestras vidas, cambia la resonancia con ellas. Con aquello con lo que resonamos es lo que experimentamos, así que tenemos que dejar de resonar con lo que no queremos y comenzar a resonar con lo que deseamos vivir.";
    
$lang["book1"] ="Consciencia y Evolución";
$lang["text2"] ="El siguiente paso en la evolución humana es uno de consciencia.";
$lang["text3"] ="La consciencia es difícil de definir, sigue siendo debatido qué es exactamente. Pero sí sabemos tres cosas acerca de ella:<br>

Podemos tener consciencia de nosotros mismos como seres sintientes. Como diría Descartes, “Pienso, luego existo.”<br>

Podemos cuestionarnos, a respecto de quiénes somos, de dónde venimos y a dónde vamos, somos curiosos y nos gusta descubrir cómo funcionan las cosas y porqué.<br>

Podemos elegir de una variedad de opciones, que creer, que hacer, como reaccionar.";
$lang["text4"] ="Así que yo defino la conciencia como estas tres cosas, auto conocimiento, capacidad de cuestionarnos y libre albedrío. En el libro voy a entrar a detalle en los diferentes niveles de consciencia y cómo pasar de uno a otro. Los niveles de consciencia son, para mí:
    <br> Primer nivel: Conciencia Egoica.
    <br> Segundo nivel: Conciencia Empática.
    <br> Tercer nivel: Conciencia Unificada.
    <br> Cuarto nivel: Conciencia de la Fuente";
$lang["text5"] ="Entre más alto vayas, más te conviertes en el amo de tu destino a través de tus decisiones. Para poder cambiar de un nivel de consciencia a otro, primero tienes que saber que no eres sólo un cuerpo físico, sino que eres mucho, mucho más que eso. Somos como una muñeca rusa, con la más pequeña siendo el cuerpo físico, el primer nivel de consciencia. En este nivel, el de la Conciencia Egoica, todo se trata de yo/mío. La segunda muñeca, la que sigue, es la Conciencia Empática, aquí ya no se trata de yo/mío, sino de nosotros/nuestro. El mundo no está aquí para servir a mi ego, sino que yo estoy aquí para ser parte del mundo, para aportar a él y todo lo que hago tiene un efecto sobre los demás con los que comparto el mundo, afecta a las otras personas, plantas, animales, a todos los seres sintientes de este planeta. En este nivel de consciencia sabemos que estamos conectados, podemos sentir como el otro, y empezamos a elegir acorde a ese saber. En el tercer nivel nos volvemos aún más conscientes, ya no sólo de que nuestro actuar tiene efectos en el otro, sino que realmente somos uno, así que cualquier cosa que le haga al otro (persona o cosa) realmente me lo estoy haciendo a mí mismo y empiezo a tomar decisiones desde la Consciencia Unificada. En el último nivel de conciencia, la Conciencia de la Fuente, entendemos que somos el Creador. Realizamos que todo lo que es viene de una misma energía, de la energía creadora, y como tal somos esa Energía de la Fuente y podemos elegir desde aquí. Pero caminemos antes de querer correr. En el primer nivel de conciencia, vamos empezando a llevar nuestra atención de lo que está pasando “ahí afuera” a lo que está pasando “aquí adentro”: Que estoy pensando, cómo me estoy sintiendo, cómo se siente mi cuerpo poniendo mi atención en sus diferentes partes, cómo se sienten las emociones en mi cuerpo, nombrándolas, siendo conscientes de ellas. Lo mismo con los patrones de pensamiento, obsérvalos, sé consciente de ellos, observa cómo te hacen sentir, cámbialos a voluntad. Cuando logres dominar este nivel, puedes empezar a cambiar al segundo nivel, a la sabiduría del corazón. Pon tu atención en el espacio del corazón y pregúntale a la sabiduría del corazón cómo esta elección daña a los demás, (desde las otras personas, plantas, animales, al planeta completo). Ese cambio del primer al segundo nivel es lo que las circunstancias que estamos viviendo nos están pidiendo. La crisis que estamos viviendo no va a ser resuelta desde la Conciencia Egoica, que fue la causó el problema. Tenemos que unirnos para resolver los graves problemas que enfrentamos como humanidad. Así que debemos de elegir. O cambiamos y resolvemos la crisis juntos o no lo hacemos y seguimos hasta alcanzar el punto sin retorno. Es, en última instancia nuestra decisión. Cada uno de nosotros debe de elegir.";
//Cursos
$lang["course"] ="CURSOS";

$lang["cur"] ="Conoce tu Cuerpo, Mejora tu Salud";
$lang["ct0"]="Un curso para aprender a conocernos y así, ser más sanos. Con el conocimiento acumulado de dedicarme a dar terapia por más de 20 años y ver los resultados en otras personas, cree este curso que condensa todo el aprendizaje que tuve, las técnicas simplificadas que podemos hacer en casa, para conocernos, comprender lo que nos mantiene mal, cambiarlo y estar cada vez mejor.
<br> El temario del curso es:
<br> Salud/Enfermedad
<br> Aprende a Conocer tu Cuerpo
<br> La Mente y las Emociones
<br> Introducir Hábitos Positivos";
$lang["cur1"] ="Aprende a manejar tu Energía";
$lang["ct1"] ="No sólo somos un cuerpo físico. Somos seres energéticos manifestados en un cuerpo físico. Cuando sabemos cómo manejar estos niveles superiores de energía, los resultados en la vida son mucho más rápidos y duraderos.";
$lang["cur2"] ="Del Duelo al Espíritu";
$lang["ct2"] ="Como doctora en Tanatología, sé que la pérdida de un ser amado es devastadora. Para poder salir de las secuelas que deja una circunstancia así, debemos de apelar a los niveles superiores de quien somos, al nivel espiritual. Sin connotaciones religiosas, somos todos seres espirituales, y en este nivel se encuentra el alivio que en el plano físico es difícil encontrar.";
$lang["cur3"] = "La ciencia de las posibilidades, la Conciencia Cuántica";
$lang["ct3"] = "Introducción a la física cuántica, y cómo, al extrapolarla a la vida psíquica, nos ayuda a conectarnos con el campo de posibilidades infinitas en donde todo es posible.";
$lang["cur4"] ="Los niveles de Conciencia";
$lang["ct4"] ="Aprende de qué se tratan los diferentes niveles de conciencia y cómo accesarlos. La situación crítica en la que nos encontramos nos está pidiendo un cambio de consciencia, una elevación para poder salir de la crisis mundial. El siguiente paso en la evolución humana es uno de consciencia.";
$lang["link"] ="Sitios relacionados";
$lang["link1"] ="Redes sociales";
//Entrevistas
$lang["inter"] ="INTERVIEWS";

$lang["edate"] ="7 de December, 2016";

$lang["en1"] ="It’s Your Health with Lisa Davies “Entrevista a Milly Diericx”";
$lang["edate1"] ="16 de Diciembre, 2016";

$lang["en2"] = "The Dona Seebo Show, Entrevista sobre <i>Befriending the Wolf</i>";
$lang["edate2"] ="30 de Marzo, 2017";

$lang["en3"] = "Adrenalina Radio en el programa Luces y Sombras con Nuria Becerril, acerca de el Camino Holístico hacia el Equilibrio";
$lang["edate3"] ="25 de Abril, 2019";

$lang["en4"] = "Voice America entrevista con la Dra. Jean Marie Farish en el programa Love Light, living in the Spirit of Love con el tema “Raising Conciousness to the Vibration of Love”";
$lang["edate4"] ="22 de Mayo, 2020";

$lang["en5"] = "Expert Insights Radio, Interview for the book Be Love: A Conscious Shift to Birthing the Future, about the theme Heal Yourself, Heal the World";
$lang["edate5"] ="24 de Septiembre / 1 de Octubre, 2019";

$lang["tvt"] ="En TV";

$lang["tv"] ="NY1 Noticias, La incansable batalla femenina contra el lupus";
$lang["tdate"] ="11 de Enero, 2017";

$lang["tv1"] ="Frente al País con Ana Paula Ordorica, Reportaje acerca de Clínica Bienestar 360";
$lang["tdate1"] ="16 de Julio, 2018";

$lang["tv2"] ="Entrevista en Expert TV en el programa La Era de la Nueva Mujer con Elisa Eraña, acerca de Salud Alternativa";
$lang["tdate2"] ="7 de Mayo, 2019";

$lang["tv3"] ="Be Free Be Fun Be Fearless! Episode 2: How To heal yourself from Lupus & other auto-immune diseases";
$lang["tdate3"] ="14 de Septiembre, 2020";

$lang["tv4"] ="Hace dieciocho meses, Bill Bennett dio la vuelta al mundo para hacer una película sobre el miedo. Entrevistó a una amplia gama de personas, incluidos los principales expertos en miedo";
$lang[""] ="";
$lang[""] ="";
$lang[""] ="";
$lang[""] ="";
$lang[""] ="";
$lang[""] ="";



<div class="row">
	<div class="col-lg-9 col-md-12">
		<!-- Add New Post Form -->
		<?php echo form_open('','class="add-new-post" enctype="multipart/form-data"') ?>
		<div class="card card-small mb-3">
			<div class="card-body">
				

				<div class="form-group">
					<?php echo form_label('Título', 'title'); ?> 
					<?php
					$text_input = array(
						'name' => 'title',
						'autofocus' => true,
						'placeholder' => 'Escriba el título del post',
						'required' => true,
						'minlength' => '5',
						'maxlength' => '255',
						'id' => 'title',
						'value' => $title,
						'class' => 'form-control input-lg'
					);
					echo form_input($text_input);
					?> 
					<?php echo form_error('title','<div class="text-error">','</div>'); ?>
				</div>

				<div class="form-group">
					 
					<?php
					$text_input = array(
						'name' => 'url_clean',
						'id' => 'url_clean',
						'type' => 'hidden',
						'value' => $url_clean,
						'class' => 'form-control input-lg'
					);
					echo form_input($text_input);
					?> 
					<?php echo form_error('url_clean','<div class="text-error">','</div>'); ?>
				</div>

				<div class="form-group">
					<?php echo form_label('Contenido', 'content'); ?> 
					<?php
					$text_area = array(
						'name' => 'content',
						'required' => true,
						'id' => 'content',
						'value' => $content,
						'class' => 'form-control input-lg'
					);
					echo form_textarea($text_area);
					?> 
					<?php echo form_error('content','<div class="text-error">','</div>'); ?>
				</div>
				
				<div class="form-group">
					<?php echo form_label('Imagen de portada', 'cover_image'); ?> 
					<?php
					$input_cover = array(
						'name' => 'cover_upload',
						'id' => 'cover_upload',
						'value' => '',
						'type' => 'file',
						'accept' => '.jpg, .png, .jpeg',
						'class' => 'form-control input-lg'
					);
					echo form_input($input_cover);
					?> 
					<?php echo form_error('cover_upload','<div class="text-error">','</div>'); ?>
					<?php echo $cover_image!="" ? '<img class="img_post img-thumbnail img-presentation-small" src="'.base_url().'uploads/cover_post/'.$cover_image.'"></img>' : ''; ?>
				</div>

				<?php echo form_submit('mySubmit', 'Guardar', 'class="btn btn-primary"'); ?> 

				 
			</div>
		</div>
		<!-- / Add New Post Form -->
	</div>

	<div class="col-lg-3 col-md-12">
		<!-- Post Overview -->
		<div class='card card-small mb-3'>
			<div class="card-header border-bottom">
				<h6 class="m-0">Acciones</h6>
			</div>
			<div class='card-body p-0'>
				<ul class="list-group list-group-flush">
					<li class="list-group-item p-3">
						<div class="form-group">
								<i class="material-icons mr-1">visibility</i>
								<?php echo form_label('Publicado', 'posted'); ?>
								<?php echo form_dropdown('posted', $data_posted, $posted, 'class="form-control input-lg"'); ?>  
								<?php echo form_error('posted','<div class="text-error">','</div>'); ?>
						</div>
					</li>

						</ul>
					</div>
				</div>
		</div>
		<?php echo form_close() ?>
		<!-- / Post Overview -->
	</div>
</div>

<script>
	
	$(function(){
		var editor = CKEDITOR.replace( 'content', {
			height:400,
		    filebrowserBrowseUrl: "<?php echo base_url() ?>admin/images_server",
		    filebrowserUploadUrl: "<?php echo base_url() ?>admin/upload",
		});
	});

	//Validar tamaño de las imagenes

	var uploadCoverImage = document.getElementById("cover_upload");

	uploadCoverImage.onchange = function() {
		if(this.files[0].size > 1048576){
		alert("EL tamaño del archivo debe ser menor a 1mb");
		this.value = "";
		};
	};

</script>
<div class="row">

	<div class="col-lg-8 sm-12">

		<div class="card card-small mb-4">

			<div class="card-header border-bottom">

				<h6 class="m-0">Iniciar sesión</h6>

			</div>

			<ul class="list-group list-group-flush">

				<li class="list-group-item p-3">

					<div class="row">

						<div class="col">

							<?php echo form_open( base_url().'app/ajax_attempt_login', ['class' => 'std-form'] ); ?>

								<div class="form-row">

									<div class="form-group col-md-12">

										<label for="login_string">Email o Usuario</label>

										<input type="text" autofocus class="form-control" name="login_string" id="login_string" placeholder="Email o usuario" value=""> 

									</div>

								</div>

								<div class="form-row">

									<div class="form-group col-md-12">

										<label for="login_pass">Contraseña</label>

										<input type="password" class="form-control" name="login_pass" id="login_pass" placeholder="Contraseña" value=""> 

									</div>

								</div>

								<input type="hidden" id="max_allowed_attempts" value="<?php echo config_item('max_allowed_attempts'); ?>" />

								<input type="hidden" id="mins_on_hold" value="<?php echo ( config_item('seconds_on_hold') / 60 ); ?>" />

								<!--<input type="submit" name="submit" value="Iniciar sesión" id="submit_button"  />-->

								<button type="submit">Login</button>

							</form>

						</div>

					</div>

				</li>

			</ul>

		</div>

	</div>

</div>





<script>

$(document).ready(function(){

	$(document).on( 'submit', 'form', function(e){

		$.ajax({

			type: 'post',

			cache: false,

			url: '<?php echo base_url() ?>app/ajax_attempt_login',

			data: {

				'login_string': $('[name="login_string"]').val(),

				'login_pass': $('[name="login_pass"]').val(),

				'loginToken': $('[name="token"]').val(),

			},

			dataType: 'json',

			success: function(response){

				$('[name="loginToken"]').val(response.token)

				console.log(response);

				if(response.status == 1){

					window.location.href='<?php echo base_url() ?>admin/posts'

				}else if(response.status == 0 && response.on_hold){

					$('form').hide();

					$('#on-hold-message').show();

					alert('Intentos de inicio de sesión exedidos');

				}else if(response.status == 0 && response.count){

					alert('Failed login attempt ' + response.count + ' of ' + $('#max_allowed_attempts').val());

					location.reload();

				}

			},

			error: function(error){

				console.log("Error")

				console.log(error)

			},

		});

		return false;

	});

});

</script>
<div class="row d-flex intro">
    <?php foreach ($main_posts as $key => $m) : ?>
        <?php if($key==0){ ?>
        <div class="col-md-8 principal mb-md-0 mb-4" style="background-image: url(<?php echo base_url()."/uploads/cover_post/".$m->cover_image ?>);background-size: cover;background-position: center;">
            <div class="excerpt">
                <h1 class="titulo"><?php echo $m->title ?></h1>
                <div class="fecha"><?php echo format_date($m->created_at) ?></div>
                <p><?php echo word_limiter ($m->description,15) ?></p>
                <a href="<?php echo base_url() . 'blog/'. $m->url_clean ?>" class="seguir">Seguir leyendo</a>
            </div>
        </div>
        <div class="col-md-4 px-0">
        <?php } else { ?>
        
            <div class="secundaria ml-md-1" style="background-image: url(<?php echo base_url()."/uploads/cover_post/".$m->cover_image ?>);background-size: cover;background-position: center;">
                <div class="excerpt">
                    <h1 class="titulo"><?php echo $m->title ?></h1>
                    <div class="fecha"><?php echo format_date($m->created_at) ?></div>
                    <p><?php echo word_limiter ($m->description,15) ?></p>
                    <a href="<?php echo base_url() . 'blog/'. $m->url_clean ?>" class="seguir">Seguir leyendo</a>
                </div>
            </div>
        <?php } ?>
        
    <?php endforeach; ?>
        </div>
</div>
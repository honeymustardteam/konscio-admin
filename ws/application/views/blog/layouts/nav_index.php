<a class="navbar-brand" href="<?php echo base_url();?>chaacfoods"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="170.8px" height="28.6px" viewBox="0 0 170.8 28.6" style="enable-background:new 0 0 170.8 28.6;" xml:space="preserve">
          <style type="text/css">
            .st0{fill:#E75125;}
            .st1{fill:#F7B0C0;stroke:#323F1C;stroke-width:0.4305;stroke-miterlimit:10;}
          </style>
          <defs>
          </defs>
          <g>
            <g>
              <path class="st0" d="M15.6,22.2c0.6,0,1.2,0.4,1.5,1.2c-0.5-0.1-0.9,0-1.1,0.7c-0.1,0.8-0.4,1.6-0.9,2.3c-1.1,1.5-2.8,2.2-4.5,2.2
                c-3.5,0-5.6-2.4-6.4-5.6c-0.5-1.8-0.7-4.1-0.8-6.3c0-1-0.3-1.6-1.3-1.9c1.2-0.4,1.3-1.1,1.4-2.2c0.2-2.3,0.5-4.3,1-6
                c0.9-2.9,3.4-5,6.5-4.8c1.5,0.1,3,0.8,4.1,2.2c0.6,0.7,0.9,1.5,0.9,2.3c0.2,0.7,0.7,0.8,1.1,0.7c-0.3,0.9-0.9,1.2-1.6,1.2
                C15,9,14,9.5,12.6,9.2C14.6,4.9,9,3,8.1,7.9c-0.2,0.8-0.4,2.5-0.5,4.7c-0.1,1.1,0,1.8,1.2,2.2c-1,0.3-1.3,0.9-1.3,1.9
                c0,2.1,0.2,4.1,0.6,5.7c1.5,5.7,6.1,2.2,4.6-1.3C14.1,20.9,15.1,21.4,15.6,22.2z"/>
              <path class="st0" d="M24.7,13.3c0,0.1,0,0.2,0,0.3c1.2-1,2.9-1.4,4.3-0.5c0.2,0.2,0.4,0.4,0.6,0.6v-1.1V9.1c0-0.9-0.9-1.6-1.7-1.6
                c0-1,0.9-1.9,2-1.9c0.7,0,1.3,0.4,1.6,0.9c0.4-0.6,1-0.9,1.7-0.9c1.1,0,1.9,0.9,1.9,1.9c-0.9,0-1.7,0.7-1.7,1.6v3.4
                c0,1.1,0.2,1.8,1.4,2.2c-1.2,0.4-1.4,1.2-1.4,2.3v3.8c0,1,0.7,1.7,1.7,1.8c0,1-0.9,1.9-1.9,1.9c-0.7,0-1.3-0.4-1.7-0.9
                c-0.3,0.6-0.9,0.9-1.6,0.9c-1.1,0-1.9-0.9-2-1.9c1.1,0,1.7-0.7,1.7-1.8V17v-0.4c-0.2,0.3-0.4,0.5-0.7,0.7c-0.7,0.5-0.8,0.2-0.7,0
                c0.5-2.2-3.5-3.4-3.5,0.5v3.2c0,1,0.7,1.7,1.7,1.8c0,1-0.9,1.9-2,1.9c-0.7,0-1.3-0.4-1.6-0.9c-0.3,0.6-0.9,0.9-1.7,0.9
                c-1,0-1.9-0.9-1.9-1.9c1.1,0,1.7-0.7,1.7-1.8V17c0-1-0.2-1.8-1.4-2.2c1.1-0.4,1.4-1.1,1.4-2.2V9.1c0-0.9-0.9-1.6-1.7-1.6
                c0-1,0.9-1.9,1.9-1.9c0.7,0,1.4,0.4,1.7,0.9c0.3-0.6,0.9-0.9,1.6-0.9c1.1,0,1.9,0.9,2,1.9c-0.9,0-1.7,0.7-1.7,1.6V13.3z"/>
              <path class="st0" d="M42.9,20.9c-0.1,0.5-0.2,1-0.1,1.5c0.1,0.5,0.6,0.9,1.1,1c-0.4,1.5-2.9,1.6-3.6,0.1c-0.8,1.7-3.3,1.4-3.2-0.8
                c1.2,0.1,1.7-0.5,1.9-1.5l1-4.2c0.2-1,0.2-1.8-0.9-2.2c1.3-0.4,1.6-1.2,1.8-2.3l0.8-3.3c0.2-1,0.2-1.8-0.9-1.8
                c0.4-2.1,2.9-2.3,4.1-1c1.1-1.4,3.6-1.1,4.1,1c-1.1,0-1.1,0.7-0.9,1.8l0.7,3.3c0.2,1.1,0.6,1.8,1.8,2.2c-1,0.4-1.1,1.1-0.9,2.2
                l1,4.3c0.2,0.9,0.8,1.6,2,1.5c-0.3,2.2-2.5,2.5-3.3,0.9c-0.5,1.4-3.1,1.4-3.5-0.1c0.9,0,1.4-0.9,1.2-1.9c-0.2-0.7-0.3-1.4-0.5-2
                c-0.1,0.2-0.2,0.5-0.4,0.8c-0.5,0.7-0.7,0.4-0.7,0.2C45.6,18.8,43.3,19,42.9,20.9z M45,11.5c-0.4,1.8-0.8,3.4-1.1,5
                c0.9-0.4,1.8-0.1,2.4,0.4C45.9,15.1,45.5,13.5,45,11.5z"/>
              <path class="st0" d="M60.8,20.9c-0.1,0.5-0.2,1-0.1,1.5c0.1,0.5,0.6,0.9,1.1,1c-0.4,1.5-2.9,1.6-3.6,0.1c-0.8,1.7-3.3,1.4-3.2-0.8
                c1.2,0.1,1.7-0.5,1.9-1.5l1-4.2c0.2-1,0.2-1.8-0.9-2.2c1.3-0.4,1.6-1.2,1.8-2.3l0.8-3.3c0.2-1,0.2-1.8-0.9-1.8
                c0.4-2.1,2.9-2.3,4.1-1c1.1-1.4,3.6-1.1,4.1,1c-1.1,0-1.1,0.7-0.9,1.8l0.7,3.3c0.2,1.1,0.6,1.8,1.8,2.2c-1,0.4-1.1,1.1-0.9,2.2
                l1,4.3c0.2,0.9,0.8,1.6,2,1.5c-0.3,2.2-2.5,2.5-3.3,0.9c-0.5,1.4-3.1,1.4-3.5-0.1c0.9,0,1.4-0.9,1.2-1.9c-0.2-0.7-0.3-1.4-0.5-2
                c-0.1,0.2-0.2,0.5-0.4,0.8c-0.5,0.7-0.7,0.4-0.7,0.2C63.5,18.8,61.2,19,60.8,20.9z M62.9,11.5c-0.4,1.8-0.8,3.4-1.1,5
                c0.9-0.4,1.8-0.1,2.4,0.4C63.7,15.1,63.4,13.5,62.9,11.5z"/>
              <path class="st0" d="M77.3,14.8c-1.1,0.4-1.3,1-1.4,1.9c0,8.1,6.8,5,4.8,0.8c1.4-0.3,2.5,0.2,2.9,1.1c0.7,0,1.3,0.3,1.6,1.2
                c-0.5-0.1-0.9,0-1.1,0.7c-0.1,0.8-0.4,1.6-0.9,2.3c-2.4,3-7.2,3-9.5-0.3c-1-1.5-1.4-3.6-1.5-5.7c0-0.9-0.3-1.6-1.4-1.9
                c0.9-0.3,1.2-0.9,1.3-1.7c0.2-1.8,0.5-3.8,1.6-5.4c2.3-3.4,7.1-3.4,9.5-0.3C83.7,8,84,8.8,84,9.6c0.2,0.6,0.7,0.8,1.1,0.7
                c-0.3,0.8-0.9,1.2-1.5,1.2c-0.5,0.9-1.6,1.4-2.9,1.1c1.1-2.4-0.1-4.3-1.9-4.2c-2.2,0.1-3.1,3.2-2.8,5C76.1,14,76.4,14.5,77.3,14.8
                z"/>
              <path class="st0" d="M94.4,24.6c0,1,0.7,1.7,1.7,1.8c0,1.1-1.1,1.9-2,1.9c-0.9,0-1.3-0.4-1.6-0.9c-0.3,0.6-0.8,0.9-1.7,0.9
                c-0.9,0-1.9-0.8-1.9-1.9c1.1,0,1.7-0.7,1.7-1.8c0-1.4,0-6.3,0-7.7c0-1-0.2-1.8-1.4-2.2c1-0.3,1.3-1,1.4-2V5.6
                c0-0.9-0.9-1.6-1.7-1.6c0-1,1.1-1.9,1.9-1.9c0.9,0,1.4,0.4,1.7,0.9c0.3-0.5,0.8-0.7,1.3-0.8c1.4-0.3,3.4-0.1,5,0
                c0.9,0,2.5,0.2,2.8-0.6c1,0.7,0.9,1.7-0.3,2.5c1.9,1.2,0.6,3.4-0.9,3.4c0-0.9-0.7-1.7-1.6-1.7h-1.7c-2,0-2.6,0-2.6,1.8
                c0,0.9,0,5.3,0,6.1c1-0.9,2.6-1.3,3.8-0.7c1.6,0.7,1.7,2.9,0.2,4.2c-0.7,0.6-0.8,0.2-0.8,0c0.4-2.2-3.2-2.7-3.3,1
                C94.4,19.1,94.4,23.7,94.4,24.6z"/>
              <path class="st0" d="M118.9,14.8c-1,0.3-1.3,1-1.4,1.9v1.3c0,3.8-3.1,6.9-6.9,6.9h-0.2c-3.8,0-6.9-3.1-6.9-6.9v-1.2
                c0-0.9-0.3-1.6-1.4-1.9c1-0.3,1.3-1,1.4-1.9v-1c0-3.8,3.1-6.8,6.9-6.8h0.2c3.8,0,6.9,2.9,6.9,6.8v1
                C117.6,13.8,117.9,14.4,118.9,14.8z M113.8,11.7c0-1.8-1.5-3.1-3.2-3.1h-0.1c-1.8,0-3.2,1.3-3.2,3.1v1.1c0,0.9,0.3,1.6,1.4,1.9
                c-1.1,0.4-1.3,1-1.4,2v1.4c0,1.8,1.5,3.2,3.2,3.2h0.1c1.8,0,3.2-1.4,3.2-3.2v-1.4c0-0.9-0.3-1.6-1.4-1.9c1-0.3,1.3-1,1.4-1.9V11.7
                z"/>
              <path class="st0" d="M137.3,14.8c-1,0.3-1.3,1-1.4,1.9v1.3c0,3.8-3.1,6.9-6.9,6.9h-0.2c-3.8,0-6.9-3.1-6.9-6.9v-1.2
                c0-0.9-0.3-1.6-1.3-1.9c1-0.3,1.3-1,1.3-1.9v-1c0-3.8,3.1-6.8,6.9-6.8h0.2c3.8,0,6.9,2.9,6.9,6.8v1
                C136,13.8,136.2,14.4,137.3,14.8z M132.2,11.7c0-1.8-1.5-3.1-3.2-3.1h-0.1c-1.8,0-3.2,1.3-3.2,3.1v1.1c0,0.9,0.3,1.6,1.3,1.9
                c-1.1,0.4-1.3,1-1.3,2v1.4c0,1.8,1.5,3.2,3.2,3.2h0.1c1.8,0,3.2-1.4,3.2-3.2v-1.4c0-0.9-0.3-1.6-1.4-1.9c1-0.3,1.3-1,1.4-1.9V11.7
                z"/>
              <path class="st0" d="M153.8,14.8c-1.1,0.4-1.3,1-1.4,1.9c-0.1,2.5-0.8,5-2.4,6.5c-1.9,1.9-5.8,2.1-7.9,0.4
                c-0.3,0.6-0.9,0.9-1.7,0.9c-1,0-1.9-0.8-1.9-1.9c1.1,0,1.7-0.7,1.7-1.8V17c0-1-0.2-1.8-1.4-2.2c1.1-0.4,1.4-1.1,1.4-2.2V9.1
                c0-0.9-0.9-1.6-1.7-1.6c0-1.1,0.9-1.9,1.9-1.9c0.7,0,1.4,0.4,1.7,0.9c0.9-1,2.3-1.4,3.7-1.3c5,0.2,6.1,3.8,6.6,7.9
                C152.5,13.9,152.8,14.5,153.8,14.8z M148.6,16.7c0-0.9-0.3-1.6-1.3-1.9c0.8-0.3,1.1-0.8,1.3-1.4c0.2-1.2-0.2-3.2-1.3-4.2
                c-1-0.9-3.4-0.9-3.4,1v2.4v3c1.3-0.6,2.8,0.2,2.7,0.8c-1.3,0.3-1.8,1.6-1.7,2.8C145.3,22.7,148.6,21.7,148.6,16.7z"/>
              <path class="st0" d="M160.3,7.4c-1.4,5.3,7.8,5,9.1,11.7c0.2,1.1-0.1,1.9,1.3,2.3c-1.5,0.5-1.2,1.9-2,3.4
                c-1.3,2.5-3.8,3.8-7.1,3.7c-3.7-0.1-6.6-2.7-6.1-7.7c0.1-0.5,0.2-0.9,0.3-1.3c0.2-1.6,0-2.5-0.8-2.7c1.2-1.3,2.7-0.8,3.1,0.5
                c1.4-0.4,3.1,0.3,3.7,2.4c-1.9-0.3-3.6,2.4-2.3,4.3c1.6,2.2,5.2,1.1,6.1-1.6c1.6-6.1-6.1-5.9-8.4-10.8c-0.5-1-0.4-1.8-1.7-2.2
                c0.9-0.7,0.9-0.7,1-1.6c0.8-7.8,12-8.3,12.6,0c0.2,0.9,0.7,0.7,1.7,0.6c-0.5,2.1-1.4,2-2.2,2c-0.6,1.2-1.8,1.8-3.6,1.4
                c0.7-1.8,0.6-3.5,0-4.8C163.8,4.7,160.9,5.1,160.3,7.4z"/>
            </g>
            <g>
              <path class="st1" d="M14.2,20.9c0.6,0,1.2,0.4,1.5,1.2c-0.4-0.1-0.9,0-1.1,0.7c-0.1,0.8-0.4,1.6-0.9,2.3c-1.1,1.5-2.8,2.2-4.5,2.2
                c-3.5,0-5.6-2.4-6.4-5.6c-0.5-1.8-0.7-4.1-0.8-6.3c0-1-0.3-1.6-1.3-1.9C1.9,13.1,2,12.4,2,11.3c0.2-2.3,0.5-4.3,1-6
                c0.9-2.9,3.4-5,6.5-4.8c1.5,0.1,3,0.8,4.1,2.2c0.6,0.7,0.9,1.5,0.9,2.3c0.2,0.7,0.7,0.8,1.1,0.7c-0.3,0.9-0.9,1.2-1.6,1.2
                c-0.5,0.9-1.5,1.4-2.9,1c2-4.3-3.6-6.1-4.5-1.3c-0.2,0.8-0.4,2.5-0.5,4.7c-0.1,1.1,0,1.8,1.2,2.2c-1,0.3-1.3,0.9-1.3,1.9
                c0,2.1,0.2,4.1,0.6,5.7c1.5,5.7,6.1,2.2,4.6-1.3C12.7,19.6,13.7,20.1,14.2,20.9z"/>
              <path class="st1" d="M23.2,12c0,0.1,0,0.2,0,0.3c1.2-1,2.9-1.4,4.3-0.5c0.2,0.2,0.5,0.4,0.6,0.6v-1.1V7.8c0-0.9-0.9-1.6-1.7-1.6
                c0-1,0.9-1.9,2-1.9c0.7,0,1.3,0.4,1.6,0.9c0.4-0.6,1-0.9,1.7-0.9c1.1,0,1.9,0.9,1.9,1.9c-0.9,0-1.7,0.7-1.7,1.6v3.4
                c0,1.1,0.2,1.8,1.4,2.2c-1.2,0.4-1.4,1.2-1.4,2.3v3.8c0,1,0.7,1.7,1.7,1.8c0,1-0.9,1.9-1.9,1.9c-0.7,0-1.3-0.4-1.7-0.9
                c-0.3,0.6-0.9,0.9-1.6,0.9c-1.1,0-1.9-0.9-2-1.9c1.1,0,1.7-0.7,1.7-1.8v-3.9v-0.4c-0.2,0.3-0.4,0.5-0.7,0.7
                c-0.7,0.5-0.8,0.2-0.7,0c0.4-2.2-3.5-3.4-3.5,0.5v3.2c0,1,0.7,1.7,1.7,1.8c0,1-0.9,1.9-2,1.9c-0.7,0-1.3-0.4-1.6-0.9
                c-0.3,0.6-0.9,0.9-1.7,0.9c-1,0-1.9-0.9-1.9-1.9c1.1,0,1.7-0.7,1.7-1.8v-3.9c0-1-0.2-1.8-1.4-2.2c1.1-0.4,1.4-1.1,1.4-2.2V7.8
                c0-0.9-0.9-1.6-1.7-1.6c0-1,0.9-1.9,1.9-1.9c0.7,0,1.4,0.4,1.7,0.9c0.3-0.6,0.9-0.9,1.6-0.9c1.1,0,1.9,0.9,2,1.9
                c-0.9,0-1.7,0.7-1.7,1.6V12z"/>
              <path class="st1" d="M41.4,19.6c-0.1,0.5-0.2,1-0.1,1.5c0.1,0.5,0.6,0.9,1.1,1c-0.4,1.5-2.9,1.6-3.6,0.1c-0.8,1.7-3.3,1.4-3.2-0.8
                c1.2,0.1,1.7-0.5,1.9-1.5l1-4.2c0.2-1,0.2-1.8-0.9-2.2c1.3-0.4,1.6-1.2,1.8-2.3l0.8-3.3c0.2-1,0.2-1.8-0.9-1.8
                c0.4-2.1,2.9-2.3,4.1-1c1.1-1.4,3.6-1.1,4.1,1c-1.1,0-1.1,0.7-0.9,1.8l0.7,3.3c0.2,1.1,0.6,1.8,1.8,2.2c-1,0.4-1.1,1.1-0.9,2.2
                l1,4.3c0.2,0.9,0.8,1.6,2,1.5c-0.3,2.2-2.5,2.5-3.3,0.9c-0.5,1.4-3.1,1.4-3.5-0.1c0.9,0,1.4-0.9,1.2-1.9c-0.2-0.7-0.3-1.4-0.5-2
                c-0.1,0.2-0.2,0.5-0.4,0.8c-0.5,0.7-0.7,0.4-0.7,0.2C44.2,17.5,41.9,17.7,41.4,19.6z M43.6,10.2c-0.4,1.8-0.8,3.4-1.1,5
                c0.9-0.4,1.8-0.1,2.4,0.4C44.4,13.8,44,12.2,43.6,10.2z"/>
              <path class="st1" d="M59.3,19.6c-0.1,0.5-0.2,1-0.1,1.5c0.1,0.5,0.6,0.9,1.1,1c-0.4,1.5-2.9,1.6-3.6,0.1c-0.8,1.7-3.3,1.4-3.2-0.8
                c1.2,0.1,1.7-0.5,1.9-1.5l1-4.2c0.2-1,0.2-1.8-0.9-2.2c1.3-0.4,1.6-1.2,1.8-2.3l0.8-3.3c0.2-1,0.2-1.8-0.9-1.8
                c0.4-2.1,2.9-2.3,4.1-1c1.1-1.4,3.6-1.1,4.1,1c-1.1,0-1.1,0.7-0.9,1.8l0.7,3.3c0.2,1.1,0.6,1.8,1.8,2.2c-1,0.4-1.1,1.1-0.9,2.2
                l1,4.3c0.2,0.9,0.8,1.6,2,1.5c-0.3,2.2-2.5,2.5-3.3,0.9c-0.5,1.4-3.1,1.4-3.5-0.1c0.9,0,1.4-0.9,1.2-1.9c-0.2-0.7-0.3-1.4-0.4-2
                c-0.1,0.2-0.2,0.5-0.4,0.8c-0.5,0.7-0.7,0.4-0.7,0.2C62,17.5,59.8,17.7,59.3,19.6z M61.5,10.2c-0.4,1.8-0.8,3.4-1.1,5
                c0.9-0.4,1.8-0.1,2.4,0.4C62.3,13.8,61.9,12.2,61.5,10.2z"/>
              <path class="st1" d="M75.8,13.5c-1.1,0.4-1.3,1-1.4,1.9c0,8.1,6.8,5,4.8,0.8c1.4-0.3,2.5,0.2,2.9,1.1c0.7,0,1.3,0.3,1.6,1.2
                c-0.5-0.1-0.9,0-1.1,0.7c-0.1,0.8-0.4,1.6-0.9,2.3c-2.4,3-7.2,3-9.5-0.3c-1-1.5-1.4-3.6-1.5-5.7c0-0.9-0.3-1.6-1.4-1.9
                c0.9-0.3,1.2-0.9,1.3-1.7c0.2-1.8,0.4-3.8,1.6-5.4C74.5,3,79.3,3,81.7,6c0.5,0.7,0.8,1.5,0.9,2.3c0.2,0.6,0.7,0.8,1.1,0.7
                c-0.3,0.8-0.9,1.2-1.5,1.2c-0.5,0.9-1.6,1.4-2.9,1.1C80.3,8.8,79.1,7,77.3,7c-2.2,0.1-3.1,3.2-2.8,5
                C74.6,12.7,74.9,13.2,75.8,13.5z"/>
              <path class="st1" d="M92.9,23.3c0,1,0.7,1.7,1.7,1.8c0,1.1-1.1,1.9-2,1.9c-0.9,0-1.3-0.4-1.6-0.9c-0.3,0.6-0.8,0.9-1.7,0.9
                c-0.9,0-1.9-0.8-1.9-1.9c1.1,0,1.7-0.7,1.7-1.8c0-1.4,0-6.3,0-7.7c0-1-0.2-1.8-1.4-2.2c1-0.3,1.3-1,1.4-2V4.3
                c0-0.9-0.9-1.6-1.7-1.6c0-1,1.1-1.9,1.9-1.9c0.9,0,1.4,0.4,1.7,0.9c0.3-0.5,0.8-0.7,1.3-0.8c1.4-0.3,3.4-0.1,5,0
                c0.9,0,2.5,0.2,2.8-0.6c1,0.7,0.9,1.7-0.3,2.5c1.9,1.2,0.6,3.4-0.9,3.4c0-0.9-0.7-1.7-1.6-1.7h-1.7c-2,0-2.6,0-2.6,1.8
                c0,0.9,0,5.3,0,6.1c1-0.9,2.6-1.3,3.8-0.7c1.6,0.7,1.7,2.9,0.2,4.2c-0.7,0.6-0.8,0.2-0.8,0c0.5-2.2-3.2-2.7-3.3,1
                C92.9,17.8,92.9,22.4,92.9,23.3z"/>
              <path class="st1" d="M117.5,13.5c-1,0.3-1.3,1-1.4,1.9v1.3c0,3.8-3.1,6.9-6.9,6.9H109c-3.8,0-6.9-3.1-6.9-6.9v-1.2
                c0-0.9-0.3-1.6-1.4-1.9c1-0.3,1.3-1,1.4-1.9v-1c0-3.8,3.1-6.8,6.9-6.8h0.2c3.8,0,6.9,2.9,6.9,6.8v1
                C116.2,12.5,116.5,13.1,117.5,13.5z M112.4,10.4c0-1.8-1.5-3.1-3.2-3.1h-0.1c-1.8,0-3.2,1.3-3.2,3.1v1.1c0,0.9,0.3,1.6,1.4,1.9
                c-1.1,0.4-1.3,1-1.4,2v1.4c0,1.8,1.5,3.2,3.2,3.2h0.1c1.8,0,3.2-1.4,3.2-3.2v-1.4c0-0.9-0.3-1.6-1.4-1.9c1-0.3,1.3-1,1.4-1.9V10.4
                z"/>
              <path class="st1" d="M135.8,13.5c-1,0.3-1.3,1-1.4,1.9v1.3c0,3.8-3.1,6.9-6.9,6.9h-0.2c-3.8,0-6.9-3.1-6.9-6.9v-1.2
                c0-0.9-0.3-1.6-1.3-1.9c1-0.3,1.3-1,1.3-1.9v-1c0-3.8,3.1-6.8,6.9-6.8h0.2c3.8,0,6.9,2.9,6.9,6.8v1
                C134.5,12.5,134.8,13.1,135.8,13.5z M130.7,10.4c0-1.8-1.5-3.1-3.2-3.1h-0.1c-1.8,0-3.2,1.3-3.2,3.1v1.1c0,0.9,0.3,1.6,1.3,1.9
                c-1.1,0.4-1.3,1-1.3,2v1.4c0,1.8,1.5,3.2,3.2,3.2h0.1c1.8,0,3.2-1.4,3.2-3.2v-1.4c0-0.9-0.3-1.6-1.4-1.9c1-0.3,1.3-1,1.4-1.9V10.4
                z"/>
              <path class="st1" d="M152.3,13.5c-1.1,0.4-1.3,1-1.3,1.9c-0.1,2.5-0.8,5-2.4,6.5c-1.9,1.9-5.8,2.1-7.9,0.4
                c-0.3,0.6-0.9,0.9-1.7,0.9c-1,0-1.9-0.8-1.9-1.9c1.1,0,1.7-0.7,1.7-1.8v-3.9c0-1-0.2-1.8-1.3-2.2c1.1-0.4,1.3-1.1,1.3-2.2V7.8
                c0-0.9-0.9-1.6-1.7-1.6c0-1.1,0.9-1.9,1.9-1.9c0.7,0,1.4,0.4,1.7,0.9c0.9-1,2.3-1.4,3.7-1.3c5,0.2,6.1,3.8,6.6,7.9
                C151.1,12.6,151.4,13.2,152.3,13.5z M147.2,15.4c0-0.9-0.3-1.6-1.3-1.9c0.8-0.3,1.1-0.8,1.3-1.4c0.2-1.2-0.2-3.2-1.3-4.2
                c-1-0.9-3.4-0.9-3.4,1v2.4v3c1.3-0.6,2.8,0.2,2.7,0.8c-1.3,0.3-1.8,1.6-1.7,2.8C143.8,21.4,147.2,20.4,147.2,15.4z"/>
              <path class="st1" d="M158.8,6.1c-1.4,5.3,7.8,5,9.1,11.7c0.2,1.1-0.1,1.9,1.3,2.3c-1.5,0.5-1.2,1.9-2,3.4
                c-1.3,2.5-3.8,3.8-7.1,3.7c-3.7-0.1-6.6-2.7-6.1-7.7c0.1-0.5,0.2-0.9,0.3-1.3c0.2-1.6,0-2.5-0.8-2.7c1.2-1.3,2.7-0.8,3.1,0.5
                c1.4-0.4,3.1,0.3,3.7,2.4c-1.9-0.3-3.6,2.4-2.3,4.3c1.6,2.2,5.2,1.1,6.1-1.6c1.6-6.1-6.1-5.9-8.4-10.8c-0.5-1-0.4-1.8-1.7-2.2
                c0.9-0.7,0.9-0.7,1-1.6c0.8-7.8,12-8.3,12.6,0c0.2,0.9,0.7,0.7,1.7,0.6c-0.5,2.1-1.4,2-2.2,2c-0.6,1.2-1.8,1.8-3.6,1.4
                c0.7-1.8,0.6-3.5,0-4.8C162.4,3.4,159.4,3.8,158.8,6.1z"/>
            </g>
          </g>
        </svg>
      </a>
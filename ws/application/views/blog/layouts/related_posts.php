<div class="row">
    <?php foreach ($related_posts as $key => $p) : ?>
		<div class="col-md-4 noticia">
			<div class="foto" style="background: url(<?php echo base_url()."/uploads/cover_post/".$p->cover_image ?>);background-size: cover;background-position: center; "></div>
			<div class="info">
				<h1 class="titulo"><?php echo $p->title ?></h1>
  			<div class="fecha"><?php echo format_date($p->created_at) ?></div>
  			<p><?php echo word_limiter ($p->description,15) ?></p>
  			<a href="<?php echo base_url() . 'blog/' . $p->url_clean ?>" class="seguir">Seguir leyendo</a>
			</div>
		</div>
    <?php endforeach; ?>
</div>
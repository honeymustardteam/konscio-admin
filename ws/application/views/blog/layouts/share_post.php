<div class="d-flex titulo align-items-center pt-4 pb-3 comparte">
  	<div class="txt">COMPARTE</div> <div class="linea"></div>  				
</div>
<ul class="comparte-list d-flex pb-3">
  		<li><a class="fb" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url()."blog/".$url_post; ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
  		<li><a target="_blank" class="tw" href="https://twitter.com/intent/tweet?url=<?php echo base_url()."blog/".$url_post; ?>&text=<?php echo $title ?>"><i class="fa fa-twitter"></i></a></li>
  		<li><a target="_blank" class="in" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url()."blog/".$url_post; ?>&title=&summary=<?php echo $title ?>&source="><i class="fa fa-linkedin"></i></a></li>
  		<li><a target="_blank" class="pi" href="https://pinterest.com/pin/create/button/?url=<?php echo base_url()."blog/".$url_post; ?>&media=<?php echo base_url() ?>assets/images/blog/cristina-foto-n@2x-100.jpg"><i class="fa fa-pinterest"></i></a></li>
</ul>
<div class="row mt-2">
<?php foreach ($posts as $key => $p) : ?>
    <div class="col-md-6 noticia">
        <div class="foto" style="background: url('<?php echo $p->cover_image !=""? base_url()."/uploads/cover_post/".$p->cover_image :'' ?>');background-size: cover;background-position: center; ">
        <?php if($p->cover_image==""){echo "Sin imagen";} ?>
        </div>
        <div class="info">
            <h1 class="titulo"><?php echo $p->title ?></h1>
            <div class="fecha"><?php echo format_date($p->created_at) ?></div>
            <p><?php echo word_limiter ($p->description,4) ?></p>
            <a href="<?php echo base_url() . 'blog/'. $p->url_clean ?>" class="seguir">Seguir leyendo</a>
        </div>
    </div>
<?php endforeach; ?>

</div>

<?php
if ($pagination):

    $prev = $current_page - 1;
    $next = $current_page + 1;

    if ($prev < 1)
        $prev = 1;

    if ($next > $last_page)
        $next = $last_page;
    ?>
    <ul class="pagination pagination-sm row d-flex justify-content-center">
        <!--<li class="page-link"><a class="page-item" href = "<?php //echo base_url() . $token_url . $prev ?>">Prev</a></li>-->
        <?php for ($i = 1; $i <= $last_page; $i++) { ?>
            <li class="page-item"><a class="page-link" style="color:#a1a1a1" href ="<?php echo base_url() . $token_url . $i; ?> "> <?php echo $i; ?></a></li>
        <?php } ?>

        <?php if ($current_page != $next) { ?>
            <!--<li class="page-link"><a class="page-item" href = "<?php //echo base_url() . $token_url . $next; ?> ">Sig</a></li>
            -->
            <?php } ?>
    </ul>
<?php endif; ?>

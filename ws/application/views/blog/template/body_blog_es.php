<!DOCTYPE html>
<html lang="en" >

<head>

  <meta charset="UTF-8">
  <title>Chaac Foods</title>
  <?php $this->load->view('blog/layouts/head_resources') ?>

</head>
<body translate="no" >
  <!--Barra de navegacion-->
  <?php $this->load->view("blog/layouts/nav"); ?>

<div id="fullpage" class="blog">
  <div class="container mt-5 pt-5">
    <!--Posts principales-->
    {main_posts}
    <div class="row mt-5">
        <div class="col-md-8">
            <div class="d-flex titulo align-items-center">
                <div class="hojitas">
                    <img src="<?php echo base_url() ?>assets/images/blog/hojitas.svg">
                </div>
                <div class="txt">{title_tag}</div> 
                <div class="linea"></div>
            </div>
            <!--Index de los posts-->
            {body}
        </div>
        <div class="col-md-4 pr-md-0 align-self-top">
            <div class="d-flex titulo align-items-center pt-4 pb-5 tag-w">
                <div class="txt">TAGS</div> <div class="linea"></div>
            </div>
            <!--Cuerpo de las categorías-->
            {categories}
        </div>
        <div class="col-md-12 mas-visto">
            <div class="d-flex titulo align-items-center">
                <div class="hojitas">
                    <img src="<?php echo base_url() ?>assets/images/blog/hojitas.svg">
                </div>
                <div class="txt">LO MÁS VISTO</div> 
                <div class="linea"></div>
            </div>
        </div>
    </div>
    <!--Cuerpo de los posts mas vistos-->
    {most_seen}
  </div>

  <?php $this->load->view('blog/layouts/footer_en'); ?>



  <script src='<?php echo base_url() ?>assets/js/blog/jquery.js'></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/js/blog/bootstrap-bundle.min.js"></script>


</body>

</html>
 

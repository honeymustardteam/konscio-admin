<!DOCTYPE html>
<html lang="en" >

<head>

  <meta charset="UTF-8">
  <title>Chaac Foods</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/blog/font-awesome.min.css">
  <?php $this->load->view('blog/layouts/head_resources') ?>
</head>
<body translate="no" >

<!--Barra de navegacion-->
<?php $this->load->view("blog/layouts/nav"); ?>

<div id="fullpage" class="blog mt-5 pt-2">
	<div class="banner d-flex mt-3" style="background: url({cover_image});background-size: cover;background-position: center;">
		<div class="container align-self-end pb-4">
			<div class="excerpt">
				<h1 class="titulo">{title}</h1>
		  		<div class="fecha">{created_at}</div>
		  	</div>
		</div>
	</div>
  <div class="container">
  	<div class="row mt-5">
  		<div class="col-md-8 post">
  			<div class="d-flex titulo align-items-center">
  				<div class="hojitas">
  					<img src="<?php echo base_url() ?>assets/images/blog/hojitas.svg">
  				</div>
				<div class="txt">{subtitle}
  				<span>{created_at}</span>
				</div>
				
  			</div>
  			{body}
  			
  		</div>
  		<div class="col-md-4 pr-md-0 align-self-top mb-5">
  			{share_post}
  			<div class="d-flex titulo align-items-center pt-4 pb-5 tag-w">
  				<div class="txt">TAGS</div> <div class="linea"></div>
  			</div>
  			{categories}
  		</div>
  		<div class="col-md-12 mas-visto rel">
  			<div class="d-flex titulo align-items-center">
  				<div class="hojitas">
  					<img src="<?php echo base_url() ?>assets/images/blog/hojitas.svg">
  				</div>
  				<div class="txt">ARTÍCULOS RELACIONADOS</div> 
  				<div class="linea"></div>
  			</div>
  		</div>
  	</div>
  	{related_posts}
  </div>

  <?php $this->load->view('blog/layouts/footer_en'); ?>



  <script src='<?php echo base_url() ?>assets/js/blog/jquery.js'></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/js/blog/bootstrap-bundle.min.js"></script>
</body>

</html>

<!-- Masthead-->
<header class="masthead bg-primary text-white text-center" style="background-image: url(<?php echo $post->cover_image !=""? base_url()."/uploads/cover_post/".$post->cover_image :'' ?>);padding-top: 15rem;padding-bottom: 15rem;    background-position: center center;">
            <div class="container d-flex align-items-center flex-column">
                
            </div>
        </header>

        <!-- About Section-->
        <section class="page-section bg-primary mb-0" id="about">
            <div class="container">
                <!-- About Section Content-->
                <div class="row blog">
                    <div class="col-lg-10 ml-auto mr-auto">
                        <h3><?php echo $post->title ?></h3>
                                <span><?php echo format_date($post->created_at) ?></span><br><br>
                                <?php echo $post->content ?>
                            
                    </div>
                    
                </div>
            </div>
        </section>
<!-- Masthead-->

<header class="masthead bg-primary text-white text-center" style="background-image: url(<?php echo base_url().'assets/img/cursos.jpg' ?>);">

    <div class="container d-flex align-items-center flex-column">

                

        <!-- Masthead Heading-->

        <h1 class="masthead-heading text-uppercase mb-0"><?php echo $course ?></h1>

                

    </div>

</header>



<!-- About Section-->

<section class="page-section bg-primary text-white mb-0" id="about">

    <div class="container">

        <!-- About Section Content-->

        <div class="row cursos">

            <div class="col-lg-3 ml-auto">

                <img src="assets/img/curso1.jpg">

                <h3><?php echo $cur ?></h3>

                <p><?php echo $ct0 ?>
                </p>

            </div>

            <div class="col-lg-3 ">

                <img src="assets/img/curso2.jpg">

                <h3><?php echo $cur1 ?></h3>

                <p><?php echo $ct1 ?>

                </p>   

            </div>

            <div class="col-lg-3 mr-auto">

                <img src="assets/img/curso3.jpg">

                <h3><?php echo $cur2 ?></h3>

                <p><?php echo $ct2 ?>

                </p>

            </div>

            <div class="col-lg-12" style="height:100px;"></div>

            <div class="col-lg-3 ml-auto">

                <img src="assets/img/curso4.jpg">

                    <h3><?php echo $cur3 ?></h3>

                <p><?php echo $ct3 ?>.

                </p>

            </div>

            <div class="col-lg-3">

                <img src="assets/img/curso5.jpg">

                <h3><?php echo $cur4 ?></h3>

                <p><?php echo $ct3 ?>

                </p>   

            </div>

            <div class="col-lg-3 mr-auto">

                        

            </div>

        </div>

    </div>

</section>
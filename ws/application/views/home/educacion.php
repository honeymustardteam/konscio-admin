<!-- Masthead-->
<header class="masthead bg-primary text-white text-center" style="background-image: url(<?php echo base_url().'assets/img/educacion.jpg' ?>);">
            <div class="container d-flex align-items-center flex-column">
                
                <!-- Masthead Heading-->
                <h1 class="masthead-heading text-uppercase mb-0">EDUCACIÓN</h1>
                
            </div>
        </header>

        <!-- About Section-->
        <section class="page-section bg-primary text-white mb-0" id="about">
            <div class="container">
                <!-- About Section Content-->
                <div class="row educacion">
                    <div class="col-lg-4 ml-auto">
                        <ul>
                            <li>Universidad Iberoamericana: <strong>Licenciada en Sociología </strong><br>
                                <span> 13 de Octubre, 1993 </span><br>
                                Mención honorífica. La sociología estudia las tendencias sociales, lo que hacemos y por que lo hacemos.</li>
                            <li>Centro Ericksoniano de Mexico: <strong>Maestría en Psicoterapia Ericksoniana</strong> <br>
                                <span>15 de Julio, 2014</span><br>
                                Milton Erickson es el padre de la hipnosis moderna. Su método es una manera rápida y fácil de accesar a la mente inconsciente y ponerla de acuerdo con nuestros proyectos y usar las herramientas que se encuentran en ella para utilizarla para sanarnos.</li>
                            <li>RR Educación Educativa, AC: <strong>Facilitador Certificado de Transformación de Patrones de Resonancia</strong> <br>
                                <span>Septiembre 2004</span><br>
                                Método creado por Chloe Wordsworth que utiliza la kinesiología aplicada y técnicas sanadoras para cambiar la resonancia con los problemas.</li>
                            <li>The Weiss Institute: <strong>Facilitador Certificado de Regresión a Vidas Pasadas</strong><br>
                                <span>Octubre 2012</span><br>
                                Técnica creada por Brian Weiss. Hemos vivido muchas veces en diferentes cuerpos y épocas. Quienes somos hoy es en gran medida el resultado de esas vidas. Conocerlas brinda claridad acerca de los obstáculos que enfrentamos.</li>


                        </ul>
                    </div>
                    <div class="col-lg-4 mr-auto">
                        <ul>
                            <li>Instituto Mexicano de Psicooncología: <strong>Doctorado en Tanatología</strong><br>
                                <span>10 de Diciembre 2019</span><br>
                                La Tanatología trata con la relación del hombre con la pérdida y el duelo. La vida está llena de pérdidas, pequeñas, significativas y devastadoras. Todas deben de ser tratadas como procesos de duelo.</li>
                            
                            <li>The Newton Institute: <strong>Facilitador Certificado de Regresión a Vida entre Vidas</strong><br>
                                <span>Enero 2015</span><br>
                                Técnica creada por Michael Newton. Entre las vidas que vivimos en cuerpo, existe todo un mundo, donde tenemos una familia de almas, actividades, nos especializamos en diferentes campos y tenemos guías que nos ayudan a cumplir la misión a la que venimos a esta vida.</li>
                            <li>Institute for Quantum Consciousness: <strong>Facilitador Certificado de Conciencia Cuántica</strong><br>
                                <span>Abril 2017</span><br>
                                Técnica creada por Peter Smith. Expandiendo la conciencia llega a los diferentes niveles del ser para descubrir lo que realmente somos.</li>


                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <!-- Patrones de resonancia-->
        <section class="page-section bg-primary mb-0 patrones" id="about">
            <div class="container">
                <!-- About Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase">Patrones de resonancia</h2>
                <!-- Icon Divider-->
                <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                </div>
                <!-- About Section Content-->
                <div class="row">
                    <div class="col-lg-5 ml-auto">
                        <p>Método de Transformación de Patrones de Resonancia, Chloe Wordsworth:</p>

                        <p>La RR Educación Educativa AC, Facilitador Certificado de  Resonance Repatterning, 3 de Febrero, 2004.</p>
                        
                        <p>The Holographic Repatterning Association, Facilitador Certificado Nivel 2, Sept 2004</p>
                        
                        <p>RR Asociación Educativa AC, Certificado, 1 de Febrero 2014</p>
                        
                        <p>La Transformación de Patrones de Resonancia busca la causa raíz de los problemas que queremos resolver a través de la verificación kinesiológica, y cuando encontramos la raíz de las creencias negativas que causan obstáculos en nuestras vidas, cambia la resonancia con ellas. Con aquello con lo que resonamos es lo que experimentamos, así que tenemos que dejar de resonar con lo que no queremos y comenzar a resonar con lo que deseamos vivir.</p>
                    </div>
                    <div class="col-lg-5 mr-auto" style="text-align:right; display:flex"><img class="width-100" src="assets/img/patrones.jpg"></div>
                    
                </div>
            </div>
        </section>

        <!-- Otros estudios-->
        <section class="page-section bg-primary mb-0 estudios" id="about">
            <div class="container">
                <!-- About Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase">Otros estudios</h2>
                <!-- Icon Divider-->
                <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                </div>
                <!-- About Section Content-->
                <div class="row">
                    <div class="col-lg-5 ml-auto">
                        <img src="assets/img/icon1.png" height="75"><br>
                        <span>Biomagnetismo médico </span>
                        <p>Con Isaac Goiz Durán. El biomagnetismo es una herramienta muy útil para eliminar virus, bacterias, hongos y parásitos a través de colocar imanes en distintos puntos del cuerpo.  </p>  
                    </div>
                    <div class="col-lg-5 mr-auto">
                        <img src="assets/img/icon2.png" height="75"><br>
                        <span>Astrología</span>
                        <p>Con Olvido Ramos, maestra independiente. La astrología es el arte de interpretar el lugar que ocupan los planetas y como nos afectan. Nuestra carta natal es una fotografía de la misión de nuestra alma en esta vida, y los movimientos planetarios marcan acontecimientos importantes en nuestras vidas. Conocer estas influencias nos ayuda a comprender mejor nuestras lecciones, explotar nuestros dones, y transitar por la vida con menos turbulencias y dudas.</p>  
                    </div>
                </div>
            </div>
        </section>
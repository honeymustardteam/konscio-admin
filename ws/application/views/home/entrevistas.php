<style>

.col-lg-3{

    margin-bottom: 50px;

}

</style>

<header class="masthead bg-primary text-white text-center" style="background-image: url(<?php echo base_url().'assets/img/entrevistas.jpg' ?>);">

    <div class="container d-flex align-items-center flex-column">

                

        <!-- Masthead Heading-->

        <h1 class="masthead-heading text-uppercase mb-0"><?php echo $inter ?></h1>

                

    </div>

</header>



<!-- About Section-->

<section class="page-section bg-primary mb-0 entrevistas" id="about">

    <div class="container">

        <!-- About Section Heading-->

        <h2 class="page-section-heading text-center text-uppercase">Podcast</h2>

        <!-- Icon Divider-->

        <div class="divider-custom divider-light">

            <div class="divider-custom-line"></div>

        </div>

        <!-- About Section Content-->

        <div class="row entrevistas">

            <div class="col-lg-3 ml-auto">

                <img src="assets/img/podcast.svg">

                <p>MyNDTALK, Women's Wednesday - Befriending The Wolf - Milly Diericx<br>

                <span><?php echo $edate ?></span>

                </p>

            </div>

            <div class="col-lg-3">

            <a href="http://itsyourhealthwithlisadavis.com/wp-content/uploads/2016/12/Milly-for-site-.mp3" target="_blank"><img src="assets/img/podcast.svg"></a>

                        <p><?php echo $en1 ?><br>

                <span><?php echo $edate1?></span>

                </p>  

            </div>

            <div class="col-lg-3 mr-auto">

            <img src="assets/img/podcast.svg">

                <p><?php echo $en2?><br>

                <span><?php echo $edate2 ?></span>

                </p>

            </div>

            <div class="col-lg-12"></div>

            <div class="col-lg-3 ml-auto">

            <a href="https://youtube.be/UZT9LodCQWO" target="_blank"><img src="assets/img/podcast.svg"></a>

                <p><?php echo $en3?><br>

                <span><?php echo $edate3?></span>

                </p>

            </div>

            <div class="col-lg-3">

            <a href="https://www.voiceamerica.com/show/3934/love-light" target="_blank"><img src="assets/img/podcast.svg"></a>

                <p><?php echo $en4?><br>

                <span><?php echo $edate4?></span>

                </p> 

            </div>

            <div class="col-lg-3 mr-auto">

            <a href="https://tinyurl.com/y2262cvh" target="_blank"><img src="assets/img/podcast.svg"></a>

                <p><?php echo $en5?><br>

                <span><?php echo $edate5?></span>

                </p>

            </div>

            

        </div>

    </div>

</section>



<section class="page-section bg-primary mb-0 entrevistas" id="about" style="background-color: #f7f7f7;">

    <div class="container">

        <!-- About Section Heading-->

        <h2 class="page-section-heading text-center text-uppercase"><?php echo $tvt?></h2>

        <!-- Icon Divider-->

        <div class="divider-custom divider-light">

            <div class="divider-custom-line"></div>

                </div>

        <!-- About Section Content-->

        <div class="row entrevistas">

            <div class="col-lg-3 ml-auto">

                <img src="assets/img/tv.svg">

                <p><?php echo $tv?><br>

                <span><?php echo $tdate?></span>

                </p>

            </div>

            <div class="col-lg-3 ">

            <img src="assets/img/tv.svg">

                <p><?php echo $tv1?><br>

                <span><?php echo $tdate1?></span>

                </p>  

            </div>

            <div class="col-lg-3 mr-auto">

            <a href="https://m.facebook.comstory.phpstory_fbid=1982636958508683&id=168014326637631" target="_blank"><img src="assets/img/tv.svg"></a>

                <p><?php echo $tv2?><br>

                <span><?php echo $tdate2?></span>

                </p>

            </div>

            <div class="col-lg-12"></div>

            <div class="col-lg-3 ml-auto">

            <a href="https://www.youtube.com/watch?v=jR5ZVI7x4NE&feature=share&fbclid=IwAR1vXcYf8rEaqR7yIpd-fGcJKKWIxYvZL2UH7wc_Lwg2yZB6-QUBj5G3U0E" target="_blank"><img src="assets/img/tv.svg"></a>

                <p><?php echo $tv3?><br>

                <span><?php echo $tdate3    ?></span>

                </p> 

            </div>

            <div class="col-lg-3">
               <img src="assets/img/tv.svg">

                <p><?php echo $tv4?><br>

                <span></span>

                </p> 
            </div>

            <div class="col-lg-3 mr-auto">

            </div>

                    

        </div>

    </div>

</section>
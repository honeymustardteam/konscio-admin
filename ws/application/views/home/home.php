<header class="masthead bg-primary text-white text-center" style="background-image: url(<?php echo base_url() ?>assets/img/home.jpg);padding-top: 8rem; padding-bottom: 4rem;">

    <div class="container d-flex align-items-center flex-column">

                

        <!-- Masthead Heading-->

        <img class="width-100" src="<?php echo base_url() ?>assets/img/logo-white@2x.png" style="max-width:412px">

        <h1 class="masthead-heading text-uppercase mb-0 texto_home"><?php echo $language_msg; ?></h1>

        <a class="boton_home" href="terapias.php"><?php echo $ver?></a>

    </div>

</header>



<!-- About Section-->

<section class="page-section bg-primary biografia mb-0" id="biografia">

    <div class="container">

        <!-- About Section Heading-->

        <h2 class="page-section-heading text-center text-uppercase"><?php echo $language_msg1; ?></h2>

        <!-- Icon Divider-->

        <div class="divider-custom divider-light">

            <div class="divider-custom-line"></div>

        </div>

        <!-- About Section Content-->

        <div class="row educacion" >

            <div class="col-lg-5 ml-auto" style="display:flex">

                <img class="width-100" style="margin:auto" src="<?php echo base_url() ?>assets/img/milly-fondo-blanco-new.png">

            </div>

            <div class="col-lg-5 mr-auto" style="display:flex">

                <div style="margin:auto">    

                    <p><?php echo $language_msg2 ?></p>

                    <p><?php echo $language_msg3 ?></p>

                </div>

            </div>

        </div>

    </div>

</section>



<!-- Educación-->

<section class="page-section bg-primary mb-0 educacion_home" id="educacion"  style="background-image:url('<?php echo base_url() ?>assets/img/educacion_home.jpg')">

    <div class="container" >

        <!-- About Section Heading-->

        <h2 class="page-section-heading text-center text-uppercase"><?php echo $language_msg4 ?></h2>

        <!-- Icon Divider-->

        <div class="divider-custom divider-light">

            <div class="divider-custom-line" style="border: solid 1px #ffffff;"></div>

        </div>

        <!-- About Section Content-->

        <div class="row educacion_home">

            <div class="col-lg-5 ml-auto">

                <ul>

                    <li>Universidad Iberoamericana: <strong><?php echo $language_msg5 ?></strong><br>

                        <span><?php echo $language_msg6 ?></span><br>

                        <?php echo $language_msg7 ?></span></li>

                    <li>Centro Ericksoniano de Mexico: <strong><?php echo $language_msg8 ?></strong> <br>

                        <span><?php echo $language_msg9 ?></span><br>

                        <?php echo $language_msg10 ?></li>



                </ul>

            </div>

            <div class="col-lg-5 mr-auto">

                <ul>

                    <li>Instituto Mexicano de Psicooncología: <strong><?php echo $language_msg11 ?></strong><br>

                        <span><?php echo $language_msg12 ?></span><br>

                        <?php echo $language_msg13 ?></li>



                </ul>

            </div>

            <div class="col-lg-12" align="center">

                <a class="boton_home" href="educacion.php"><?php echo $ver2 ?></a>

            </div>

        </div>

    </div>

</section>



<!-- About Section-->

<section class="page-section bg-primary mb-0 biografia fondo_terapias" id="terapias">

    <div class="container">

        <!-- About Section Heading-->

        <h2 class="page-section-heading text-center text-uppercase"> <?php echo $language_msg14?></h2>

        <!-- Icon Divider-->

        <div class="divider-custom divider-light">

            <div class="divider-custom-line"></div>

        </div>

        <!-- About Section Content-->

        <div class="row terapias">

            <div class="col-lg-5 ml-auto">

                <ul>

                    <li><h3>Mary Burmeister, Certificate of Attendance in the art of Jin Shin Jyutsu</h3>

                        <span> <?php echo $language_msg15?></span><br>

                        <?php echo $language_msg16?></li>

                            

                    <li><h3><?php echo $language_msg18?></h3>

                    <span> <?php echo $language_msg17?></span><br>

                    <?php echo $language_msg19?></li>

                        

                    <li><h3>Weiss Institute, Past Life Therapy Training</h3>

                    <span><?php echo $language_msg20?></span><br>

                    <?php echo $language_msg21?></li>

                        

                    <li><h3>Institute for Quantum Consiousness, Facilitator Training “Journey Through other Realms”</h3>

                    <span><?php echo $language_msg22?></span><br>

                    <?php echo $language_msg23?></li>

                           

                    <li><h3><?php echo $language_msg24?></h3>

                    <?php echo $language_msg25?></li>

                    
                        

                </ul>

            </div>

            <div class="col-lg-5 mr-auto">

                <ul>
                        

                    <li><h3><?php echo $language_msg26?></h3>
                    
                    <span><?php echo $language_msg27?></span><br>

                    <?php echo $language_msg28?></li>


                    <li><h3>Omega NYC: Reflect, Reconnect, Renew. Touching the Unseen World: Discovering your Spiritual Self</h3>

                    <span><?php echo $language_msg29?></span><br>

                    <?php echo $language_msg30?></li>

                        

                    <li><h3>The Newton Institute, Life Between Lives Therapist</h3>

                    <span><?php echo $language_msg31?></span><br>

                    <?php echo $language_msg32?></li>

                        

                    <li><h3><?php echo $language_msg33?></h3>

                    <span><?php echo $language_msg34?></span><br>

                    <?php echo $language_msg35?></li>

                        

                    <li><h3><?php echo $language_msg36?></h3>

                    <?php echo $language_msg37?></li>

            


                </ul>

            </div>

        </div>

            <div class="col-lg-12" align="center">

                <a class="boton_home" href="terapias.php"><?php echo $ver2?></a>

            </div>

        </div>

    </div>

</section>



<!-- About Section-->

<section class="page-section bg-primary biografia mb-0" id="blog">

    <div class="container">

        <!-- About Section Heading-->

        <h2 class="page-section-heading text-center text-uppercase">Blog</h2>

        <!-- Icon Divider-->

        <div class="divider-custom divider-light">

            <div class="divider-custom-line"></div>

        </div>

        <div class="row cursos entradas">



            <?php foreach ($posts as $key => $p) : ?>

                <div class="col-lg-4 entrada">

                    <div style="width:100%; height:300px; background:url('<?php echo $p->cover_image !=""? base_url()."/uploads/cover_post/".$p->cover_image :'' ?>'); background-size: cover; background-position: center;box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.5);"></div>

                    

                    <h3><?php echo $p->title ?><br><span><?php echo format_date($p->created_at) ?></span></h3>

                            

                    <p><?php echo word_limiter ($p->content,20) ?></p>

                    <p><a href="<?php echo base_url() . 'blog/'. $p->url_clean ?>">Seguir leyendo</a><p>

                </div>

            <?php endforeach; ?>



        </div>

        

        <div class="row cursos">

            <div style="margin: 0 auto;">

            <?php

                

    

                    $prev = $current_page - 1;

                    $next = $current_page + 1;

    

                    if ($prev < 1)

                        $prev = 1;

    

                    if ($next > $last_page)

                        $next = $last_page;

                    ?>

                <ul class="pagination pagination-sm row d-flex justify-content-center">

                    <!--<li class="page-link"><a class="page-item" href = "<?php //echo base_url() . $token_url . $prev ?>">Prev</a></li>-->

                    <?php for ($i = 1; $i <= $last_page; $i++) { ?>

                        <li class="page-item"><a class="page-link" style="color:#a1a1a1" href ="<?php echo base_url() . $token_url . $i; ?> "> <?php echo $i; ?></a></li>

                    <?php } ?>



                    <?php if ($current_page != $next) { ?>

                        <!--<li class="page-link"><a class="page-item" href = "<?php //echo base_url() . $token_url . $next; ?> ">Sig</a></li>

                        -->

                        <?php } ?>

                </ul>

           </div>

        </div> 

        

        

    </div>

</section>



<!-- Educación-->

<section class="page-section bg-primary mb-0 contacto" id="contacto" style="background-image:url('<?php echo base_url() ?>assets/img/bg-contact.jpg')">

    <div class="container">

        <!-- About Section Heading-->

        <h2 class="page-section-heading text-center text-uppercase"><?php echo $language_msg38?></h2>

        <!-- Icon Divider-->

        <div class="divider-custom divider-light">

            <div class="divider-custom-line" style="border: solid 1px #ffffff;"></div>

        </div>

        <!-- About Section Content-->

        <div class="row contacto">

            <div class="col-lg-3 ml-auto" align="center">

                <img src="<?php echo base_url() ?>assets/img/icn-mail.svg"><br><br>

                <a href="#">hola@concienciayevolucion.com</a>

            </div>

            <div class="col-lg-3 mr-auto" align="center">

                <img src="<?php echo base_url() ?>assets/img/icn-phone.svg"><br><br>

                <a href="#">55 15 74 59 61</a>

            </div>



            <div class="col-lg-12" align="center"></div>

            <div class="col-lg-6 ml-auto mr-auto" align="left" style="margin-top: 100px;">

            <label for="nombre"><?php echo $language_msg39?></label><br>

            <input type="text" id="nombre" name="nombre"><br><br>



            <label for="telefono"><?php echo $language_msg40?></label><br>

            <input type="text" id="telefono" name="telefono"><br><br>



            <label for="correo"><?php echo $language_msg41?></label><br>

            <input type="email" id="correo" name="correo"><br><br>



            <label for="mensaje"><?php echo $language_msg42?></label><br>

            <textarea id="mensaje" name="mensaje" rows="4">     

            </textarea><br><br>

            <div class="alert alert-success" id="success-alert" style="display:none" >

                <strong><?php echo $language_msg43?></strong><br><br>

            </div>

            <div class="alert alert-danger" id="error-alert" style="display:none" >

                <strong id="texto-error" ><?php echo $language_msg44?></strong><br><br>

            </div>

            <button type="button" onclick="clickBtnEnviarCorreo()" class="btn btn-primary form-control" id="btn-enviar" ><?php echo $language_msg45?></li></button>



            </div>

        </div>

    </div>

</section>





<script>



const clickBtnEnviarCorreo = () => {



    const nombre = document.querySelector('#nombre').value

    const telefono = document.querySelector('#telefono').value

    const correo = document.querySelector('#correo').value

    const mensaje = document.querySelector('#mensaje').value

    const btn = document.querySelector('#btn-enviar')



    if([nombre,telefono,correo,mensaje].some(e=>e==='')) {

        mostrarMensaje('error-alert', 'Todos los campos son obligatorios')

        return

    }



    enviarMensaje(nombre,telefono,correo,mensaje,btn)



}



const enviarMensaje = (nombre,telefono,correo,mensaje,btn) => {



    btn.firstChild.data = "Cargando..."

    btn.disabled = "disabled"



    const url = "https://proyectoshm.com/conciencia-evolucion/ws/email/index.php"



    var formData = new FormData();



    formData.append('nombre', nombre);

    formData.append('telefono', telefono);

    formData.append('email', correo);

    formData.append('mensaje', mensaje);



    fetch(url, {

    method: 'POST',

    body: formData

    }).then(response => response.json())

    .catch(error => {

        console.log("Error ", error)

        mostrarMensaje('error-alert','Todos los campos son requeridos')

    })

    .then(response => {

        console.log("Response ", response)

        if(response.status===1){

            limpiarFormulario()

            btn.firstChild.data = "Enviar"

            btn.disabled = ""

            mostrarMensaje('success-alert')

            $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){

                $("#success-alert").slideUp(500);

            });

        } else {

            mostrarMensaje('error-alert')

        }

    });



}



const mostrarMensaje = (id, mensaje = null) => {

    $('#'+id).fadeTo(2000, 500).slideUp(500, () => $(id).slideUp(500))

    document.getElementById('texto-error').innerHTML = mensaje!==null ? mensaje : 'Ocurrió un error inesperado'

}



const limpiarFormulario = () => {

    document.querySelector('#nombre').value = ''

    document.querySelector('#telefono').value = ''

    document.querySelector('#correo').value = ''

    document.querySelector('#mensaje').value = ''

}



</script>
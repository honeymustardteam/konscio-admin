<header class="masthead bg-primary text-white text-center" style="background-image: url(<?php echo base_url().'assets/img/educacion.jpg' ?>);">

    <div class="container d-flex align-items-center flex-column">

                

        <!-- Masthead Heading-->

        <h1 class="masthead-heading text-uppercase mb-0">LINKS</h1>

                

    </div>

</header>



<!-- About Section-->

<section class="page-section bg-primary text-white mb-0" id="about">

            <div class="container">

        <!-- About Section Content-->

        <div class="row educacion">

            <div class="col-lg-3 ml-auto">

                <h3><?php echo $link ?></h3>

                <ul>

                    <li>The Newton Institute<br>

                        <span><a class="links" href="http://newtoninstitute.org" target="_blank">http://newtoninstitute.org</a></span></li>

                    <li>Resonance Repatterning Mexico<br>

                        <span><a class="links" href="http://resonancerepatterning-mexico.com" target="_blank">http://resonancerepatterning-mexico.com</a></span></li>

                    <li>Resonance Repatterning <br>

                        <span><a class="links" href="http://www.resonancerepatterning.net" target="_blank">http://www.resonancerepatterning.net</a></span></li>

                    <li>Hipnosis<br>

                        <span><a class="links" href="http://www.hipnosis.com.mx" target="_blank">http://www.hipnosis.com.mx</a></span></li>





                </ul>

            </div>

            <div class="col-lg-3">

                <h3>&nbsp;</h3>

                <ul>

                    <li>Befriending the Wolf Newton Institute<br>

                        <span><a class="links" href="http://befriendingthewolf.com/" target="_blank">http://befriendingthewolf.com/</a></span></li>

                    <li>Milly Diericx<br>

                        <span><a class="links" href="http://millydiericx.com" target="_blank">http://millydiericx.com</a></span></li>

                    <li>Bienestar 360 <br>

                        <span><a class="links" href="http://bienestar360.com" target="_blank">http://bienestar360.com</a></span></li>

                    <li><?php echo $book1?><br>

                        <span><a class="links" href="http://www.concienciayevolucion.com" target="_blank">http://www.concienciayevolucion.com</a></span></li>





                </ul>

            </div>

            <div class="col-lg-3 mr-auto">

                <h3><?php echo $link1 ?></h3>

                <ul>

                    <li>Milly Diericx<br>

                        <span><a class="links" href="https://www.facebook.com/milly.diericx.77" target="_blank">https://www.facebook.com/milly.diericx.77</a></span><br>

                        <span>@MillyDiericx1</span>

                    </li>

                    <li><?php echo $book1?><br>

                        <span><a class="links" href="https://www.facebook.com/Conciencia-y-evoluci%C3%B3n-100372338298468" target="_blank">https://www.facebook.com/Conciencia-y-evoluci%C3%B3n-100372338298468</a></span><br>

                        <span><a class="links" href="https://instagram.com/concienciaparalaevolucion?r=nametag" target="_blank">https://instagram.com/concienciaparalaevolucion?r=nametag</a></span><br>

                        <span><a class="links" href="https://www.youtube.com/channel/UC7yTaE49njuKlH9obxHmo8w" target="_blank">https://www.youtube.com/channel/UC7yTaE49njuKlH9obxHmo8w</a></span><br>

                        <span><a class="links" href="https://www.youtube.com/results?search_query=Conscousness+%26+evolution+by+Dr.+Diericx&app=desktop" target="_blank">https://www.youtube.com/results?search_query=Conscousness+%26+evolution+by+Dr.+Diericx&app=desktop</a></span>

                    </li>

                    <li>Befriending the Wolf<br>

                        <span><a class="links" href="https://www.facebook.com/befriendingthewolf" target="_blank">https://www.facebook.com/befriendingthewolf</a></span>

                    </li>

                    <li>Bienestar 360<br>

                        <span><a class="links" href="https://www.facebook.com/1983726058507890/" target="_blank"> https://www.facebook.com/1983726058507890/</a></span><br>

                        <span><a class="links" href="https://instagram.com/bienestar360mx?r=nametag" target="_blank"> https://instagram.com/bienestar360mx?r=nametag</a></span>

                    </li>

                </ul>

            </div>

        </div>

    </div>

</section>
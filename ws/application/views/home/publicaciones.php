<header class="masthead bg-primary text-white text-center" style="background-image: url(<?php echo base_url().'assets/img/publicaciones.jpg' ?>);">

            <div class="container d-flex align-items-center flex-column">

                

                <!-- Masthead Heading-->

                <h1 class="masthead-heading text-uppercase mb-0"><?php echo $pub ?></h1>

                

            </div>

        </header>



        <!-- About Section-->

        <section class="page-section bg-primary text-white mb-0" id="about">

            <div class="container">

                <!-- About Section Content-->

                <div class="row publicaciones">

                    <div class="col-lg-5 ml-auto">

                        <ul>

                            <li><h3>Diericx, Milly, Befriending the Wolf: The Guide to Living with Lupus, Morgan James</h3>

                                <span> N.Y., 2016</span>

                                

                            <li><h3>U.S. News Article: 4 Keys to Coping With the Emotions of a Chronic Illness Diagnosis First on the list: acceptance and getting to know your new self.</h3>

                            <span>By Milly Diericx | Contributor Nov. 4, 2016, at 10:27 a.m.</span>

                            

                            <li><h3>Mind Body Green, The Only 3 Questions You Need To Ask Yourself The Next Time You're Stressed Out,</h3>

                            <span>by Milly Diericx Octubre 31, 2016</span>

                           

                            <li><h3>Lupus Foundation of America, Lupus Awareness Month, Article on Befiending the Wolf: The Guide to Living and Thriving with Lupus. </h3>

                            <span>By Migdalia Fernandez, 27 de Enero, 2017.</span>



                            <li><h3>EIPPY 2020 "Be Love" Book Authors' Only Page </h3>

                            <span><a class="links" href="https://www.mybookmybiz.com/eippy2020book" target="_blank">https://www.mybookmybiz.com/eippy2020book</a></span>

                            

                        </ul>

                    </div>

                    <div class="col-lg-5 mr-auto">

                        <ul>

                            <li><h3>U.S. News Article: What It's Like to Be Diagnosed With a Chronic Illness 'Your body has become a sort of prison from which you can never escape, except through death.'</h3>

                                <span>By Milly Diericx | Contributor Nov. 2, 2016, at 6:00 a.m.</span>

                                

                            <li><h3>U.S. News Article: 5 Daily Steps to Combat the Depression That Comes With Chronic Illness Hope, positive thinking and gratitude make the list.</h3>

                            <span>By Milly Diericx | Contributor Dec. 14, 2016, at 6:00 a.m.</span>



                            <li><h3>Willowbay.com, Article about Befriending the Wolf: How One Therapist Found Health By Befriending Her Incurable Disease Quick Adsense WordPressPlugin: </h3>

                            <span><a class="links" href="http://quicksense.net/2a" target="_blank">http://quicksense.net/2a</a></span>

                            

                            <li><h3>Mariah Nichols, Voices from the Disability Community, Milly Diericx</h3>

                            <span>March 7, 2017</span>

                        </ul>

                    </div>

                </div>

            </div>

        </section>



        <!-- Libros-->

        <section class="page-section bg-primary mb-0 patrones" id="about">

            <div class="container">

                <!-- About Section Heading-->

                <h2 class="page-section-heading text-center text-uppercase"><?php echo $book ?></h2>

                <!-- Icon Divider-->

                <div class="divider-custom divider-light">

                    <div class="divider-custom-line"></div>

                </div>

                <!-- About Section Content-->

                <div class="row">

                    <div class="col-lg-5 ml-auto">

                        <p><strong>Befriending the Wolf <br>

                            The Guide to Living and Thriving with Lupus</strong></p>

                        <p>

                            “<?php echo $text?>”</p>

                        

                        <p><?php echo $text1?></p></div>

                    <div class="col-lg-5 mr-auto" style="text-align:right; display:flex"><img class="width-100" src="assets/img/libros.jpg"></div>



                    <div class="col-lg-8 mr-auto ml-auto" style="padding:80px 0;"><hr></div>

                    

                    <div class="col-lg-5 ml-auto" style="text-align:right; display:flex"><img class="width-100" style="margin-top:0" src="assets/img/lectura.jpg"></div>

                    <div class="col-lg-5 mr-auto interlineado">

                        <p style="font-size:16px"><strong><?php echo $book1?><br>

                        <?php echo $text2?></strong>

                        </p>

                        <p>

                            “ <?php echo $text3?>

                        </p>

                        <p>

                        <?php echo $text4?>

                        </p>

                        <p><?php echo $text5?>
                        </p>

                    </div>

                </div>

            </div>

        </section>
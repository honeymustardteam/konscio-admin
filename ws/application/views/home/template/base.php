<!DOCTYPE html>

<html lang="en">

    <head>

        <meta charset="utf-8" />

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

        <meta name="description" content="" />

        <meta name="author" content="" />

        <title>Conciencia y evolución</title>

        <!-- Favicon-->

        <link rel="icon" type="image/x-icon" href="<?php echo base_url() ?>assets/img/favicon.ico" />

        <!-- Font Awesome icons (free version)-->

        <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>

        <!-- Google fonts-->

        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />

        <!--<link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />-->

        <link href="https://fonts.googleapis.com/css2?family=Lato:wght@100;300;400;700;900&display=swap" rel="stylesheet">

        <!-- Core theme CSS (includes Bootstrap)-->

        <link rel='stylesheet' href="<?php echo base_url(); ?>assets/css/custom.css">

    </head>

    <body id="page-top">

        <!-- Navigation-->

        {nav}

        <!-- Masthead-->

        {content}

        <!-- Copyright Section-->

        <?php include("footer.php"); ?>

        

        <!-- Bootstrap core JS-->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>

        <!-- Third party plugin JS-->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

        <!-- Contact form JS-->

        <script src='<?php echo base_url() ?>assets/js/mail/jqBootstrapValidation.js'></script>

        <script src='<?php echo base_url() ?>assets/js/mail/contact_me.js'></script>

        <!-- Core theme JS-->

        <script src='<?php echo base_url() ?>assets/js/scripts.js'></script>

    </body>

</html>


<nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">

    <div class="container">

        <a class="navbar-brand js-scroll-trigger" href="index.php#page-top"><img src="<?php echo base_url().'assets/img/logo-header@2x.png' ?>" class="logo" ></a>

        <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">         

            <i class="fas fa-bars"></i>

        </button>

        <div class="collapse navbar-collapse" id="navbarResponsive">

            <ul class="navbar-nav ml-auto">

                <!-- <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#">Inicio</a></li>-->

                <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="<?php echo base_url() ?>?#biografia"><?php echo $msg1 ?></a></li>

                <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="<?php echo base_url() ?>#educacion"><?php echo $msg2 ?></a></li>

                <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="<?php echo base_url() ?>#terapias"><?php echo $msg3 ?></a></li>

                <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="<?php echo base_url() ?>publicaciones"><?php echo $msg4 ?></a></li>

                <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="<?php echo base_url() ?>entrevistas"><?php echo $msg5 ?></a></li>

                <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="<?php echo base_url() ?>cursos"><?php echo $msg6 ?></a></li>

                <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="<?php echo base_url() ?>#blog">Blog</a></li>

                <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="<?php echo base_url() ?>links">Links</a></li>
                
                <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="<?php echo base_url() ?>?#contacto"><?php echo $language_msg38 ?></a></li>
                
                <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="<?php echo base_url() ?>langswitch/switchLanguage/english">ENG</a></li>
                
                <li class="nav-item mx-0 mx-lg-0"><a class="nav-link py-3 px-0 px-lg-0 rounded js-scroll-trigger" href="<?php echo base_url() ?>langswitch/switchLanguage/spanish">ESP</a></li>
                
            </ul>

        </div>

    </div>

</nav>
<!-- Masthead-->
<header class="masthead bg-primary text-white text-center" style="background-image: url(<?php echo base_url().'assets/img/terapias.jpg' ?>);">
    <div class="container d-flex align-items-center flex-column">
                
        <!-- Masthead Heading-->
        <h1 class="masthead-heading text-uppercase mb-0">TERAPIAS</h1>
                
    </div>
</header>

<!-- About Section-->
<section class="page-section bg-primary text-white mb-0 fondo_terapias" id="about">
    <div class="container">
        <!-- About Section Content-->
        <div class="row terapias">
            <div class="col-lg-5 ml-auto">
                <ul>
                    <li><h3>Mary Burmeister, Certificate of Attendance in the art of Jin Shin Jyutsu </h3>
                        <span> Agosto 2007</span><br>
                        Jin Shin Jyutsu, método traído a Occidente por Mary Burmeister, es un plano de puntos energéticos que recuerdan al cuerpo su creación, su función perfecta. Durante la vida estos candados energéticos se bloquean y necesitamos desbloquearlos para regresar al funcionamiento apropiado de nuestro cuerpo.</li>
                            
                    <li><h3>Instituto de Gestalt, Reprogramación Energética para la Salud</h3>
                    <span> 18 de Febrero, 2007</span><br>
                    Esta terapia tomada por Sandy Radomski de N.A.E.T., está diseñada para aliviar las reacciones alérgicas. Cuando tenemos una reacción alérgica, es el cuerpo sobre reaccionando a una sustancia que no debería de provocar esa reacción. Al tratar la sustancia con su emoción correspondiente, se desprograma la sobre reacción del cuerpo.</li>
                        
                    <li><h3>Weiss Institute, Past Life Therapy Training</h3>
                    <span>October 8-12, 2012</span><br>
                    Técnica desarrollada por Brian Weiss. Esta  regresión accesa las memorias del alma de las experiencias de vidas pasadas, ayudándonos a resolver los problemas persistentes en esta vida que parecen no tener explicación, como relaciones conflictivas, circunstancias muy difíciles o fobias.</li>
                        
                    <li><h3>Institute for Quantum Consiousness, Facilitator Training “Journey Through other Realms”</h3>
                    <span>7 de Abril, 2019</span><br>
                    Basada en los principios de la física cuántica. Es una técnica hipnótica diferente, desarrollada por Peter Smith, en la que expandimos la consciencia a los diferentes niveles de nuestro ser, donde podemos ver nuestra verdadera magnificencia como seres multidimensionales y tocar nuestra esencia eterna.</li>
                           
                    <li><h3>Biomagnetismo </h3>
                    Método creado por Isaac Goiz. A través de imanes se crea un ambiente interno en el cuerpo que inhibe la reproducción de virus, bacterias y patógenos.</li>
                        
                    <li><h3>Sanando el Sistema Familiar</h3>
                    Método creado por Carin Block. Derivado de las constelaciones familiares, este método utiliza la kinesiología para ordenar el sistema familiar.</li>
                        
                </ul>
            </div>
            <div class="col-lg-5 mr-auto">
                <ul>
                    <li><h3>Master Reiki</h3>
                        <span>28 de Octubre, 2008</span><br>
                        Impartida por Rami Rion. A través de la transmisión energética, el facilitador de Reiki actúa como canal de la energía curativa para el receptor, cuyo sistema utiliza donde la necesita. El Reiki actúa como acelerador de las energías sanadoras naturales del cuerpo.</li>
                            
                    <li><h3>Omega NYC: Reflect, Reconnect, Renew. Touching the Unseen World: Discovering your Spiritual Self</h3>
                    <span>April 20, 2012</span><br>
                    Impartido por James Van Praagh. A veces estamos en contacto con energías desencarnadas que no pueden dejar el plano terrestre por alguna razón. Este contacto les ayuda a resolver las situaciones pendientes para poder seguir adelante.</li>
                        
                    <li><h3>The Newton Institute, Life Between Lives Therapist</h3>
                    <span>January 2015</span><br>
                    Michael Newton desarrolla esta regresión hipnótica a las memorias del alma que accesa la vida que llevamos como almas, cuando no estamos en el cuerpo. En este estado de consciencia podemos conocer a nuestros guías espirituales, nuestra misión y propósito de vida y la trayectoria evolutiva de nuestra alma.</li>
                        
                    <li><h3>Bio Reprogramación Cuántica</h3>
                    <span>16 de Enero, 2020</span><br>
                    Esta técnica de Claudio Caliendi es una limpieza de los obstáculos y bloqueos que se encuentran en el campo energético. A través de la verificación kinesiológica podemos barrer los residuos energéticos de las experiencias de vida de nuestro campo.</li>
                        
                    <li><h3>Transformación de Patrones de Resonancia</h3>
                    Busca la causa de los problemas utilizando la kinesiología y a través de técnicas curativas de varias disciplinas, cambia nuestra resonancia con ellas, haciendo que el problema actual deje de ser un obstáculo. Aquello con lo que resuenas es lo que vives.</li>
                        
                    <li><h3>Tanatologia</h3>
                    Su objetivo es ayudar a las personas a procesar y aliviar sus duelos.</li>
                        
                    <li><h3>Psicoterapia Ericksoninana</h3>
                    Creado por Milton Erickson, el padre de la hipnosis moderna. Utilizando técnicas de imaginación accesa el poder de la mente inconsciente ayudándola a sanar heridas del pasado para proyectarnos al futuro con toda la fuerza de nuestro potencial.</li>
                        

                </ul>
            </div>
        </div>
    </div>
</section>
<?php     

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header('Content-Type: application/json');

if(!isset($_POST['mensaje']) || !isset($_POST['email']) ){
    $resp = [
        "msg" => "Es necesario los campos: mensaje",
        "status" => 0, 
    ];
    echo json_encode($resp);
    return false;
}

$to_email = 'william.terrones@outlook.com';
$subject = 'Conciencia y evolución';
$mensaje = $_POST['mensaje'];
$nombre = $_POST['nombre'];
$telefono = $_POST['telefono'];
$message = "Nombre: $nombre<br>Teléfono: $telefono<br>Mensaje: $mensaje ";
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
 
// Create email headers
$headers .= 'From: '.$_POST['email']."\r\n".
    'Reply-To: '.$from."\r\n" .
    'X-Mailer: PHP/' . phpversion();

if(mail($to_email,utf8_decode($subject), utf8_decode($message) ,$headers)){
    $resp = [
        "msg" => "Mensaje enviado",
        "status" => 1, 
    ];
    echo json_encode($resp);
} else {
    $resp = [
        "msg" => "Error",
        "status" => 0, 
    ];
    echo json_encode($resp);
}
?>
<?php
    header('Access-Control-Allow-Origin: *');
    ini_set('memory_limit','512M');
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    date_default_timezone_set('America/Mexico_City');
    
   
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    
    require 'email/PHPMailer/Exception.php';
    require 'email/PHPMailer/PHPMailer.php';
    require 'email/PHPMailer/SMTP.php';
    
    include('conexion.php');

    $mail = new PHPMailer(true);
    
    $nombre = isset($_REQUEST["nombre"]) ? $_REQUEST["nombre"] : '';
    $telefono = isset($_REQUEST["telefono"]) ? $_REQUEST["telefono"] : '';
    $contenido= isset($_REQUEST["mensaje"]) ? $_REQUEST["mensaje"] : '';
    $correo = isset($_REQUEST["correo"]) ? $_REQUEST["correo"] : '';
    
    $curso = isset($_REQUEST["curso"]) ? $_REQUEST["curso"] : '';
    
    $formulario = isset($_REQUEST["formulario"]) ? $_REQUEST["formulario"] : '';
    $tipo_formulario = isset($_REQUEST["tipo_formulario"]) ? $_REQUEST["tipo_formulario"] : '';
    
    
   
    
    
    
    
    if($correo == ''){
        echo 'faltan datos';
        die();
    }
    
    $bodyMensaje = '';
    
    if($curso){
        $bodyMensaje .= "Curso de interes: $curso
        <br>";
    }
    if($tipo_formulario == 'libros'){
        $bodyMensaje .= "Solicitando el regalo gratis desde la sección de libros
        <br>";
    }
    if($nombre){
        $bodyMensaje .= "Nombre: $nombre
        <br>";
    }
    
    
    if($correo){
        $bodyMensaje .= "Correo: $correo
        <br>";
    }
    if($telefono){
        $bodyMensaje .= "Teléfono: $telefono
        <br>";
    }
  
    
    
    if($contenido){
        $bodyMensaje .= "<br>Mensaje: $contenido
        <br>";
    }
    
    
    $mensaje="
        Se ha recibido un nuevo correo: 
        <br><br>
        
        $bodyMensaje
        
        <br><br><br>
        Enviado desde: <a href='https://konscio.mx'>https://konscio.mx</a>
    ";
    
    $mensajeCliente="
        Hemos recibido tu correo.
        <br><br>
        
        En la brevedad te contactaremos, gracias.
        
        <br><br><br>
        Att. <a href='https://konscio.mx'>Konscio</a>
    ";
    
    
    
     $sql = "INSERT INTO `clientes` ( `nombre`, `telefono`, `correo`, `mensaje`) VALUES ( '$nombre', '$telefono', '$correo', '$contenido');";
    if (!mysqli_query($conn, $sql)) {
      echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }
    
    
    
    enviarCorreo('admin', 'Nuevo correo desde la Web', $mensaje, false, $mail, $tipo_formulario);
    
    
    $mail2 = new PHPMailer(true);
    enviarCorreo('cliente', 'Gracias por contactarnos', $mensajeCliente, $correo, $mail2, $tipo_formulario);
    
    echo "Mensaje enviado";
    
    function enviarCorreo($tipo, $asunto, $mensaje, $correo, $mail, $tipo_formulario){
        try {
            //Server settings
            $mail->CharSet = 'UTF-8';
            //$mail->SMTPDebug = 0;                                       // Enable verbose debug output
            $mail->SMTPDebug = false;
            $mail->isSMTP();                                            // Set mailer to use SMTP
            $mail->Host       = 'smtp.gmail.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   = 'konscio.mexico@gmail.com';                     // SMTP username
            $mail->Password   = 'konscio2021';                               // SMTP password
            $mail->SMTPSecure = 'ssl';                       // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 465; 

    
            //Recipients
            $mail->setFrom('konscio.mexico@gmail.com', 'Konscio México');
            
            
            if($tipo == 'admin'){
                /*
                $mail->addAddress('info@konscio.mx');
                $mail->addAddress('info@konscio.com.mx');
            	$mail->addBCC('marco12195@hotmail.com');
            	$mail->addBCC('eduardo@honeymustard.mx');
            	*/
            
            	$mail->addAddress('marco12195@hotmail.com');
            	$mail->addAddress('eduardo@honeymustard.mx');
            	
            	if($tipo_formulario == 'libros'){
            	    
            	    move_uploaded_file($_FILES['file']['tmp_name'],"uploads/imagenes_para_regalos/".$_FILES['file']['name']);
            	    
            	    //$mail->AddAttachment($_FILES['file']['tmp_name'], $_FILES['file']['name']);
            	    $mail->addAttachment("uploads/imagenes_para_regalos/".$_FILES['file']['name']);
            	}
            }else{
                $mail->addAddress($correo);
            }
           
               
            
          
            // Content
            $mail->isHTML(true);
            $mail->Subject = $asunto;
            $mail->Body = $mensaje;
          
            
            $mail->send();
            
            
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }
    
   

?>